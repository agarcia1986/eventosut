# Aviso de Privacidad Simplificado

<p class="text-justify">En cumplimiento a Ley General de Protección de Datos Personales en Posesión de los 
Sujetos Obligados y la Ley de Protección de Datos Personales en Posesión de Sujetos 
Obligados para el Estado de Quintana Roo, la UT Cancún en su calidad de Sujeto Obligado 
informa que es el responsable del tratamiento de los Datos Personales que nos proporcione, 
los cuales serán protegidos de conformidad con lo dispuesto en los citados ordenamientos 
y demás que resulten aplicables.</p>
<p>La información de carácter personal aquí proporcionada, únicamente podrá ser utilizada 
para base de datos, estadística y logística del evento, asumiendo la obligación de cumplir 
con las medidas legales y de seguridad suficientes para proteger los Datos Personales que 
se hayan recabado. Para mayor detalle consulte, nuestro Aviso de Privacidad Integral en: 
www.utcancun.edu.mx en la sección de “Avisos de Privacidad”</p>
