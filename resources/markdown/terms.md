# Terminos de servicio.
<p class="text-justify">En cumplimiento a Ley General de Protección de Datos Personales en Posesión de los Sujetos Obligados 
y la Ley de Protección de Datos Personales en Posesión de Sujetos Obligados para el Estado de Quintana 
Roo, la Universidad Tecnológica de Cancún en su calidad de Sujeto Obligado informa que es el 
responsable del tratamiento de los Datos Personales que nos proporcione, tales como imágenes o 
fotografías, registro de audio y video, y en algunos casos, el nombre de particulares y/o de servidores 
públicos, así como datos laborales como nombre de la Institución o Sujeto Obligado al que pertenece, los 
cuales serán protegidos de conformidad con lo dispuesto en los citados ordenamientos y demás que 
resulten aplicables. La información de carácter personal aquí proporcionada, únicamente podrá ser 
utilizada para difundir y archivar la memoria gráfica de los eventos públicos que realiza la Universidad 
Tecnológica de Cancún, asumiendo la obligación de cumplir con las medidas legales y de seguridad 
suficientes para proteger los Datos Personales que se hayan recabado. Para mayor detalle consulte, 
nuestro Aviso de Privacidad Integral en: www.utcancun.edu.mx en la sección de Transparencia –
Protección de Datos Personales</p>
