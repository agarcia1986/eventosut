<x-guest-layout>
    <x-jet-authentication-card>
        <x-slot name="logo">
            <x-jet-authentication-card-logo />
        </x-slot>
    <div class="text-sm breadcrumbs">
        <ul class="flex h-8 space-x-2 ml-4">
          <li>
            <a href="/">
              <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" class="w-4 h-4 mr-2 stroke-current"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 7v10a2 2 0 002 2h14a2 2 0 002-2V9a2 2 0 00-2-2h-6l-2-2H5a2 2 0 00-2 2z"></path></svg>
              Inicio
            </a>
          </li> 
          <li>
            <a href="{{route('register')}}">
              Registro
            </a>
          </li> 
        </ul>
    </div>
        <div class="flex h-8 space-x-2 ml-4">
            <x-jet-validation-errors/>      
        </div>
        <form method="POST" action="{{ route('register') }}" class="flex flex-col w-full max-w-lg p-12 rounded shadow-lg dark:text-gray-100 ng-untouched ng-pristine ng-valid">
            @csrf
            <div class="mt-4">
                <x-jet-label for="name" value="{{ __('Nombre') }}" />
                <x-jet-input id="name" class="block mt-1 w-full" type="text" name="name" :value="old('name')" required autofocus autocomplete="name" onkeyup="javascript:this.value=this.value.toUpperCase();"/>
            </div>
            <div class="mt-4">
                <x-jet-label for="pname" value="{{ __('Apellido paterno') }}" />
                <x-jet-input id="pname" class="block mt-1 w-full" type="text" name="pname" :value="old('pname')" required autofocus autocomplete="pname" onkeyup="javascript:this.value=this.value.toUpperCase();"/>
            </div>
            <div class="mt-4">
                <x-jet-label for="mname" value="{{ __('Apellido materno') }}" />
                <x-jet-input id="mname" class="block mt-1 w-full" type="text" name="mname" :value="old('mname')" required autofocus autocomplete="mname" onkeyup="javascript:this.value=this.value.toUpperCase();"/>
            </div>
            <div class="mt-4">
                <x-jet-label for="email" value="{{ __('Correo Electrónico') }}" />
                <x-jet-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required />
            </div>

            <div class="mt-4">
                <x-jet-label for="password" value="{{ __('Contraseña (8 Caracteres)') }}" />
                <x-jet-input id="password" class="block mt-1 w-full" type="password" name="password" required autocomplete="new-password" />
            </div>

            <div class="mt-4">
                <x-jet-label for="password_confirmation" value="{{ __('Confirma tu Contraseña') }}" />
                <x-jet-input id="password_confirmation" class="block mt-1 w-full" type="password" name="password_confirmation" required autocomplete="new-password" />
            </div>
            <div class="mt-4">
                <x-jet-label for="sexo" value="{{ __('Sexo') }}" />
                    <select name="sexo" class="w-full flex items-center h-12 px-4 mt-2 rounded focus:outline-none focus:ring-2 dark:text-gray-900 focus:dark:border-violet-400 focus:ring-violet-400">
                        <option value="1">Hombre</option>
                        <option value="2">Mujer</option>
                        <option value="3">No definido</option>
                    </select>
            </div>
            <div class="mt-4">
                <x-jet-label for="modalidad" value="{{ __('Modalidad ') }}" />
                    <select name="modalidad" class="w-full flex items-center h-12 px-4 mt-2 rounded focus:outline-none focus:ring-2 dark:text-gray-900 focus:dark:border-violet-400 focus:ring-violet-400">
                        <option value="1">Público en General</option>
                        <option value="2">Alumno</option>
                        <option value="3">Administrativo/Docente</option>
                        <option value="4">Egresado</option>
                    </select>
            </div>
            <div class="mt-4">
                <x-jet-label for="celular" value="{{ __('Telefono Celular') }}" />
                <x-jet-input id="celular" class="block mt-1 w-full" type="text" name="celular" required autocomplete="telefono" />
            </div>

            @if (Laravel\Jetstream\Jetstream::hasTermsAndPrivacyPolicyFeature())
                <div class="mt-4">
                    <x-jet-label for="terms">
                        <div class="flex items-center">
                            <x-jet-checkbox name="terms" id="terms"/>

                            <div class="ml-2">
                                {!! __('Estoy de acuerdo con  :terms_of_service y  :privacy_policy', [
                                        'terms_of_service' => '<a target="_blank" href="'.route('terms.show').'" class="underline text-sm text-gray-600 hover:text-gray-900">'.__('Términos de servicio').'</a>',
                                        'privacy_policy' => '<a target="_blank" href="'.route('policy.show').'" class="underline text-sm text-gray-600 hover:text-gray-900">'.__('Politica de privacidad').'</a>',
                                ]) !!}
                            </div>
                        </div>
                    </x-jet-label>
                </div>
            @endif
            <div class="flex items-center justify-end mt-4">
                <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('login') }}">
                    {{ __('¿Ya tienes un registro?') }}
                </a>

                <x-jet-button class="ml-4">
                    {{ __('Registrar') }}
                </x-jet-button>
            </div>
        </form>
    </x-jet-authentication-card>
</x-guest-layout>
