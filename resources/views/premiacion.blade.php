<x-app-layout>
    <div class="container max-w-5xl mx-auto -mt-1 opacity-90">
        <div class="mx-0 sm:mx-6">
            <div class="bg-white w-full p-8 md:p-18 text-xl md:text-1xl text-gray-800 leading-normal">
                		
            <div class="container font-sans bg-green-50 rounded p-4 md:p-24 text-center">
                <div class="mb-2">
                    <p class="py-1 text-center ">Sorteo Carrera UT Cancún 2022</p>				    
                </div>		
                
                <h3 class="font-bold break-normal font-normal text-gray-600 text-base md:text-xl">Nómina- Nombre</h3>
                <h2  id="nombregenerado" class="font-bold break-normal text-2xl md:text-4xl"> - </h2>
                <div class="w-full text-center pt-2">
                    <div class="w-full text-center pt-2">
                        <div class="max-w-sm mx-auto p-1 pr-0 flex flex-wrap items-center">
                            <button id="genera" type="submit" onclick="FormAleatorio()" class="btn btn-accent btn-outline flex-1 mt-4">Rifar</button>		
                        </div>
                    </div>
                </div>
                <div class="w-full text-center pt-6">
                    <div class="flex flex-wrap -mx-2">
                        <div class=" md:w-3/4 px-2 pb-12">
                            <div class="h-full bg-white rounded overflow-hidden shadow-md hover:shadow-lg relative smooth">
                                        <div class="p-6 h-auto ">	
                                            <p class="text-gray-600 text-xs md:text-sm">Numeros Ganadores</p>
                                            <!--div id="historial" class="font-bold text-sm text-gray-900"-->
                                                <div class="table">
                                                    <table id="table">
                                                    <thead>
                                                        <tr>
                                                            <th>Num.</td>
                                                            <th>Nombre</td>
                                                            <th>Premio</td>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="muestraganadores">
                                                       <!--AQUI VA EL CUERPO DE LA TABLA--->
                                                    </tbody>
        
                                                </table>
                                            </div>
                                            <!--div class="historial">
                                            </div>
                                        <div-->
                                            <p class="text-gray-800 font-serif text-base mb-5">
                                            </p>
                                            </div>    
                            </div>  
                        </div>
                        <div class="md:w-1/4 px-2 pb-12">
                            <div class="h-full bg-white rounded overflow-hidden shadow-md hover:shadow-lg relative smooth">
                                
                                    <div class="p-6 h-auto md:h-48">	
                                        <p class="text-gray-600 text-xs md:text-sm">Premio</p>
                                        <!--div id="CantHistory" class="font-bold text-xl text-gray-900">
                                           <h2 class="font-bold break-normal text-2xl md:text-2xl">0</h2>
                                        </div>
                                        <p id="historial" class="text-gray-800 font-serif text-base mb-5">
                                         -
                                        </p-->
                                        <div id="premio" class="font-bold text-xl text-gray-900">
                                           <h2 class="font-bold break-normal text-2xl md:text-2xl">-</h2>
                                        </div>
                                    </div>                         
                            </div>
                        </div>
                    </div>
                        <div class="w-full text-center pt-6">
                            <div class="flex flex-wrap -mx-2">
                                
                                <!--div class="h-full bg-white rounded overflow-hidden shadow-md hover:shadow-lg relative smooth">
                                    <div  class="p-6 h-auto ">
                                        <button class="btn btn-sm mb-2" type="submit" onclick="printganadores()">Historial</button>
                                        <a href="#" class="h6" id="descargar">Descargar</a>
                                        <button  class="btn btn-sm mb-2 btn-info" id="descargar">Descarga</button>
                                        
                                        
                                        </div>
                                    </div-->
                                </div>
                            </div>
                            <a href="#" class="text-small" id="descargar">Descarga</a>
                        </div>
                </div>
            </div>        
        </div>			
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
   integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
   crossorigin="anonymous">
</script>
<script src="jquery.tableToExcel.js"></script>


<script>   
let participantes=[
[0,22,'Aurelio'],
[1,3433,'David'],
[2,456,'Nanci'],
[3,1204,'Rosalba'],
[4,334,'Carlos'],
[5,11,'Pascual'],
[6,3322,'Martin']
];
let premios=[
    [0,'tele'],
    [1,'refri'],
    [2,'plancha'],
    [3,'alexa']
];
let ganadores = [];
let savethenumber = []; 
function getNumRand(min, max) {       
    return Math.round(Math.random()*(max-min)+parseInt(min));
}
/**Funcion matematica que genera los numeros sin repetir*/
function getNumRandUnique(min, max, ganadores) {      
    var NumAleatorio =  getNumRand(min, max);
    if(! ganadores.includes( NumAleatorio )){  
        ganadores.push(NumAleatorio);
        return NumAleatorio;
    }  
    else{
        getNumRandUnique(min, max, ganadores)
    }
}
    
function MostrarNombresGenerados(ganadores,diferencia){
    let nombreGenerado= document.getElementById('nombregenerado');
    let historialNum= document.getElementById('HistorialNum');
    let indexNumber = ganadores.length-1;
    if(ganadores.length  < 1){
        nombreGenerado.innerHTML="-"; 
    }
    else if(ganadores.length >= 1 && ganadores.length <= diferencia){
        nombreGenerado.innerHTML= participantes[ganadores[ganadores.length-1]][1]+'-'+participantes[ganadores[ganadores.length-1]][2];  
        insertavalganadores([participantes[ganadores[ganadores.length-1]][1],participantes[ganadores[ganadores.length-1]][2],premios[ganadores.length-1][1]]);
        printganadores();
    }
}
function insertavalganadores(array){  
    var index=savethenumber.length;
    savethenumber.push(array);
    return savethenumber;
}
function printganadores(){
    let muestraGanadores= document.getElementById('muestraganadores');
    let muestrapremio =document.getElementById('premio');
    let cadenaganadora="";
    let contador = 0;
    //console.table(savethenumber);
    savethenumber.forEach(element => {        
        contador ++;
        premio = premios[contador-1];
        if(contador == savethenumber.length){
            cadenaganadora = '<tr><td class="text-gray-600 text-xs md:text-sm">'+element[0]+'</td><td class="text-gray-600 text-xs md:text-sm">'+element[1]+'</td>'+'</td><td class="text-gray-600 text-xs md:text-sm">'+element[2]+'</td></tr>'+cadenaganadora;
        }
        else{
            cadenaganadora ='<tr><td class="text-gray-600 text-xs md:text-sm">'+element[0]+'</td><td class="text-gray-600 text-xs md:text-sm">'+element[1]+'</td>'+'</td><td class="text-gray-600 text-xs md:text-sm">'+element[2]+'</td></tr>'+cadenaganadora;
        }
    });
    muestraGanadores.innerHTML=cadenaganadora;
    muestrapremio.innerHTML= premio[1];
   
}
function MostrarHistorial(ganadores){
    let historial = document.getElementById('historial');
    let totalhistorial= document.getElementById('CantHistory');
    let muestrapremio =document.getElementById('premio');
    let StringHistorial = ""; 
    let contador = 0;
    ganadores.forEach(element => {        
        contador ++;
        if(contador == ganadores.length){
            premio = premios[contador-1];
            StringHistorial = '<button class="btn btn-success btn-md mr-1 mb-1">'+participantes[element][1]+'</button>'+ StringHistorial;
        }
        else{
            StringHistorial = '<button class="btn-sm">'+participantes[element][1]+'</button>'+ StringHistorial;
        }
    });
    muestrapremio.innerHTML= premio[1];
    historial.innerHTML =StringHistorial;

    //totalhistorial.innerHTML= '<H2 class="font-bold break-normal text-2xl md:text-2xl">'+ArraySend.length+'</h2>';
}
function FormAleatorio(){ //Este ejecuta el sorteo
    event.preventDefault();
    let botonGenera= document.getElementById('genera');//Nombre del botón
    let NumMin = 0;//minimo empieza en 0 por ser array
    let NumMax;
    if(premios.length >=1){
        NumMax = premios.length;   //longitud del array menos uno(porque empieza en 0)    
    }else{
        NumMax = participantes.length-1;   //longitud del array menos uno(porque empieza en 0)
    }
    var diferencia = (NumMax - NumMin);
    
    if(ganadores.length < diferencia-1){            
        getNumRandUnique(NumMin, NumMax, ganadores); 
        MostrarNombresGenerados(ganadores,diferencia);
    }
    else if(ganadores.length == diferencia-1){
        getNumRandUnique(NumMin, NumMax, ganadores); 
        MostrarNombresGenerados(ganadores,diferencia);
        botonGenera.setAttribute('disabled');
        botonGenera.innerHTML='Felicidades a todos';
    }
    else{               
        botonGenera.setAttribute('disabled');
        botonGenera.innerHTML='Felicidades a todos';
    }
    //MostrarHistorial(ganadores); 
}
const guardarArchivoDeTexto = (contenido, nombre) => {
        const a = document.createElement("a");
        let descarga;
        const archivo = new Blob([contenido], { type: 'text/html' });
        const url = URL.createObjectURL(archivo);
        a.href = url;
        a.download = nombre;
        a.click();
        URL.revokeObjectURL(url);
    }
    const $botonDescargar = document.querySelector("#descargar");
    
    
    $botonDescargar.onclick = () => {
        guardarArchivoDeTexto(    
        savethenumber, "ganadores.txt");
    }
</script>
</x-app-layout>