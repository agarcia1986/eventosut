<nav class="bg-utblue" x-data="{open:false}">
  <div class="max-w-7xl mx-auto px-2 sm:px-6 lg:px-8">
    <div class="relative flex items-center justify-between h-16">
      <!-- Boton del menu movil-->
      <div class="absolute inset-y-0 left-0 flex items-center sm:hidden">    
        <button type="button" x-on:click="open= true" class="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-white hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-white" aria-controls="mobile-menu" aria-expanded="false">
          <!--Boton hamburguesa-->
          <svg class="block h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16" />
          </svg>
          <svg class="hidden h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
          </svg>
        </button>
      </div>
      <div class="flex-1 flex items-center justify-center sm:items-stretch sm:justify-start">
        {{--LOGOTIPO--}}
        <a href="/" class="flex-shrink-0 flex items-center">
          <!--img class="block lg:hidden md:hidden sm:hidden h-8 w-auto" src="http://www.utcancun.edu.mx/wp-content/uploads/2016/07/estetamano.png" alt="UT Cancún"-->
          <img class="hidden sm:block md:block lg:block h-8 w-auto" src="http://utcancun.edu.mx/wp-content/uploads/2024/06/cropped-logo-11.png" alt="UT Cancún">
        </a>
        <!--div class="hidden sm:block sm:ml-6">
          <div class="flex space-x-4">
            <Current: "bg-gray-900 text-white", Default: "text-gray-300 hover:bg-gray-700 hover:text-white" >
            <a href="#" class="bg-gray-900 text-white px-3 py-2 rounded-md text-sm font-medium" aria-current="page">Dashboard</a>
              @foreach ($categorias as $categoria)
              <a href="#" class="text-gray-300 hover:bg-gray-700 hover:text-white px-3 py-2 rounded-md text-sm font-medium">{{$categoria->nombre}}</a>    
              @endforeach
          </div>
        </div-->
      </div>

      <div class="absolute inset-y-0 right-0 flex items-center pr-2 sm:static sm:inset-auto sm:ml-6 sm:pr-0">
        <!-- Menú del perfil -->
      <div class="hidden sm:block md:block lg:block ml-3 relative" x-data="{open : false}">
        @auth
          <div>
            <button x-on:click="open = true" type="button" class="bg-gray-800 flex text-sm rounded-full focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white" id="user-menu-button" aria-expanded="false" aria-haspopup="true">
              <!--span class="sr-only">Open user menu</span-->
              <img class="h-8 w-8 rounded-full" src="{{ auth()->user()->profile_photo_url}}" alt="">
            </button>
          </div>
         <!--MENUS-->
          <div x-show ="open" x-on:click.away="open=false" class="origin-top-right absolute right-0 mt-2 w-48 rounded-md shadow-lg py-1 bg-white ring-1 ring-black ring-opacity-5 focus:outline-none" role="menu" aria-orientation="vertical" aria-labelledby="user-menu-button" tabindex="-1">
            <!-- Active: "bg-gray-100", Not Active: "" -->
            <!--a href="{{route('profile.show')}}" class="block px-4 py-2 text-sm text-gray-700" role="menuitem" tabindex="-1" id="user-menu-item-0">Tu perfil</a-->
            @can('admin.home')
              <a href="{{route('admin.home')}}" class="block px-4 py-2 text-sm text-gray-700 font-medium" role="menuitem" tabindex="-1" id="user-menu-item-0">Dashboard</a>
            @endcan
            @can('user.home')
              <a href="{{route('user.home')}}" class="block px-4  text-sm text-gray-700 font-medium" role="menuitem" tabindex="-1" id="user-menu-item-0">Dashboard</a>
            @endcan 
            <form method="POST" action="{{ route('logout') }}" x-data>
              @csrf
            <a href="{{ route('logout')}}" class="block px-4 text-sm text-gray-700 font-medium" role="menuitem" tabindex="-1" id="user-menu-item-2"  @click.prevent="$root.submit();">Cerrar Sesión</a>
            </form>
          @else
            <a href="{{route('login')}}" class="text-gray-300 hover:bg-gray-700 hover:text-white px-3 py-2 rounded-md text-md font-medium">Ingresar</a>
            <a href="{{route('register')}}" class="text-gray-300 hover:bg-gray-700 hover:text-white px-3 py-2 rounded-md text-md font-medium">Registro</a>
          @endauth
          </div>
      </div>
      </div>
  <!-- MENU MOVIL -->
  <div class="sm:hidden" id="mobile-menu" x-show="open" x-on:click.away="open = false">
    @auth
      <div class="px-2 pt-2 pb-3 space-y-1">
     
        @can('admin.home')
          <a href="{{route('admin.home')}}" class="bg-utblue text-white block px-3 py-2 rounded-md text-base font-medium" aria-current="page">Dashboard</a>
        @endcan
        @can('user.home')
          <a href="{{route('user.home')}}" class="bg-utblue text-white block px-3 py-2 rounded-md text-base font-medium" aria-current="page">Dashboard</a>
        @endcan 
      <form method="POST" action="{{ route('logout') }}" x-data class="bg-gray-700 text-white block px-3 py-2 rounded-md text-base font-medium">
        @csrf
        <a href="{{ route('logout')}}" class="bg-gray-700 text-white block px-3 py-2 rounded-md text-base font-medium" aria-current="page">Cerrar Sesión</a>  
      </form>
    @else
        <!-- Current: "bg-gray-900 text-white", Default: "text-gray-300 hover:bg-gray-700 hover:text-white" -->
        <a href="{{route('login')}}" class="bg-utblue text-white block px-3 py-2 rounded-md text-base font-medium" aria-current="page">Ingresar</a>
        <a href="{{route('register')}}" class="bg-utblue text-white block px-3 py-2 rounded-md text-base font-medium" aria-current="page">Registro</a>
      </div>
    @endauth
    
      @foreach ($categorias as $categoria)
        <!--a href="#" class="text-gray-300 hover:bg-gray-700 hover:text-white block px-3 py-2 rounded-md text-base font-medium">{{$categoria->nombre}}</a-->    
      @endforeach      
  </div>
</nav>

