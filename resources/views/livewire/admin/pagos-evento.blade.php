<div class="container-fluid">
    <div class="row">
        <div class="col-md-9">
        <div class="form-group">
            <label>Eventos</label>  
            <select wire:model="selectEvento">
                <option value="vacio">-</option>
                @foreach ($eventos as $evento)
                    <option value="{{$evento->id}}">{{$evento->nombre}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-3">
        @if (!is_null($selectEvento) && $selectEvento != "vacio")
        <button type="submit" class="btn btn-sm btn-outline-info" wire:click="exportPagos({{$selectEvento}})">Pagos</button>
        <button type="submit" class="btn btn-sm  btn-outline-info" wire:click="exportAsistencia({{$selectEvento}})">Excel Asistencia</button>
        <!--button type="submit" class="btn btn-sm  btn-outline-info">Excel Asistencia</button-->
        <a class="btn btn-sm  btn-outline-info" href="{{route('admin.user.export',$selectEvento)}}">Asistencias</a>
        @endif
        
    </div>
    </div>
    <div class="row">
        <div class="col-md-12">  
        @if (!is_null($pagos))
            @if ($pagos->count())
                <div class="table table-condensed table-responsive">
                    
                <table id="pagosEvento" class="table table-striped">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <!--th>Paterno</th>
                            <th>Materno</th-->
                            <th>Email</th>
                            <th>Celular</th>
                            <th>Num</th>
                            <th>Monto</th>
                            <th>Talla</th>
                            <th>Modalidad</th>
                            <th>Tipo Sanguineo</th>
                            <th>Teléfono de Emergencia</th>
                            <th>Términos y condiciones</th>
                            <!--th>Referencia</th-->
                            
                            
                            <th>Estatus</th>
                            <th colspan="3">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($pagos as $pago)
                            <tr class="text-">
                                <td class=""><small> {{$pago->name}} {{$pago->pname}} {{$pago->mname}}</small></td>
                                <!--td><small>{{$pago->pname}}</small></td>
                                <td><small>{{$pago->mname}}</small></td-->
                                <td><small>{{$pago->email}}</small></td>
                                <td><small>{{$pago->celular}}</small></td>
                                <td>
                                    @if(is_null($pago->numero))
                                        <a class="btn btn-sm btn-info" wire:click="asignarNumero({{$pago->id}})"><small>Asignar</small></a>
                                    @else
                                    <small>{{$pago->numero}}</small>
                                    @endif
                                </td>
                                <!--td><small>{{$pago->referencia}}</small></td-->
                                <td><small>${{$pago->monto}}.00</small></td>
                                <td>
                                    @switch($pago->talla)
                                        @case('xs')
                                            <small><p>Extra chico</p></small>
                                            @break
                                        @case('sm')
                                            <small><p>Chico</p></small>
                                            @break
                                        @case('md')
                                            <small><p>Mediana</p></small>
                                            @break
                                        @case('gd')
                                            <small><p>Grande</p></small>
                                            @break
                                        @default
                                            <small><p>Extragrande</p></small>
                                    @endswitch
                                </td>
                                <td>
                                    @switch($pago->modalidad)
                                        @case('p')
                                            <small>Presencial</small>
                                            @break
                                       
                                        @default
                                            <small>Virtual</small>
                                    @endswitch
                                 
                                </td>
                                <td><small>{{$pago->tiposanguineo}}</small></td>
                                <td><small>{{$pago->emergencia}}</small></td>
                                <td>
                                    @if ($pago->responsabilidad ==1)
                                    <small>Aceptó</small>    
                                    @endif
                                  </td>
                                    @switch($pago->estatuspago)
                                        @case(2)
                                            <td>
                                                <small><p>Revisar</p></small>
                                            </td>
                                            <td>
                                                <a href="{{route('admin.pagos.show',$pago)}}" ><i class="fas fa-solid fa-eye"></i></a>
                                            </td>   
                                                @break
                                        @case(3)
                                                <td>
                                                    <small><p>Pagado</p></small>
                                                    
                                                </td>
                                                <td>
                                                    <a href="{{route('admin.pagos.show',$pago)}}" ><i class="fas fa-solid fa-eye"></i></a>
                                                </td>
                                                @break
                                       @default
                                                <td>
                                                    <small><p>Espera</p></small>
                                                </td>
                                    @endswitch
                                    <td width="10px">
                                        <a href="" wire:click.prevent="sendmail({{ $pago->id }})">
                                            <i class="fas fa-lg fa-envelope"></i>
                                        </a>
                                    </td>
                                    <td width="10px">
                                        <a href="" wire:click.prevent="delete({{ $pago->id }})">
                                            <i class="fa fa-trash text-danger"></i>
                                        </a>
                                    </td>                            
                            </tr> 
                        @endforeach
                    </tbody>
                </table>
            </div>
            @else
                <x-adminlte-alert theme="danger" title="No hay pagos">
                </x-adminlte-alert>
            @endif
        @else
            <x-adminlte-alert class="bg-teal text-uppercase" title="Atención">
                Selecciona un Evento
            </x-adminlte-alert>
        @endif
    </div>
    
    </div><!--TERMINA CONTAINER DE LA TABLA-->
</div>  <!--TERMINA ROW -->
