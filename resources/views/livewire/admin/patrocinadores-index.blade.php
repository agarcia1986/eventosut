<div>
<x-adminlte-card theme="lime" theme-mode="outline">
    <div class="card-header">
        <div class="row">
           
            <div class="col-md-11">
                <input wire:model="search" class="form-control" type="text" placeholder="Ingrese el nombre del patrocinador">
            </div>
            <div class="col-md-1">
                <a type="button" class="btn btn-primary btn-md" href="{{route('admin.patrocinadores.create')}}">Nuevo</a>
          </div>
        </div> 
    </div>
   
    @if($patrocinadores->count())
    <div class="card-body">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Logo</th>
                    <th>Description</th>
                    <th>web</th>
                    <th>Redes</th>
                    <th colspan="2"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($patrocinadores as $patrocinador)
                <tr>
                 
                    <td>{{$patrocinador->nombre}}</td>
                    <td><img class="img img-rounded"  src="@if($patrocinador->image){{Storage::url($patrocinador->image->url)}} @else {{Storage::url('patrocinadores/empty.png')}} @endif " width="50px" height="50px"/> </td>
                    <td>{{$patrocinador->descripcion}}</td>
                    <td>{{$patrocinador->pagina_web}}</td>
                    <td>
                        @foreach (json_decode($patrocinador->redes) as $item =>$key)
                            <h6 class="text-small">{{$item}}:<small>{{$key}} </small></h6>
                        @endforeach    
                    </td>
                    <td with="10px"> 
                        <a class="btn btn-outline-primary btn-sm" href="{{route('admin.patrocinadores.edit', $patrocinador)}}"><i class="fas fa-sm fa-pen"></i></a>
                    </td>
                                    <td with="10px">
                                        <form action="{{route('admin.patrocinadores.destroy',$patrocinador)}}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <x-adminlte-button class="btn-sm" type="submit" theme="outline-danger" icon="fas fa-sm fa-trash"/>
                                        </form>
                                    </td> 
                </tr>
                @endforeach
            </tbody> 
            <tfoot>
              
            </tfoot>             
            @else
            <div class="card-body">
                <strong>No hay registros</strong>
            </div>
            @endif
             
        </table>
        {{$patrocinadores->links()}}
    </div>
   
</x-adminlte-card>
@section('js')
<script>
window.addEventListener('show-modal', (order) => {
   
    $('#patrocinadormodal').modal(order);
    //console.log(order.detail.order);
    if(order.detail.order == 'hidden'){
        //console.log('entra');
    $('.modal-backdrop').removeClass('show');
        //$(document.body).removeClass("modal-open");
   // $('.ruben').modal(order);
    }
    
})
</script>
@endsection
</div>
