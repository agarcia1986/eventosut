<x-adminlte-card theme="lime" theme-mode="outline">
    <div class="card-header">
        <div class="row">
            <div class="col-md-11">
                <input wire:model="search" class="form-control" type="text" placeholder="Ingrese el nombre del evento">
            </div>
            <div class="col-md-1">
                <a href="{{route('admin.permisos.create')}}" class="btn btn-sm btn-outline-info">
                    <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-plus-circle" viewBox="0 0 16 16">
                    <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                    <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
                    </svg>
                </a>              
            </div>
        </div>
        
    </div> 
    @if ($permisos->count())
        <div class="card-body">
            <table class="table table-stripped table-condensed">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Nombre</th>
                                <th>Descripción</th>
                                <th>fecha</th>
                                <th colspan="2"></th>
                            </tr>
                        </thead>
                        <tbody>
                                @foreach($permisos as $permiso)
                                    <tr>
                                        <td><small>{{$permiso->id}}</small></td>
                                        <td>{{$permiso->name}}</td>
                                        <td>{{$permiso->description}}</td>
                                        
                                        <td><small>{{date("d/m/Y",strtotime($permiso->created_at))}}</small></td>
                                        <td with="10px"><x-adminlte-button class="btn-sm" type="reset" theme="outline-info" icon="fas fa-sm fa-pen"/></td>
                                        <td with="10px">
                                            <form action="{{route('admin.permisos.destroy',$permiso)}}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <x-adminlte-button class="btn-sm" type="submit" theme="outline-danger" icon="fas fa-sm fa-trash"/>
                                                
                                            </form>
                                        </td> 
                                    </tr>
                                @endforeach                        
                        </tbody>
            </table>
            <div class="card footer">
                {{$permisos->links()}}
            </div>
        </div>    
    @else
                <div class="card-body">
                    <strong>No hay registros</strong>
                </div>
    @endif
</x-adminlte-card>
 