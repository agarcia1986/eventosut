<div>
    <x-adminlte-card theme="lime" theme-mode="outline">
        <div class="card-header">
            <div class="row">
                <div class="col-md-11">
                    <input wire:model="search" class="form-control" type="text" placeholder="Ingrese el nombre del patrocinador">
                </div>
                <div class="col-md-1">
                    <a type="button" class="btn btn-primary btn-md" href="{{route('admin.ponentes.create')}}">Nuevo</a>
                </div>
            </div>
        </div>
        <div class="card-body">
            @if ($ponentes->count())
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Nivel Educativo</th>
                        <th>Puesto</th>
                        <th>Compañia</th>
                        <th>Ponencia</th>
                        <th>Foto</th>
                        <th colspan="2">Acciones</th>
                    </tr>
                </thead>
              
                <tbody>
                    @foreach ($ponentes as $ponente)
                        <tr>
                            <td>{{$ponente->nombre}}</td>
                            <td>{{$ponente->nivestudio}}</td>
                            <td>{{$ponente->puesto}}</td>
                            <td>{{$ponente->compania}}</td>
                            <td>{{$ponente->ponencia}}</td>
                            <td><img class="img img-rounded"  src="@if($ponente->image){{Storage::url($ponente->image->url)}} @else {{Storage::url('ponentes/empty.png')}} @endif" width="50px" height="50px" /> </td>
                            <td with="10px">
                                <a class="btn btn-outline-primary btn-sm" href="{{route('admin.ponentes.edit', $ponente)}}"><i class="fas fa-sm fa-pen"></i></a>
                                
                            <td with="10px">
                                <form action="{{route('admin.ponentes.destroy',$ponente)}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <x-adminlte-button class="btn-sm" type="submit" theme="outline-danger" icon="fas fa-sm fa-trash"/>
                                </form>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>

                </tfoot>
            </table>
              @else
                  <p>No hay registros</p>
              @endif
            
        </div>
    </x-adminlte-card>
</div>
