<div>
    <form action="" method="post">
        <div class="row ">
            <div class="col-md-4 form-group">
                <label for="">Nombre</label>
                <input class="form-control" type="text ">
            </div>
            <div class="col-md-4 form-group">
                <label  for="slug">Slug</label>
                <input class="form-control" id="slug" type="text">
            </div>
            <div class="col-md-4">
                <label  for="">Intro</label>
                <input  class="form-control"type="text">
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 form-group">
                <label for="" >Información</label>
                <textarea class="form-control" name="informacion" id="informacion" cols="30" rows="10"></textarea>
            </div>
            <div class="col-md-4 form-group">
                <label  for="">Banner</label>
                <input class="form-control" type="file">
                <div>
                    <img src="" alt="">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 form-group">
                <label for="intro">Fecha :</label>
                <input class="form-control" type="date">
            </div>
            <div class="col-md-3">
                <label for="intro">Hora :</label>
                <input class="form-control" type="time">
            </div>
            <div class="col-md-3">
                <label for="intro">Inicia Inscripcion :</label>
                <input class="form-control" type="date">
            </div>
            <div class="col-md-3">
                <label for="intro">Termina Inscripcion :</label>
                <input class="form-control" type="date">
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 form-group">
                <label for="">Detalle de la ubicación :</label>
                <input class="form-control" type="text">
            </div>
            <div class="col-md-4">
                <label for="">Requisitos :</label>
                <input  class="form-control"type="text">
            </div>
            <div class="col-md-4">
                <label for="">Video (URL):</label>
                <input class="form-control" type="text">
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 form-group">
                <label for="">Categoria: </label>
                <select  class="form-control"name="" id="">
                    <option value="">q</option>
                    <option value="">2</option>
                </select>
                
            </div>
            <div class="col-md-4">
                <label for="">Convocatoria (PDF):</label>
                <input class="form-input" type="file">
            </div>
            
        </div>
        <div class="row">
            <button class="btn btn-block btn-success">Guardar</button>
        </div>

    </form>
</div>
