<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-xs-12 col-md-12">
                <input wire:model="search" class="form-control" type="text" placeholder="Ingrese el nombre del evento que desea encontrar">
            </div>
        </div>
    </div>
    
    @if ($eventos->count())
    <div class="card-body">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th class="text-left">Nombre</th>
                    <th class="text-center">Capacidad</th>
                    <th class="text-center">Inscritos</th>
                    <th class="text-center">Estado</th>
                    <th class="text-center"> Acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($eventos as $evento)
                    <tr>
                        <td class="align-middle">{{$evento->nombre}}</td>
                        <td class="align-middle text-center">{{$evento->setevento->capacidad}}</td>
                        <td class="align-middle text-center">
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" style="width:{{($evento->setevento->inscritos/$evento->setevento->capacidad)*100}}%;" aria-valuenow="5" aria-valuemin="0" aria-valuemax="100">{{($evento->setevento->inscritos/$evento->setevento->capacidad)*100}}% -- {{$evento->setevento->inscritos}}</div>
                            </div>
                              
                        </td>
                        <td class="align-middle text-center">
                            @if($evento->status==2)
                                <span class="badge badge-success">Publicado</span> 
                            @else 
                                <span class="badge badge-warning">Borrador</span>                              
                            @endif
                        </td>
                        <!--td>                    
                            <a href="{{route('admin.eventos.lista',$evento)}}"><i class="fas fa-solid fa-users"></i></a>
                        </td-->
                        <td class="vertical-alignment d-flex flex-column">
                            <a class="btn btn-outline-info btn-sm mb-1 d-flex align-items-center justify-content-center" href="{{route('admin.eventos.asistencia',$evento)}}"><i class="fas fa-list"></i></a>
                            <a class="btn btn-outline-info btn-sm mb-1 d-flex align-items-center justify-content-center" href="{{route('admin.eventos.show',$evento)}}"><i class="fas fa-eye"></i></a>
                            <a class="btn btn-outline-primary btn-sm mb-1 d-flex align-items-center justify-content-center" href="{{route('admin.eventos.edit',$evento)}}"><i class="fas fa-edit"></i></a>
                            <form class="form-inline d-flex flex-column mb-1" action="{{route('admin.eventos.destroy',$evento)}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-outline-danger btn-sm d-flex align-items-center justify-content-center" style="width: 100%; height: 100%;"><i class="fas fa-trash-alt"></i></button>
                            </form>
                        </td>                  
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <div class="card footer">
        {{$eventos->links()}}
    </div>      
    @else
    <div class="card-body">
        <strong>No hay registros</strong>
    </div>
    @endif
</div>
