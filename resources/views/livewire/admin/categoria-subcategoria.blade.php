<div class="row">
    <div class="form-group col-md-6">
        {!! Form::label('categoria_id','Categoría :') !!}  
      
        <select name="categoria_id" id="categoria_id" wire:model="categoriaId" class="form-control">
            <option value="">Selecciona..</option>
                @foreach ($categorias as $categoria)
                <option value="{{$categoria->id}}">{{$categoria->nombre}}</option>
                @endforeach
        </select>
        @error('categoria_id')
            <small class="text-danger">{{$message}}</small>
        @enderror  
    </div>
    <div class="form-group col-md-6">
        <label for="subcategoria">Subcategoría :</label>
            @if ($wsubcategorias != '')
                @foreach ($wsubcategorias as $subcategoria)
                    <div class=" form-check form-inline">
                        <input type="checkbox" name="subcategoria[]" id="subcategoria[]" value="{{$subcategoria->id}}" class="mr-1">
                        <label> 
                            {{$subcategoria->nombre}} <small class="text-success">  {{$subcategoria->referencia}}</small>
                        </label>
                    </div>
                @endforeach
            @else 
                <div class=" form-check form-inline">
                    <x-adminlte-alert theme="danger">
                        Debes elegir primero una categoría
                    </x-adminlte-alert>    
                    
                </div>
                
                
            @endif
            
         
                   
        </select>
        @error('subcategoria')
            <small class="text-danger">{{$message}}</small>
        @enderror
    </div>  
</div>
