<div class="card-header">
    <div class="row">
        <div class="col-md-11">
            <input wire:model="search" class="form-control" type="text" placeholder="Ingrese el nombre de la subcategoria">
        </div>
        <div class="col-md-1">
            <a href="{{route('admin.subcategorias.create')}}" class="btn btn-sm btn-outline-info">
                <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-plus-circle" viewBox="0 0 16 16">
                    <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                    <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
                </svg>
            </a>
        </div>
    </div>

@if ($subcategorias->count())
<table class="table table-stripped table-condensed">
    <thead>
        <tr>
            <th>Nombre</th>
            <th>Referencia</th>
            <th>Precio</th>
            <th colspan="3"></th>
        </tr>
    </thead>
    <tbody>
    @foreach ($subcategorias as $subcat)
        <tr>
            <td>
                {{$subcat->nombre}}
            </td>
            <td>
                {{$subcat->referencia}}
            </td>
            <td>
                ${{$subcat->precio}}.00
            </td> 
            <td with="10px">
                <a class="btn btn-outline-primary btn-sm" href="{{route('admin.subcategorias.edit',$subcat)}}">Editar</a>
            </td>
            <td with="10px">
                <form action="{{route('admin.subcategorias.destroy',$subcat)}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-outline-danger btn-sm">Eliminar</button>
                </form>
            </td>                     
        </tr>
    @endforeach
    </tbody>
</table>
<div class="card footer">
    {{$subcategorias->links()}}
</div>
@else
<div class="card-body">
    <strong>No hay registros</strong>
</div>
@endif
</div> 
