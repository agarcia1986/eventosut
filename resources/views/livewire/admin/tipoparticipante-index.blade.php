<div>
    <div class="card">
        <div class="card-body">
<table class="table table-striped">
    <thead>
        <tr>
            <th>
                referencia
            </th>
            <th>
                tipo
            </th>
            <th>
                monto:
            </th>
        </tr>
    </thead>
    <tbody>
        @foreach ($tipo as $tipo)
        <tr>
        <td>
        {{$tipo->referencia}}
        </td> 
        <td>
            {{$tipo->tipo}}
        </td>
        <td>
            ${{$tipo->precio}}.00
        </td>
        <td with="10px">
            <a class="btn btn-outline-primary btn-sm" href="{{route('admin.tipoparticipante.edit', $tipo)}}">Editar</a>
        </td>
        <td with="10px">
            <form action="{{route('admin.tipoparticipante.destroy',$tipo)}}" method="POST">
                @csrf
                @method('DELETE')
                <button class="btn btn-outline-danger btn-sm">Eliminar</button>
            </form>
        </td>
        </tr>        
        @endforeach

    </tbody>

</table>
            @foreach ($tipo as $tipo)
                
            @endforeach

        </div>
    </div>
</div>
