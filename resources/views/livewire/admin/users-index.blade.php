    <div class="card">
        
        <div class="card-header">
            <input wire:model="search" class="form-control" type="text" placeholder="Ingrese el nombre o correo de un usuario">
        </div>
        @if ($users->count())
            <div class="card-body">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>Nombre</th>
                            <th>Email</th>
                            <!--th>Rol</th-->
    
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $user)
                          <tr>
                              <td>{{$user->id}}</td>
                              <td>{{$user->name}}</td>
                              <td>{{$user->email}}</td>
                              <!--td>{{$user->roles}}</td-->                           
                              <td width="10px">
                                  <a class="btn btn-outline-primary btn-sm" href="{{route('admin.users.edit',$user)}}">Editar</a>
                                  <form class="form-inline d-flex flex-column mb-1" action="{{route('admin.users.destroy',$user)}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-outline-danger btn-sm d-flex align-items-center justify-content-center" style="width: 100%; height: 100%;"><i class="fas fa-trash-alt"></i></button>
                                </form>
                                </td>
                            </tr>  
                        @endforeach
    
                    </tbody>
                </table>
               
            </div>

            <div class="card footer">
                {{$users->links()}}
            </div>
        @else
            <div class="card-body">
                <strong>No hay coincidencias </strong>
            </div>        
        @endif
        
    </div>
