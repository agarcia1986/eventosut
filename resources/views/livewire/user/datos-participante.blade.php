<div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col info-box rounded col-lg-10 col-xl-8 bg-light p-2 m-1 ">
           <div class="row ">
              <div class="col-12 col-sm-2 d-flex justify-content-center align-items-center mb-2 mb-sm-0 display-3 text-primary">
                 <i class="fa fa-info-circle"><!-- / --></i>
              </div>
              <div class="col-12 col-sm-10 pl-sm-0 mb-2">
                <h2>Ya casi estas inscrito.</h2>
                 <p> Necesitamos que nos ayudes a completar tus datos, esto nos ayudara a conocerte mejor y tener una mejor organización del evento. 
                 </p>
                 <p class="text-danger"><small>Si eres parte de la comunidad UT deberas presentar una identificación vigente*</small></p>
              </div>
           </div>
           <!--div class="row">
              <div class="col text-right"><a class="btn btn-outline-primary" href="#">More</a></div>
           </div-->
        </div>
     </div>   
        <div class="row"> 
            <div class="container">
                {!! Form::model($participante,['route' => ['user.participantes.update',$participante],'autocomplete' => 'off', 'method' => 'put']) !!}
                <div class="row">
                    {!! Form::hidden('evento', $evento->id, ['class'=>'form-control','placeholder'=>'Ninguna']) !!}
                    {!! Form::hidden('categoria_evento', $evento->categoria_id, ['class'=>'form-control','placeholder'=>'Ninguna']) !!}
                </div>
                
                @if ($evento->categoria->nombre =="Deportivo")
                <div class="row">
                    <!--LINEA--> 
                        <div class="form-group col-md-6">
                            {!! Form::label('talla', '*', ['class'=>'text-danger']) !!}
                            {!! Form::label('talla', 'Talla') !!}
                            {!! Form::select('talla', ['xs'=>'extrachica','sm'=>'chica','md'=>'mediana','gd'=>'Grande','xl'=>'Extra grande'],null, ['class'=>'form-control']) !!}
                            @error('talla')
                                <small class="text-danger">{{$message}}</small>    
                            @enderror
                        </div>    
                        
                        <div class="form-group col-md-6">
                            {!! Form::label('alergias', '*', ['class'=>'text-danger']) !!}
                            {!! Form::label('alergias','¿Tiene alguna alergia o enfermedad? :') !!}
                            {!! Form::text('alergias', null, ['class'=>'form-control','placeholder'=>'Ninguna']) !!}
                            @error('alergias')
                            <small class="text-danger">En caso de ninguna, escriba No o ninguna </small>    
                            @enderror
                        
                        </div>
                    </div>
               
                 <!--LINEA-->
                <div class="row">
                    @if($participante->club != null)

                    <div class="form-group col-md-6">
                            {!! Form::label('club', '*', ['class'=>'text-danger']) !!}
                            {!! Form::label('club', '¿Pertenece a algun club?',['class' => '','name'=>'club']) !!}
                            {!! Form::text('club', null, ['class'=>'form-control']) !!}
                            @error('club')
                            <small class="text-danger">{{$message}}</small>    
                            @enderror
                    </div>
                    @else
                        <div class="form-group @if ($club == 1) col-md-3 @else col-md-6 @endif">
                            {!! Form::label('clubedit', '*', ['class'=>'text-danger']) !!}
                            {!! Form::label('clubedit', '¿Pertenece a algun club?',['class' => '','name'=>'club']) !!}
                            {!! Form::select('clubedit', ['0' => 'No','1' => 'Si'],null, ['class'=>'form-control' ,'wire:model'=>'club']) !!}
                        </div>
                    @if ($club == 1)
                            <div class="form-group col-md-3">
                                {!! Form::label('club', '*', ['class'=>'text-danger']) !!}
                                {!! Form::label('club', '¿Cual?',['class' => '','name'=>'club']) !!}
                                {!! Form::text('club', null, ['class'=>'form-control']) !!}
                                @error('club')
                                <small class="text-danger">{{$message}}</small>    
                                @enderror
                            </div> 
                    @endif
                @endif
                   
                    <div class="form-group col-md-6">
                        {!! Form::label('modalidad', '*', ['class'=>'text-danger']) !!}
                        {!! Form::label('modalidad', 'Modalidad :') !!}
                        {!! Form::select('modalidad', ['p'=>'Presencial','v'=> 'Virtual'],null, ['class'=>'form-control']) !!}
                        {{--!! Form::text('condicion', null, ['class'=>'form-control']) !!--}}
                        @error('modalidad')
                        <small class="text-danger">{{$message}}</small>    
                        @enderror
                    </div>
                </div>
                  <!--LINEA-->
                  <div class="row">
                    <div class="form-group col-md-6">
                        {!! Form::label('tiposanguineo', '*', ['class'=>'text-danger']) !!}
                        {!! Form::label('tiposanguineo', 'Tipo Sanguineo') !!}
                        {!! Form::select('tiposanguineo',['A+' => 'A Positivo','A-' => 'A Negativo','B+' => 'B Positivo','B-' => 'B Negativo','AB+' => 'AB Positivo','AB-' => 'AB Negativo','O+' => 'O Positivo','O-' => 'O Negativo'] ,null, ['class'=>'form-control']) !!}
                        @error('tiposanguineo')
                        <small class="text-danger">{{$message}}</small>    
                        @enderror
                    </div>
                    
                    <div class="form-group col-md-6">
                        {!! Form::label('emergencia', '*', ['class'=>'text-danger']) !!}
                        {!! Form::label('emergencia', 'Teléfono de emergencia :') !!}
                        {!! Form::text('emergencia', null, ['class'=>'form-control']) !!}
                        @error('emergencia')
                        <small class="text-danger">{{$message}}</small>    
                        @enderror
                    </div>
                </div>
                @endif
                 <!--LINEA-->
                <div class="row">
                    <div class="form-group col-md-6">
                        {!! Form::label('sexo', '*', ['class'=>'text-danger']) !!}
                        {!! Form::label('sexo', 'Sexo :') !!}
                        {!! Form::select('sexo',['1'=>'Masculino','2'=> 'Femenino', '3' => 'Otro'],null, ['class'=>'form-control']) !!}
                        @error('sexo')
                        <small class="text-danger">{{$message}}</small>    
                        @enderror
                    </div>
                    <div class="form-group col-md-6">
                        {!! Form::label('celular', '*', ['class'=>'text-danger']) !!}
                        {!! Form::label('celular', 'Celular :') !!}
                        {!! Form::text('celular', null, ['class'=>'form-control']) !!}
                        @error('celular')
                        <small class="text-danger">{{$message}}</small>    
                        @enderror
                    </div>
                </div>
               
                <!--LINEA-->
                <div class="row">
                    <div class="form-group col-md-6">
                        {!! Form::label('fechanacimiento', '*', ['class'=>'text-danger']) !!}
                        {!! Form::label('fechanac', 'Fecha de Nacimiento') !!}
                        {!! Form::date('fechanac', null, ['class'=>'form-control']) !!}
                        @error('fechanac')
                        <small class="text-danger">{{$message}}</small>    
                        @enderror
                    </div>
                    <div class="form-group col-md-6">
                        <label for="">Seleccione su categoria: </label>
                        @foreach ($subcategorias as $subcat)
                            <div>
                                <label>           
                                    {!! Form::radio('subcategoria[]',$subcat->id, null, ['class' => 'mr-1 form-control-inline']) !!}
                                    {{$subcat->nombre}}
                                </label>
                            </div>
                        @endforeach
                        @error('subcategorias')
                            <small class="text-danger">{{$message}}</small>    
                        @enderror
                    </div>
                </div>
                @if ($participante == "2")
                <div class="form-group col-md-6">
                    {!! Form::label('celular', '*', ['class'=>'text-danger']) !!}
                    {!! Form::label('celular', 'Celular :') !!}
                    {!! Form::text('celular', null, ['class'=>'form-control']) !!}
                    @error('celular')
                    <small class="text-danger">{{$message}}</small>    
                    @enderror
                </div>
                    
                @endif
                <div class="row">
                    <div class="form-group col-md-12">
                        <label>
                            {!! Form::checkbox('responsabilidad','1', 'true',['class' => 'mr-1','wire:model' => 'test']) !!}
                            Acepto los  <a href="#">Términos y condiciones</a>.
                        </label>
                        @error('responsabilidad')
                            <small class="text-danger">{{$message}}</small>    
                        @enderror
                    </div>
                </div>
                @if($test)
                    <div class="row">
                        <div class="col-md-12">
                            {!! Form::submit('Continuar', ['class' => 'btn btn-lg  btn-outline-info']) !!}
                        </div>
                    </div>
                
                @endif
                {!! Form::close() !!}
            </div>                       
        </div>
</div>