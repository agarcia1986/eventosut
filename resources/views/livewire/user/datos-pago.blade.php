@if($referencia =! "free")
<x-adminlte-card title="Pago en Banco / Transferencia" theme="info" icon="fas fa-lg fa-money-bill" collapsible>
    <div class="row">
        <div class="col-md-6">
            <x-adminlte-callout theme="success" class="bg-teal" icon="fas fa-lg fa-building" title="Pago en banco">
                    <span>Universidad Tecnológica de Cancún</span><br>
                    <span>Santander Serfín</span><br>
                    <span>CTA. 18000136307</span><br>
                    <span>CLABE interbancaria 014691180001363074</span><br>
                    <span>Convenio 7260</span>
            </x-adminlte-callout>
        </div>
        <div class="col-md-6">
            <x-adminlte-callout theme="success" class="bg-teal" icon="fas fa-lg fa-solid fa-credit-card" title="Pago por transferencia">   
                <p>NOTA:</p> 
                <p>La manera correcta para realizar la transferencia es: en el apartado de CONCEPTO solamente poner los 12 dígitos de la referencia bancaria correspondiente al concepto a pagar.</p>
            </x-adminlte-callout>
            
        </div>   
    </div>
    
    <div class="row">
        <div class="col-md-3">
            <h4>Referencia : </h4> 
        </div>
        <div class="col-md-3">
            <h5><span>{{$referencia[0]->referencia}}</span></h5>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <h4>Monto : </h4>
        </div>
        <div class="col-md-3">
            <h5><span>${{$referencia[0]->precio}}.00</span></h5>
        </div>
    </div>
    {!! Form::model($evento, ['route' => ['user.eventos.update', $evento], 'autocomplete' => 'off', 'files' => true, 'method' => 'put']) !!}         
        {!! Form::hidden('referencia', 'referencia :')!!}
        {!! Form::hidden('referencia', $referencia[0]->referencia) !!}    
        {!! Form::hidden('monto', 'Precio:')!!}
        {!! Form::hidden('monto', $referencia[0]->precio) !!}
        {!! Form::hidden('participante', 'Participante') !!}
        {!! Form::hidden('participante', Auth()->user()->id ) !!}
        {!! Form::submit('Inscribirme', ['class' => 'btn btn-lg btn-outline-info']) !!}
    {!! Form::close() !!}
</x-adminlte-card>
<x-adminlte-card title="Pago en linea (Próximamente)" theme="info" theme-mode="full" icon="fas fa-lg fa-money-card" collapsible="collapsed">
    No disponible.
</x-adminlte-card>
@else
<x-adminlte-card title="Este es un evento gratuito" theme="info" theme-mode="full" icon="fas fa-lg fa-money-card">
    Gracias por tu registro, por favor se puntual al evento.

    No olvides dar clik en aceptar.
</x-adminlte-card>

    {!! Form::model($evento, ['route' => ['user.eventos.update', $evento], 'autocomplete' => 'off', 'files' => true, 'method' => 'put']) !!}         
    {!! Form::hidden('referencia', 'referencia :')!!}
    {!! Form::hidden('referencia', 0) !!}    
    {!! Form::hidden('monto', 0) !!}
    {!! Form::hidden('participante', 'Participante') !!}
    {!! Form::hidden('participante', Auth()->user()->id ) !!}
    {!! Form::submit('Aceptar', ['class' => 'btn btn-lg btn-outline-info']) !!}
    {!! Form::close() !!}

@endif

