<x-adminlte-card theme="lightblue" theme-mode="outline">
  <div class="card-body">
      <div class="row">
          @if ($eventos->count())
              @foreach ($eventos as $ev)
              <div class="col-md-4 col-sm-6 mb-4">  
                  <div class="card bg-light shadow-sm h-100">
                      <img class="card-img-top img-fluid" style="height: 350px; object-fit: cover;" src="@if($ev->image){{Storage::url($ev->image->url)}}@else {{Storage::url('default_event.jpg')}}  @endif" >
                      <div class="card-body d-flex flex-column">
                          <h5 class="card-title border-bottom pb-3">{{$ev->nombre}}</h5>
                          <p class="card-text flex-grow-1">{{$ev->intro}}</p> 
                          <p class="small-text">{{date("d/m/Y",strtotime($ev->Setevento->fechaevento))}}</p>   
                          @foreach($ev->referencias as $ref)
                              <span class="badge badge-info">{{$ref->tipo}}</span>             
                          @endforeach                             
                          @if ($ev->participantes->count())
                              @foreach ($ev->participantes as $parti)
                                  @if ($parti->id == auth()->user()->id)
                                    <a href="{{route('user.eventos.edit',$ev)}}" class="btn btn-sm btn-info float-right mt-auto">Inscribirme</a>
                                  @endif 
                              @endforeach
                          @else
                          
                          <!--a href="{{route('user.eventos.edit',$ev)}}" class="float-left"><i class="fas fa-lg fa-qrcode"></i></a>
                          <a href="{{route('user.eventos.edit',$ev)}}" class="btn btn-sm btn-info float-right mt-auto"><i class="fas fa-lg fa-user-plus"></i></a-->
                          @endif
                      </div>      
                  </div>
              </div>
              @endforeach
          </div>
          <div class="card-footer">
              {{$eventos->links()}}    
          </div>
          @else
          <div class="card-body">
              <strong>No hay registros</strong>
          </div>
          @endif
  </div>
</x-adminlte-card>