@extends('adminlte::page')
@section('title', 'Eventos UT')

@section('content_header')
    <h1>Subcategorias</h1>
@stop

@section('content')
@if (session('info'))
    <div class="alert alert-success">
        {{session('info')}}
    </div>
@endif
<x-adminlte-card theme="lime" theme-mode="outline">
    @livewire('admin.subcategorias')
</x-adminlte-card>
    
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

