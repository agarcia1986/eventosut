@extends('adminlte::page')
@section('title', 'Eventos UT')

@section('content_header')
    <h1>Crear una subcategoria</h1>
@stop

@section('content')
<x-adminlte-card>
    <div class="container">
    {!! Form::open(['route'=> 'admin.subcategorias.store']) !!}
    <div class="row">
        <div class="col-md-4 form-group">
            {!! Form::label('nombre','Nombre') !!}
            {!! Form::text('nombre', null, ['class'=> 'form-control']) !!}
            @error('nombre')
                <span class="text-danger">{{$message}}</span>
            @enderror   
        </div>
        <div class="col-md-4 form-group">
            {!! Form::label('referencia','Referencia') !!}
            {!! Form::text('referencia', null, ['class'=> 'form-control']) !!}
            @error('referencia')
                <span class="text-danger">{{$message}}</span>
            @enderror 
        </div>
        <div class="col-md-4 form-group">
            {!! Form::label('precio','Precio') !!}
            {!! Form::text('precio', null, ['class'=> 'form-control']) !!}
            @error('precio')
                <span class="text-danger">{{$message}}</span>
            @enderror 
        </div>
    </div>
    {!! Form::submit('Crear', ['class'=> 'btn btn-success form-control']) !!}
    {!! Form::close() !!}
</div>
</x-adminlte-card>
    
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop