@extends('adminlte::page')
@section('title', 'Eventos UT')

@section('content_header')
    <h1>Crear una subcategoria</h1>
@stop

@section('content')
<x-adminlte-card>    
    {!! Form::model($subcategoria,['route'=>['admin.subcategorias.update',$subcategoria],'method' => 'put']) !!}
    <div class="row">
        <div class="col-md-4 form-group">
            {!! Form::label('nombre','Nombre') !!}
            {!! Form::text('nombre', null, ['class'=> 'form-control']) !!}
            @error('nombre')
                <span class="text-danger">{{$message}}</span>
            @enderror   
        </div>
        <div class="col-md-4 form-group">
            {!! Form::label('referencia','Referencia') !!}
            {!! Form::text('referencia', null, ['class'=> 'form-control']) !!}
            @error('referencia')
                <span class="text-danger">{{$message}}</span>
            @enderror 
        </div>
        <div class="col-md-4 form-group">
            {!! Form::label('precio','Precio') !!}
            {!! Form::text('precio', null, ['class'=> 'form-control']) !!}
            @error('precio')
                <span class="text-danger">{{$message}}</span>
            @enderror 
        </div>
    </div>
    {!! Form::submit('Actualizar', ['class'=> 'btn btn-success form-control']) !!}
    {!! Form::close() !!}
</x-adminlte-card>
@stop