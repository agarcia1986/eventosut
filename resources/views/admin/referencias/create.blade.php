@extends('adminlte::page')
@section('title', 'Eventos UT')
@section('content_header')
   
@stop
@section('content')
<x-adminlte-card title="Crear referencia" theme="success" theme-mode="outline" body-class="bg-none" header-class="bg-light"  footer-class="bg-none border-top rounded border-light"
    icon="fas fa-lg fa-plus" >
    <div class="row">
        
    {!! Form::open(['route'=>'admin.referencias.store','autocomplete' => 'off']) !!}
    @include('admin.referencias.partials.form')
    <div class="form-group">
    {!! Form::submit('Crear', ['class'=>'btn btn-success']) !!}
</div>
    {!! Form::close() !!}
    </div>

    
    
</x-adminlte-card>
@endsection