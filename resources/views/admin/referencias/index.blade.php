@extends('adminlte::page')
@section('title', 'Eventos UT')

@section('content_header')

<a  class="btn btn-outline-success btn-sm float-right"href="{{route('admin.referencias.create')}}">Nueva Referencia</a>
    <h1>Referencias</h1>
@stop

@section('content')
@if(session('info'))
    <div class="alert alert-success">
        <strong>{{session('info')}}</strong>
    </div>
@endif

@livewire('admin.referencia-index')

@endsection
    

