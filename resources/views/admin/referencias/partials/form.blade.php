<div class="form-group">
    {!! Form::label('tipo') !!}
    {!! Form::text('tipo', null, ['class'=>'form-control']) !!}
    @error('tipo')
    <small class="text-danger">{{$message}}</small>
@enderror
    </div>
    <div class="form-group">
    {!! Form::label('referencia') !!}
    {!! Form::text('referencia', null, ['class'=>'form-control']) !!}
    @error('referencia')
    <small class="text-danger">{{$message}}</small>
@enderror
    </div>
    <div class="form-group">
    {!! Form::label('precio') !!}
    {!! Form::text('precio', null, ['class'=>'form-control']) !!}
    @error('precio')
    <small class="text-danger">{{$message}}</small>
@enderror
    </div>