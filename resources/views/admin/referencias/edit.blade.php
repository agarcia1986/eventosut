@extends('adminlte::page')
@section('title', 'Eventos UT')

@section('content_header')

@stop
@section('content')
@if(session('info'))
    <div class="alert alert-success">
        <strong>{{session('info')}}</strong>
    </div>
@endif
    <x-adminlte-card title="Editar referencia" theme="success" theme-mode="outline" body-class="bg-none" header-class="bg-light"  footer-class="bg-none border-top rounded border-light"
    icon="fas fa-lg fa-plus" >
        <div class="row"> 
            {!! Form::model($referencia,['route'=>['admin.referencias.update',$referencia], 'method' => 'put']) !!}
            @include('admin.referencias.partials.form')
            <div class="form-group">
            {!! Form::submit('Crear', ['class'=>'btn btn-success']) !!}
            </div>
            {!! Form::close() !!}
        </div>  
</x-adminlte-card>




@endsection
    