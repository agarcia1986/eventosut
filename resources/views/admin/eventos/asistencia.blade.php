@extends('adminlte::page')
@section('title', 'Asistencia a evento')
@section('content_header')
    <h1>Capturar asistencia a evento : {{$evento->nombre}}</h1>
@stop
@section('content')
    @livewire('admin.asistencia')
@stop
@section('js')
<script src="https://unpkg.com/html5-qrcode"></script>
<script>
    //console.log('prueba');
    var resultContainer = document.getElementById('qr-reader-results');
    var lastResult, countResults = 0;
    const eventoId = {!! json_encode($evento['id']) !!};
    //console.log(evento['id']);
    function onScanSuccess(decodedText, decodedResult) {
        if (decodedText !== lastResult) {
            ++countResults;
            lastResult = decodedText;
            console.log(`Scan result: ${decodedText}`, decodedResult);
            //Livewire.emit("asistenciaP",['idparticipante' => decodedResult,'evento'=> eventoId]);
            Livewire.emit('asistenciaP', [decodedText,eventoId]);
            //var arg = {'key': key, 'idEvento': evento,'idparticipante' =decodedResult};           
            //if (result.value) {
              //  Livewire.emit("asistenciaP", arg);
            //}
            //resultContainer.innerHTML += `<p>[${countResults}] - ${decodedText}</p>`;
            //Livewire.emit('asistenciaP',[{'idparticipante' =decodedResult,'evento'=> evento}]);
            

            resultContainer.innerHTML += 'vamo pue' ;
            /*Livewire.on('asistenciaP',(){
                //resultContainer.innerHTML += 'vamo pue' ;
                alert('Asistencia aprobada');
            })*/
            
            
        }
    }
    var html5QrcodeScanner = new Html5QrcodeScanner(
        "qr-reader", { fps: 10, qrbox: 250 });
        html5QrcodeScanner.render(onScanSuccess);
        
</script>
@endsection