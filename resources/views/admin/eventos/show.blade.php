@extends('adminlte::page')
@section('title', 'Eventos UT')

@section('content_header')
    <h1>detalles evento</h1>
@stop
@section('content')
<div class="card">
    <div class="card-body">
        <x-app-layout>
            <!--div class="text-center pt-16 md:pt-32">
                  <p class="text-sm md:text-base text-green-500 font-bold"> <span class="text-gray-900">/</span> GETTING STARTED</p-->
                  <h1 class="font-bold break-normal text-2xl md:text-3xl"></h1>
              </div-->
              <!--IMAGEN-->
              <div class="container w-full max-w-6xl mx-auto bg-white bg-cover mt-1 rounded" style="background-image:url('@if($evento->image){{Storage::url($evento->image->url)}}@else http://www.utcancun.edu.mx/wp-content/uploads/2016/06/Instalaciones-Deportivas-UT-Soccer-Americano.jpg @endif'); height: 75vh;"></div>
              <!--CONTENEDOR-->
              <div class="container max-w-5xl mx-auto -mt-5 opacity-90">
                  <div class="mx-0 sm:mx-6">
                      <div class="bg-white w-full p-8 md:p-18 text-xl md:text-1xl text-gray-800 leading-normal">
                          <!--CONTENIDO-->
                          <p class="text-2xl md:text-2xl mb-1 ">
                              {{$evento->nombre}}
                          </p>
                          <p class="py-1 text-justify ">{!!$evento->informacion!!}</p>				
                          <ol>
                              <li class="py-3">Lugar : {{$evento->setevento->localidad}}</li>
                              <li class="py-3">Locación : {{$evento->setevento->detalle}}</li>
                              <li class="py-3">Periodo de inscripción : Del {{strftime($evento->setevento->iniciainscripcion)}} al {{$evento->setevento->terminainscripcion}}</li>
                              <li class="py-3">Fecha del evento :{{$evento->setevento->fechaevento}}</li>
                          </ol>								
                          <!--BOTON CONVOCATORIA-->
                          <div class="container font-sans bg-white rounded mt-2 p-4 text-center">
                              <div class="w-full text-center pt-4">
                                  <div class="max-w-sm mx-auto p-1 pr-0 flex flex-wrap items-center">
                                      
                                  </div>
                              </div>
                          </div>
                      </div>              
                      
                  </div>
              
          
              </div>
          </x-app-layout>
        
       
    </div>

</div>
    
@stop