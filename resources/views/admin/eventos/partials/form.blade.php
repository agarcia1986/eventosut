<div class="row">
    <div class="form-group col-md-6">
        {!! Form::label('nombre', 'Nombre:', ['class' => 'form-label']) !!}
        {!! Form::text('nombre', null, ['class' => 'form-control', 'placeholder' => 'Ingrese el nombre del evento']) !!}
        @error('nombre')
            <small class="text-danger">{{ $message }}</small>
        @enderror
    </div>
    <div class="form-group col-md-6">
        {!! Form::label('slug', 'Slug:', ['class' => 'form-label']) !!}
        {!! Form::text('slug', null, ['class' => 'form-control', 'placeholder' => 'Ingrese el nombre del evento', 'readonly']) !!}
        @error('slug')
            <small class="text-danger">{{ $message }}</small>
        @enderror
    </div>
</div>
<div class="row">
    <div class="form-group col-md-6">
        {!! Form::label('intro','Introducción: :') !!}
        {!! Form::text('intro', null, ['class'=>'form-control','placeholder'=>'Ingrese el nombre del evento']) !!}
        @error('introon')
            <small class="text-danger">{{$message}}</small>
        @enderror
    </div> 
    <div class="row col-md-3">
        <div class="col">
            <div class="image-wrapper">
                @isset ($evento->image)
                    <img id="picture" src="{{Storage::url($evento->image->url)}}">
                    @else
                    <img  id="picture" src="http://www.utcancun.edu.mx/wp-content/uploads/2016/06/Instalaciones-Deportivas-UT-Soccer-Americano.jpg" alt="" class="img img-rounded">
                @endisset
            </div>
        </div>
    </div> 
    <div class="col-md-3">
            <div class="form-group">
                {!! Form::label('image','Imagen del evento.') !!}
                {!! Form::file('image', null, ['class'=>'btn btn-outline-secondary','accept'=>'image/*']) !!}
                <small id="imageerror"></small>
                @error('image')
                <small  class="text-danger">{{$message}}</small> 
                
                @enderror  
        </div>
    </div>
</div>
<div class="row">
    <div class="form-group col-md-12">
        {!! Form::label('informacion','Información :') !!}
        {!! Form::textarea('informacion', null, ['class'=>'form-control','rows'=>'30','name'=>'informacion']) !!}
        @error('informacion')
            <small class="text-danger">{{$message}}</small>
        @enderror
    </div>
</div>
@livewire('admin.categoria-subcategoria')
<div class="row">
    <div class="form-group col-md-4">
        {!! Form::label('localidad','Lugar del evento :') !!}
        {!! Form::text('localidad', null, ['class'=>'form-control']) !!}
        @error('localidad')
            <small class="text-danger">{{$message}}</small>
        @enderror
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('detalle','Detalles de la ubicación:') !!}
        {!! Form::text('detalle', null, ['class'=>'form-control']) !!}
        @error('detalles')
            <small class="text-danger">{{$message}}</small>
        @enderror
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('archivo','Convocatoria :') !!}
        {!! Form::file('archivo', null, ['class'=>'form-control','accept'=>'*/pdf*']) !!}          
        @error('archivo')
            <small class="text-danger">{{$message}}</small>    
        @enderror  
    </div>
</div>
<div class="row">
    <div class="form-group col-md-4">
        {!! Form::label('capacidad','Capacidad :') !!}
        {!! Form::number('capacidad', null, ['class'=>'form-control']) !!}     
        @error('capacidad')
            <small class="text-danger">{{$message}}</small>
        @enderror
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('requisitos','Requisitos :') !!}
        {!! Form::text('requisitos', null, ['class'=>'form-control']) !!}     
        @error('requisitos')
            <small class="text-danger">{{$message}}</small>
        @enderror
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('video','Video(URL) :') !!}
        {!! Form::text('video', null, ['class'=>'form-control']) !!}
        @error('video')
            <small class="text-danger">{{$message}}</small>
        @enderror
    </div>
</div>
<div class="row">
    <div class="form-group col-md-3">
        {!! Form::label('fechaevento','Fecha del evento:') !!}
        {!! Form::date('fechaevento', null, ['class'=>'form-control']) !!}
        @error('fechaevento')
            <small class="text-danger">{{$message}}</small>
        @enderror
    </div>
    <div class="form-group col-md-3">
        {!! Form::label('horaInicio','Hora :') !!}
        {!! Form::time('horaInicio', null, ['class'=>'form-control']) !!} 
        @error('horaInicio')
            <small class="text-danger">{{$message}}</small>
        @enderror
    </div>
    <div class="form-group col-md-3">
        {!! Form::label('iniciainscripcion','Inicio de inscripción:') !!}
        {!! Form::date('iniciainscripcion', null, ['class'=>'form-control']) !!}
        
        @error('iniciainscripcion')
            <small class="text-danger">{{$message}}</small>
        @enderror
    </div>
    <div class="form-group col-md-3">
        {!! Form::label('terminainscripcion','Termina Inscripción:') !!}
        {!! Form::date('terminainscripcion', null, ['class'=>'form-control']) !!} 
        @error('terminainscripcion')
            <small class="text-danger">{{$message}}</small>
        @enderror
    </div>
</div>
<div class="row">
    <div class="col-md-6  form-group">
        {!! Form::label('patrocinador','Patrocinadores:') !!}
        <div class="form-inline">   
            @foreach ($patrocinadores as $patrocinador)
                <label>
                    {!! Form::checkbox('patrocinador[]', $patrocinador->id, null, ['class' => 'mr-1']) !!}
            </label>
                <div class="thumbnail">
                    <img class="img img-rounded" src="{{Storage::url($patrocinador->image->url)}}" width="60px" height="60px" alt="..." value="{{old('image')}}">
                    <div class="caption">
                        <p class="small">{{$patrocinador->nombre}}</p>
                    </div>    
                </div>  
            @endforeach   
        </div> 
        @error('patrocinador')
            <small class="text-danger">{{$message}}</small>
        @enderror
    </div>
    <div class="col-md-6 form-group">
        <label for="ponente">Ponente:</label> 
        <div class="form-inline">
 
            @foreach ($ponentes as $ponente)
            <div>
                <label class="form-check-label">
                    {!! Form::checkbox('ponente[]', $ponente->id, null, ['class' => 'mr-1']) !!}
                    <div class="thumbnail">
                        <img class="img img-rounded" src="{{Storage::url($ponente->image->url)}}" width="60px" height="60px" alt="..." value="{{old('image')}}">
                        <div class="caption">
                            <p class="small">{{$ponente->nombre}}</p>
                        </div>
                    </div>
                </label>
            </div>
        @endforeach
        </div>     
        @error('ponente')
            <small class="text-danger">{{$message}}</small>
        @enderror
    </div>
    <div class="col-md-12 form-group">
        <label for="ponente">¿Que datos requiere del participante?:</label> 
        <div class="form-inline">
 
            @foreach ($ponentes as $ponente)
            <div>
                <label class="form-check-label">
                    {!! Form::checkbox('participante[]', $ponente->id, null, ['class' => 'mr-1']) !!}
                    <div class="thumbnail">
                        <p class="text-sm">Fecha Nacimiento</p>
                    </div>
                </label>
            </div>
        @endforeach
        </div>     
        @error('ponente')
            <small class="text-danger">{{$message}}</small>
        @enderror
    </div>
</div>
<div class="row">
    <div class="form-group col-md-6">
        <label class="font-weingt-bold"> Estatus: </label>
        <br>
        <div class="form-check form-check-inline ">       
        <label>
            {!! Form::radio('status', 1, true, ['class' => 'mr-1']) !!}
            Borrador
        </label>
                
        </div>
        <div class="form-check form-check-inline ">
            <label>
                {!! Form::radio('status',2,['class' => 'mr-1']) !!}
                Publicado
            </label>
        </div>
        @error('status')
            <small class="text-danger">{{$message}}</small>
        @enderror
    </div>
</div>