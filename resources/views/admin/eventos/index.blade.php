@extends('adminlte::page')
@section('title', 'Eventos UT')

@section('content_header')

<!--a  class="btn btn-outline-success btn-sm float-right"href="{{route('admin.eventos.create')}}">Nuevo Evento</a-->
    <h1>Todos los Eventos</h1>
@stop

@section('content')

@if(session('info'))
    <div class="alert alert-success">
        <strong>{{session('info')}}</strong>
    </div>
@endif
    @livewire('admin.evento-index')
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop