@extends('adminlte::page')
@section('title', 'Eventos UT')

@section('content_header')
    <h1 class="text-center">Crear nuevo evento</h1>
@stop

@section('content')
    <div class="card">
        <div class="card-body">
            <form action="{{route('admin.eventos.store')}}" method="POST" enctype="multipart/form-data">
                @csrf
                @include('admin.eventos.partials.form')
                <div class="row justify-center ">
                    <button type="submit" class="btn btn-success btn-lg btn-block" >Guardar</button>
                </div>
            </form>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet">
    <style>
    .image-wrapper{
        position :relative;
        padding-bottom: 56.25%;
    }
    .image-wrapper img{
        position: absolute;
        object-fit: cover;
        width: 100%;
        height: 100%;
    }
</style>
@stop

@section('js')

        <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script>
<script src="{{asset('vendor/jQuery-Plugin-stringToSlug-1.3/jquery.stringToSlug.min.js')}}"></script>


<script>
     $(document).ready( function() {
    $("#nombre").stringToSlug({
        setEvents: 'keyup keydown blur',
        getPut: '#slug',
        space: '-'
        });
    });
    $('#informacion').summernote({
        placeholder: 'Aqui va toda la informacion importante del evento',
        tabsize: 10,
        height: 200
      });
    //imagen del formulario
    //document.getElementById("image").addEventListener('change',cambiarImagen);
    $("#image").on('change', function() {
                const [file] = this.files
                let pictureResult=document.getElementById("picture");
                let errorLabel= document.getElementById("imageerror");
                if (file) {
                    const img = new Image();
                    img.onload = function() {
                        
                        if(this.width){
                            let cumple= false;
                            if(this.height/this.width < .68 ){
                                if(this.height/this.width > .55)
                                {
                                    pictureResult.setAttribute('src', img.src);
                                    errorLabel.innerHTML = "";
                                    //console.log('cumple con 16-9');    
                                }else{
                                        //console.log('jsnjsnd');
                                        errorLabel.innerHTML = "<p class='text-danger'>La imagen debe ser en formato 16:9 por ejemplo: 1280*850px /1024*576px</p>";
                                }
                            }else{
                                //console.log('la imagen es muy cuadrada');
                                errorLabel.innerHTML = "<p class='text-danger'>La imagen debe ser en formato 16:9 por ejemplo: 1280*850px /1024*576px</p>";
                            }
                        }
                       
                    }
                    img.src = URL.createObjectURL(file);
                }
});
</script>
@stop