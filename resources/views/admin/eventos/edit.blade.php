@extends('adminlte::page')
@section('title', 'Eventos UT')
@section('content_header')
    <h1 class="text-center">Editar evento</h1>
@stop
@section('content')
@if(session('info'))
    <div class="alert alert-success">
        <strong>{{session('info')}}</strong>
    </div>
@endif
<div class="card">
    <div class="card-body">
        {!! Form::model($evento,['route'=> ['admin.eventos.update',$evento],'method' =>'put','enctype'=>'multipart/form-data']) !!}
        <div class="row">
            <div class="form-group col-md-6">
                {!! Form::label('nombre','Nombre :') !!}
                {!! Form::text('nombre', null, ['class'=>'form-control','placeholder'=>'Ingrese el nombre del evento']) !!}
                 @error('nombre')
                    <small class="text-danger">{{$message}}</small>
                @enderror
            </div>
            <div class="form-group col-md-6">
                {!! Form::label('slug','Slug :') !!}
                {!! Form::text('slug', null, ['class'=>'form-control','placeholder'=>'Ingrese el nombre del evento','readonly']) !!}
                @error('slug')
                    <small class="text-danger">{{$message}}</small>
                @enderror
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-6">
                {!! Form::label('intro','Introducción: (Minimo 50- Maximo 360 Caracteres)') !!}
                {!! Form::text('intro', null, ['class'=>'form-control']) !!}
                @error('intro')
                    <small class="text-danger">{{$message}}</small>
                @enderror
            </div> 
            <div class="row col-md-3">
                <div class="col">
                    <div class="image-wrapper">
                        @isset ($evento->image)
                            <img id="picture" src="{{Storage::url($evento->image->url)}}">
                            @else
                            <img  id="picture" src="http://www.utcancun.edu.mx/wp-content/uploads/2016/06/Instalaciones-Deportivas-UT-Soccer-Americano.jpg" alt="" class="img img-rounded">
                        @endisset
                    </div>
                </div>
            </div> 
            <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('image','Imagen del evento.') !!}
                        {!! Form::file('image', null, ['class'=>'form-control','accept'=>'image/*']) !!}
                        <small id="imageerror"></small>
                        @error('image')
                        <small class="text-danger">{{$message}}</small>    
                        @enderror  
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-12">
                {!! Form::label('informacion','Información :') !!}
                {!! Form::textarea('informacion', null, ['class'=>'form-control','rows'=>'10','name'=>'informacion']) !!}
                @error('informacion')
                    <small class="text-danger">{{$message}}</small>
                @enderror
            </div>
        </div>
        @livewire('admin.categoria-subcategoria')
        <div class="row">
            <div class="form-group col-md-4">
                {!! Form::label('localidad','Lugar del evento :') !!}
                {!! Form::text('localidad', $setevento->localidad, ['class'=>'form-control']) !!}
                @error('localidad')
                    <small class="text-danger">{{$message}}</small>
                @enderror
            </div>
            <div class="form-group col-md-4">
                {!! Form::label('detalle','Detalles de la ubicación:') !!}
                {!! Form::text('detalle', $setevento->detalle, ['class'=>'form-control']) !!}
                @error('detalles')
                    <small class="text-danger">{{$message}}</small>
                @enderror
            </div>
            <div class="form-group col-md-4">
                {!! Form::label('archivo','Convocatoria :') !!}
                {!! Form::file('archivo', $setevento->archivo, ['class'=>'form-control','accept'=>'*/pdf*']) !!}          
                @error('archivo')
                    <small class="text-danger">{{$message}}</small>    
                @enderror  
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-4">
                {!! Form::label('capacidad','Capacidad :') !!}
                {!! Form::number('capacidad',$setevento->capacidad, ['class'=>'form-control']) !!}     
                @error('capacidad')
                    <small class="text-danger">{{$message}}</small>
                @enderror
            </div>
            <div class="form-group col-md-4">
                {!! Form::label('requisitos','Requisitos :') !!}
                {!! Form::text('requisitos', $setevento->requisitos, ['class'=>'form-control']) !!}     
                @error('requisitos')
                    <small class="text-danger">{{$message}}</small>
                @enderror
            </div>
            <div class="form-group col-md-4">
                {!! Form::label('video','Video(URL) :') !!}
                {!! Form::text('video',  $setevento->video, ['class'=>'form-control']) !!}
                @error('video')
                    <small class="text-danger">{{$message}}</small>
                @enderror
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-3">
                {!! Form::label('fechaevento','Fecha del evento:') !!}
                {!! Form::date('fechaevento', $setevento->fechaevento, ['class'=>'form-control']) !!}
                @error('fechaevento')
                    <small class="text-danger">{{$message}}</small>
                @enderror
            </div>
            <div class="form-group col-md-3">
                {!! Form::label('horaInicio','Hora :') !!}
                {!! Form::time('horaInicio', $setevento->horaInicio, ['class'=>'form-control']) !!} 
                @error('horaInicio')
                    <small class="text-danger">{{$message}}</small>
                @enderror
            </div>
            <div class="form-group col-md-3">
                {!! Form::label('iniciainscripcion','Inicio de inscripción:') !!}
                {!! Form::date('iniciainscripcion', $setevento->iniciainscripcion, ['class'=>'form-control']) !!}
                
                @error('iniciainscripcion')
                    <small class="text-danger">{{$message}}</small>
                @enderror
            </div>
            <div class="form-group col-md-3">
                {!! Form::label('terminainscripcion','Termina Inscripción:') !!}
                {!! Form::date('terminainscripcion', $setevento->terminainscripcion, ['class'=>'form-control']) !!} 
                @error('terminainscripcion')
                    <small class="text-danger">{{$message}}</small>
                @enderror
            </div>
        </div>
        <div class="row">
            <div class="col-md-6  form-group">
                {!! Form::label('patrocinador','Patrocinadores:') !!}
                <div class="form-inline">   
                    @foreach ($patrocinadores as $patrocinador)
                        <label>
                            {!! Form::checkbox('patrocinador[]', $patrocinador->id, null, ['class' => 'mr-1']) !!}
                    </label>
                        <div class="thumbnail">
                            <img class="img img-rounded" src="{{Storage::url($patrocinador->image->url)}}" width="60px" height="60px" alt="..." value="{{old('image')}}">
                            <div class="caption">
                                <p class="small">{{$patrocinador->nombre}}</p>
                            </div>    
                        </div>  
                    @endforeach   
                </div> 
                @error('patrocinador')
                    <small class="text-danger">{{$message}}</small>
                @enderror
            </div>
            <div class="col-md-6 form-group">
                <label for="ponente">Ponente:</label> 
                <div class="form-inline">
         
                    @foreach ($ponentesall as $ponente)
                    <div>
                        <label class="form-check-label">
                            {!! Form::checkbox('ponente[]', $ponente->id, null, ['class' => 'mr-1']) !!}
                            <div class="thumbnail">
                                <img class="img img-rounded" src="{{Storage::url($ponente->image->url)}}" width="60px" height="60px" alt="..." value="{{old('image')}}">
                                <div class="caption">
                                    <p class="small">{{$ponente->nombre}}</p>
                                </div>
                            </div>
                        </label>
                    </div>
                @endforeach
                </div>     
                @error('ponente')
                    <small class="text-danger">{{$message}}</small>
                @enderror
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-6">
                <label class="font-weingt-bold"> Estatus: </label>
                <br>
                <div class="form-check form-check-inline ">       
                <label>
                    {!! Form::radio('status', 1, true, ['class' => 'mr-1']) !!}
                    Borrador
                </label>
                        
                </div>
                <div class="form-check form-check-inline ">
                    <label>
                        {!! Form::radio('status',2,['class' => 'mr-1']) !!}
                        Publicado
                    </label>
                </div>
                @error('status')
                    <small class="text-danger">{{$message}}</small>
                @enderror
            </div>
        </div>
            {!! Form::submit('Actualizar', ['class'=>'btn btn-success btn-lg btn-block']) !!}
        {!! Form::close() !!}    
       
    </div>
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <style>
        .image-wrapper{
            position :relative;
            padding-bottom: 56.25%;
        }
        .image-wrapper img{
            position: absolute;
            object-fit: cover;
            width: 100%;
            height: 100%;
        }
    </style>
@stop

@section('js')
<script src="https://cdn.ckeditor.com/ckeditor5/38.0.1/classic/ckeditor.js"></script>
    <script src="{{asset('vendor/jQuery-Plugin-stringToSlug-1.3/jquery.stringToSlug.min.js')}}"></script>
<script>
    $(document).ready( function() {
    $("#nombre").stringToSlug({
        setEvents: 'keyup keydown blur',
        getPut: '#slug',
        space: '-'
        });
    });
ClassicEditor
    .create( document.querySelector( '#informacion' ) )
    .catch( error => {
        console.error( error );
    } );
    /*imagen del formulario
    document.getElementById("image").addEventListener('change',cambiarImagen);
    function cambiarImagen(event){
        var file= event.target.files[0];
        var reader = new FileReader();
        reader.onload = (event) => {
            document.getElementById("picture").setAttribute('src', event.target.result);
        };
        reader.readAsDataURL(file);
    }*/
    $("#image").on('change', function() {
                const [file] = this.files
                let pictureResult=document.getElementById("picture");
                let errorLabel= document.getElementById("imageerror");
                if (file) {
                    const img = new Image();
                    img.onload = function() {
                        
                        if(this.width){
                            let cumple= false;
                            if(this.height/this.width < .68 ){
                                if(this.height/this.width > .55)
                                {
                                    pictureResult.setAttribute('src', img.src);
                                    errorLabel.innerHTML = "";
                                    //console.log('cumple con 16-9');    
                                }else{
                                        //console.log('jsnjsnd');
                                        errorLabel.innerHTML = "<p class='text-danger'>La imagen debe ser en formato 16:9 por ejemplo: 1280*850px /1024*576px</p>";
                                        

                                }
                            }else{
                                //console.log('la imagen es muy cuadrada');
                                errorLabel.innerHTML = "<p class='text-danger'>La imagen debe ser en formato 16:9 por ejemplo: 1280*850px /1024*576px</p>";
                            }
                        }
                       
                    }
                    img.src = URL.createObjectURL(file);
                }
});
    document.getElementById("archivo").addEventListener('change',cambiarArchivo);
    function cambiarArchivo(event){
        var file= event.target.files[0];
        var reader = new FileReader();
        reader.onload = (event) => {
            document.getElementById("archive").setAttribute('src', event.target.result);
        };
        reader.readAsDataURL(file);
    }
</script>
@stop