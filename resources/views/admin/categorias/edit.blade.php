@extends('adminlte::page')
@section('title', 'Eventos UT')

@section('content_header')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{route('admin.categorias.index')}}">Categorias</a></li>
      <li class="breadcrumb-item active" aria-current="page">Subcategorias</li>
    </ol>
  </nav>
    <h1>Editar Categoría</h1>
@stop

@section('content')
@if(session('info'))
    <div class="alert alert-success">
        <strong>{{session('info')}}</strong>
    </div>
@endif
<div class="card">
   
    <div class="card-body">
        {!! Form::model($categoria,['route'=> ['admin.categorias.update',$categoria],'method' =>'put']) !!}
            @include('admin.categorias.partials.form')
            {!! Form::submit('Actualizar', ['class'=>'btn btn-primary']) !!}
        {!! Form::close() !!}

    </div>
</div>
@stop
@section('js')
    <script src="{{asset('vendor/jQuery-Plugin-stringToSlug-1.3/jquery.stringToSlug.min.js')}}"></script>
    <script>
        $(document).ready( function() {
        $("#nombre").stringToSlug({
            setEvents: 'keyup keydown blur',
            getPut: '#slug',
            space: '-'
        });
});
    </script>
@endsection

