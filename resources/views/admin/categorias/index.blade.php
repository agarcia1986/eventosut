@extends('adminlte::page')
@section('title', 'Eventos UT')

@section('content_header')
    <h1>Lista de categorías</h1>
@stop

@section('content')

@if(session('info'))
    <div class="alert alert-success">
        <strong>{{session('info')}}</strong>
    </div>
@endif
<x-adminlte-card>
    
        <div class="card-header">
            <a class="btn btn-outline-secondary float-right" href="{{route('admin.categorias.create')}}">Nuevo</a>
        </div>
        <div class="card-body">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th colspan="2"></th>

                    </tr>
                </thead>
                <tbody>
                    
                     @foreach ($categorias as $categoria)
                    <tr>
                       
                       <td>{{$categoria->nombre}}</td>
                       <td width="10px">
                           <a class="btn btn-outline-primary btn-sm" href="{{route('admin.categorias.edit',$categoria)}}">Editar</a>
                        </td>
                       <td width="10px">
                           <form action="{{route('admin.categorias.destroy',$categoria)}}" method="POST">
                            @csrf
                            @method('delete')
                            <button type="submit" class="btn btn-outline-danger btn-sm">Eliminar</button>
                        </form>
                       </td>
                    </tr>    
                    @endforeach   
                </tbody>
                
            </table>
        </div>
    
</x-adminlte-card>
@stop

