<div class="row">
<div class="form-group col-md-6">
    {!! Form::label('nombre','Nombre') !!}
    {!! Form::text('nombre', null, ['class'=>'form-control','placeholder'=>'Ingrese el nombre de la categoria']) !!}
    @error('nombre')
        <span class="text-danger">{{$message}}</span>
    @enderror
</div>                
<div class="form-group col-md-6">
    {!! Form::label('slug','Slug') !!}
    {!! Form::text('slug', null, ['class'=>'form-control','placeholder'=>'Ingrese el slug de la categoria','readonly']) !!}
    @error('slug')
        <span class="text-danger">{{$message}}</span>
    @enderror     
</div>
</div>
@foreach ($subcategorias as $subcategoria)
     <div>
         <label class="form-check-label">
             {!! Form::checkbox('subcategorias[]', $subcategoria->id, null, ['class' => 'mr-1']) !!}
             {{$subcategoria->nombre}}
         </label>
     </div>
 @endforeach