@extends('adminlte::page')
@section('title', 'Eventos UT')

@section('content_header')
    <h1>Información del Usuario.</h1>
@stop

@section('content')
    <div class="card">
       <div class="card-body">
         <div class="row">
            <div class="panel panel-success">
               <div class="panel-heading">Panel heading without title</div>
               <div class="panel-body">
                  <h5>Nombre : {{auth()->user()->name}}</h3>
                  <p>Email : {{auth()->user()->email}}</p>
                  <p>Usuario desde : {{auth()->user()->created_at}}</p>
               </div>
             </div>
         </div>
         @if ($participante != 0)
         <div class="row">
            <div class="panel panel-success">
               <div class="panel-heading">van los datos del participante</div>
               <div class="panel-body">         
               </div>
            </div>
         </div>

         @else
         <div class="row">
            <div class="panel panel-success">
               <div class="panel-heading">Ahora hay que llenar los datos de participante</div>
               <div class="panel-body">         
               </div>
            </div>
         </div>

         @endif
          
             
            
           
         </div>
       </div>
    </div>
@stop
