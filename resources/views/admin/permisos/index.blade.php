@extends('adminlte::page')
@section('title', 'Eventos UT')

@section('content_header')
    <h1>Lista de Permisos</h1>
@stop
@section('content')
    @if (session('info'))
    <x-adminlte-alert theme="info" title="Hecho" dismissable>
        {{session('info')}}
    </x-adminlte-alert>
    @endif

    @livewire('admin.permisos-index')

@stop

