@extends('adminlte::page')
@section('title', 'Eventos UT')

@section('content_header')
    <h1>Crear Permiso</h1>
@stop

@section('content')
<x-adminlte-card theme="lime" theme-mode="outline">
    <div class="card-body">
        {!! Form::open(['route' =>'admin.permisos.store']) !!}
        <div class="form-group">
         {!! Form::label('name', 'Nombre') !!}
         {!! Form::text('name', null, ['class'=>'form-control']) !!}
         @error('name')
             <small id="passwordHelpBlock" class="form-text text-danger">{{$message}}</small>
             
         @enderror
        </div>
        <div class="form-group">
         {!! Form::label('description', 'Descripción') !!}
         {!! Form::text('description', null, ['class'=>'form-control']) !!}
         @error('description')
         <small id="passwordHelpBlock" class="form-text text-danger">{{$message}}</small> 
         @enderror
        </div>
        {!! Form::submit('Crear permiso', ['class' => 'btn btn-outline-primary']) !!}
        {!! Form::close() !!}

    </div>
</x-adminlte-card>
  
@stop

