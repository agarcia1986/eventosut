@extends('adminlte::page')
@section('title', 'Lista Ponentes')

@section('content_header')
    <br>
@stop

@section('content')
    @if(session('info'))
        <div class="alert alert-success">
            <strong>{{session('info')}}</strong>
        </div>
    @endif
    <x-adminlte-card title="Editar ponente" theme="success" theme-mode="outline" body-class="bg-none" header-class="bg-light"  footer-class="bg-none border-top rounded border-light"
    icon="fas fa-lg fa-plus" >
    
    <form action="{{route('admin.ponentes.update',$ponente)}}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="row">
            <div class="form-group col-md-6">
                <label for="">Nombre completo :</label>
                <input class="form-control @error('nombre') is-invalid @enderror" type="text" name="nombre" id="nombre" value="{{$ponente->nombre}}">
                @error('nombre')
                    <p class="text-danger">{{$message}}</p>
                @enderror
            </div>
            <div class="form-group col-md-6">
                <label for="">Nivel Estudios :</label>
                <select name="nivelestudio" id="nivelestudio" class="form-control" value="{{$ponente->nivestudio}}">
                    @foreach ($grados as $grado)
                    <option value="{{$grado->grado}}">{{$grado->grado}}</option>
                    @endforeach
                </select>
                
                <!--input class="form-control @error('nivelestudio') is-invalid @enderror" type="text" name="nivelestudio" id="nivelestudio" value="{{ old('nivelestudio') }}"-->
                @error('nivelestudio')
                    <p class="text-danger">{{$message}}</p>
                @enderror
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-6">
                <label for="">Empresa:</label>
                <input class="form-control @error('compania') is-invalid @enderror" type="text" name="compania" id="compania" value="{{$ponente->compania}}">
                @error('compania')
                    <p class="text-danger">{{$message}}</p>
                @enderror
            </div>
            <div class="form-group col-md-6">
                <label for="">Ponencia :</label>
                <input class="form-control @error('ponencia') is-invalid @enderror" type="text" name="ponencia" id="ponencia" value="{{$ponente->ponencia}}">
                @error('ponencia')
                    <p class="text-danger">{{$message}}</p>
                @enderror
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-6">
                <label for="">Puesto</label>
                <input class="form-control @error('puesto') is-invalid @enderror" type="text" name="puesto" id="puesto" value="{{$ponente->puesto}}">
                @error('puesto')
                    <p class="text-danger">{{$message}}</p>
                @enderror
            </div>
            <div class="form-group col-md-3 align-items-center">
                <div class="row">
                    <div class="form-group col-md-10">
                        <label for="">Foto :</label>
                    <input class="form-control-file" type="file" name="profile_photo" id="profile_photo">
                    <p class="text-danger">*Las imagenes deben ser en formato PNG, sin fondo y en un tamaño de 400 * 400 pixeles</p>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <div class="image-wrapper">
                                <img class="img img-rounded" id="picture" src="@if($ponente->image){{Storage::url($ponente->image->url)}} @else {{Storage::url('patrocinadores/empty.png')}} @endif" height="200px" width="200px" alt="">
                            </div> 
                        </div>
                    </div>
                </div>    
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-12 text-center">
                <button type="submit" class="btn btn-success btn-lg">Guardar</button>
            </div>
        </div>
    </form>
    </x-adminlte-card>  
  
           
@stop
@section('js')
<script src="https://cdn.ckeditor.com/ckeditor5/34.0.0/classic/ckeditor.js"></script>
<script src="{{asset('vendor/jQuery-Plugin-stringToSlug-1.3/jquery.stringToSlug.min.js')}}"></script>
<script>
    //imagen del formulario
    document.getElementById("profile_photo").addEventListener('change',cambiarImagen);
    function cambiarImagen(event){
        var file= event.target.files[0];
        var reader = new FileReader();
        reader.onload = (event) => {
            document.getElementById("picture").setAttribute('src', event.target.result);
        };
        reader.readAsDataURL(file);
    }
</script>
@stop
