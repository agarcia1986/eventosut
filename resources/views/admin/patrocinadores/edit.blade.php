@extends('adminlte::page')
@section('title', 'Lista Patrocinadores')

@section('content_header')
    <br>
@stop

@section('content')
    @if(session('info'))
        <div class="alert alert-success">
            <strong>{{session('info')}}</strong>
        </div>
    @endif
    <x-adminlte-card title="Crear patrocinador" theme="success" theme-mode="outline" body-class="bg-none" header-class="bg-light"  footer-class="bg-none border-top rounded border-light"
    icon="fas fa-lg fa-plus" >
    <form action="{{route('admin.patrocinadores.update', $patrocinadore->id)}}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="row">
            <div class="form-group col-md-6">
                <label for="">Nombre</label>
                <input class="form-control @error('nombre') is-invalid @enderror" type="text" name="nombre" id="nombre" value="{{$patrocinadore->nombre}}" placeholder="prueba">
                @error('nombre')
                    <p class="text-danger">{{$message}}</p>
                @enderror
            </div>
            <div class="form-group col-md-6">
                <label for="">Descripción</label>
                <input class="form-control @error('descripcion') is-invalid @enderror" type="text" name="descripcion" id="descripcion" value="{{$patrocinadore->descripcion}}">
                @error('descripcion')
                    <p class="text-danger">{{$message}}</p>
                @enderror
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-6">
                <label for="">Pagina Web</label>
                <input class="form-control @error('pagina_web') is-invalid @enderror" type="text" name="pagina_web" id="pagina_web" value="{{ $patrocinadore->pagina_web }}">
                @error('pagina_web')
                    <p class="text-danger">{{$message}}</p>
                @enderror
            </div>
            <div class="form-group col-md-3 align-items-center">
                <div class="row">
                    <div class="form-group col-md-10">
                        <label for="">Logo :</label>
                    <input class="form-control-file" type="file" name="logo" id="logo" placeholder="logo">
                    <p class="text-danger">*Las imagenes deben ser en formato PNG, sin fondo y en un tamaño de 400 * 400 pixeles</p>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <div class="image-wrapper">
                                    <img class="img img-rounded" id="picture" src="@if($patrocinadore->image){{Storage::url($patrocinadore->image->url)}} @else {{Storage::url('patrocinadores/empty.png')}} @endif" height="200px" width="200px" alt="">
                            </div> 
                        </div>
                    </div>
                </div>    
            </div>
        </div>
        <div class="row">
        @foreach (json_decode($patrocinadore->redes) as $item=>$data)
        <div class="col-md-1">
            <label for="">{{$item}}</label>
        </div>
        <div class="col-md-5 form-group">
            <div class="col-auto">
                <label class="sr-only" for="inlineFormInputGroup">Username</label>
                <div class="input-group mb-2">
                <div class="input-group-prepend">
                    <div class="input-group-text">www.{{$item}}.com/</div>
                </div>
                <input type="text" class="form-control" id="{{$item}}" name="{{$item}}" value="{{$data}}">
                </div>
            </div>
        </div>
        @endforeach
        </div>  
        <div class="row">
            <div class="col-md-12 text-center">
                <a href="{{route('admin.patrocinadores.index')}}" class="btn btn-danger btn-lg">Regresar</a>
                <button type="submit" class="btn btn-success btn-lg">Actualizar</button>
            </div>
        </div>
    </form>

    </x-adminlte-card>  

@stop
@section('js')
<script src="https://cdn.ckeditor.com/ckeditor5/34.0.0/classic/ckeditor.js"></script>
<script src="{{asset('vendor/jQuery-Plugin-stringToSlug-1.3/jquery.stringToSlug.min.js')}}"></script>
<script>
    //imagen del formulario
    document.getElementById("logo").addEventListener('change',cambiarImagen);
    function cambiarImagen(event){
        var file= event.target.files[0];
        var reader = new FileReader();
        reader.onload = (event) => {
            document.getElementById("picture").setAttribute('src', event.target.result);
        };
        reader.readAsDataURL(file);
    }
</script>
@stop
