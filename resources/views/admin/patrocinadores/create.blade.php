@extends('adminlte::page')
@section('title', 'Lista Patrocinadores')

@section('content_header')
    <br>
@stop

@section('content')
    @if(session('info'))
        <div class="alert alert-success">
            <strong>{{session('info')}}</strong>
        </div>
    @endif
    <x-adminlte-card title="Crear patrocinador" theme="success" theme-mode="outline" body-class="bg-none" header-class="bg-light"  footer-class="bg-none border-top rounded border-light"
    icon="fas fa-lg fa-plus" >
    
    <form action="{{route('admin.patrocinadores.store')}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="form-group col-md-6">
                <label for="">Nombre</label>
                <input class="form-control @error('nombre') is-invalid @enderror" type="text" name="nombre" id="nombre" value="{{ old('nombre') }}">
                @error('nombre')
                    <p class="text-danger">{{$message}}</p>
                @enderror
            </div>
            <div class="form-group col-md-6">
                <label for="">Descripción</label>
                <input class="form-control @error('descripcion') is-invalid @enderror" type="text" name="descripcion" id="descripcion" value="{{ old('descripcion') }}">
                @error('descripcion')
                    <p class="text-danger">{{$message}}</p>
                @enderror
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-6">
                <label for="">Pagina Web</label>
                <input class="form-control @error('pagina_web') is-invalid @enderror" type="text" name="pagina_web" id="pagina_web" value="{{ old('pagina_web') }}">
                @error('pagina_web')
                    <p class="text-danger">{{$message}}</p>
                @enderror
            </div>
            <div class="form-group col-md-3 align-items-center">
                <div class="row">
                    <div class="form-group col-md-10">
                        <label for="">Logo :</label>
                    <input class="form-control-file" type="file" name="logo" id="logo">
                    <p class="text-danger">*Las imagenes deben ser en formato PNG, sin fondo y en un tamaño de 400 * 400 pixeles</p>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <div class="image-wrapper">
                                    <img class="img img-rounded" id="picture" src="http://www.utcancun.edu.mx/wp-content/uploads/2016/06/Instalaciones-Deportivas-UT-Soccer-Americano.jpg" height="200px" width="200px" alt="">
                            </div> 
                        </div>
                    </div>
                </div>    
            </div>
        </div>
        <div class="row">
            <div class="col-md-1">
                <label for="">Facebook</label>
            </div>
            <div class="col-md-5 form-group">
                <div class="col-auto">
                    <label class="sr-only" for="inlineFormInputGroup">Username</label>
                    <div class="input-group mb-2">
                    <div class="input-group-prepend">
                        <div class="input-group-text">www.facebook.com/</div>
                    </div>
                    <input type="text" class="form-control" id="facebook" name="facebook" placeholder="Username">
                    </div>
                </div>
            </div>
            <div class="col-md-1">
                <label for="">Instagram</label>
            </div>
            <div class="col-md-5 form-group"> 
                
                <div class="col-auto">
                    <label class="sr-only" for="inlineFormInputGroup">Username</label>
                    <div class="input-group mb-2">
                    <div class="input-group-prepend">
                        <div class="input-group-text">@</div>
                    </div>
                    <input type="text" class="form-control" id="instagram" name="instagram" placeholder="Username">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-1">
                <label for="">Tik Tok</label>
            </div>
            <div class="col-md-5 form-group">
                <div class="col-auto">
                    <label class="sr-only" for="inlineFormInputGroup">Username</label>
                    <div class="input-group mb-2">
                    <div class="input-group-prepend">
                        <div class="input-group-text">@</div>
                    </div>
                    <input type="text" class="form-control" id="tiktok" name="tiktok" placeholder="Username">
                    </div>
                </div>
            </div>
            <div class="col-md-1">
                <label for="">X</label>
            </div>
            <div class="col-md-5 form-group">
            
                <div class="col-auto">
                    <label class="sr-only" for="inlineFormInputGroup">Username</label>
                    <div class="input-group mb-2">
                    <div class="input-group-prepend">
                        <div class="input-group-text"> https://x.com/</div>
                    </div>
                    <input type="text" class="form-control" id="x" name="x" placeholder="Username">
                    </div>
                </div>
                
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <button type="submit" class="btn btn-success btn-lg">Guardar</button>
            </div>
        </div>
    </form>

    </x-adminlte-card>  

@stop
@section('js')
<script src="https://cdn.ckeditor.com/ckeditor5/34.0.0/classic/ckeditor.js"></script>
<script src="{{asset('vendor/jQuery-Plugin-stringToSlug-1.3/jquery.stringToSlug.min.js')}}"></script>
<script>
    //imagen del formulario
    document.getElementById("logo").addEventListener('change',cambiarImagen);
    function cambiarImagen(event){
        var file= event.target.files[0];
        var reader = new FileReader();
        reader.onload = (event) => {
            document.getElementById("picture").setAttribute('src', event.target.result);
        };
        reader.readAsDataURL(file);
    }
</script>
@stop
