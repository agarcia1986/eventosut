@extends('adminlte::page')
@section('title', 'Lista Patrocinadores')

@section('content_header')
    <br>
@stop

@section('content')
    @if(session('info'))
        <div class="alert alert-success">
            <strong>{{session('info')}}</strong>
        </div>
    @endif
   
            @livewire('admin.patrocinadores-index') 

@stop
