@extends('adminlte::page')
@section('title', 'Editar pagos')

@section('content_header')
    <h1>Editar pagos</h1>
@stop

@section('content')

@if(session('info'))
    <div class="alert alert-success">
        <strong>{{session('info')}}</strong>
    </div>
@endif
<div class="card">
    
    <div class="card-body">
        {!! Form::model($pago,['route'=>['admin.pagos.update', $pago], 'method' => 'put']) !!}
        @include('admin.pagos.partials.form')
        {!! Form::submit('Actualizar', ['class'=>'btn btn-primary']) !!}
        {!! Form::close() !!}
    </div>
</div>
@stop

@section('css')
<link rel="stylesheet" href="//code.jquery.com/ui/1.13.1/themes/base/jquery-ui.css">
@endsection
@section('js')
<script src="https://code.jquery.com/ui/1.13.1/jquery-ui.js"></script>
<script>
    $( function() {
        $( "#fechavencimiento" ).datepicker();
    });
</script>
@endsection