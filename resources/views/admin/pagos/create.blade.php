@extends('adminlte::page')
@section('title', 'Pagos')

@section('content_header')
    <h1>Crear Pago</h1>
@stop

@section('content')

@if(session('info'))
    <div class="alert alert-success">
        <strong>{{session('info')}}</strong>
    </div>
@endif
    <div class="card">
        <div class="card-body">
            {!! Form::open(['route'=>'admin.pagos.store']) !!}
            @include('admin.pagos.partials.form')
            {!! Form::submit('Asignar', ['class'=>'btn btn-primary']) !!}
            {!! Form::close() !!}
        </div>
    </div>
@stop
@section('css')
<link rel="stylesheet" href="//code.jquery.com/ui/1.13.1/themes/base/jquery-ui.css">
@endsection
@section('js')
<script src="https://code.jquery.com/ui/1.13.1/jquery-ui.js"></script>
<script>
    $( function() {
        $( "#fechavencimiento" ).datepicker();
    });
</script>
@endsection

