@extends('adminlte::page')
@section('title', 'Lista Pagos')

@section('content_header')
    <br>
@stop

@section('content')
    @if(session('info'))
        <div class="alert alert-success">
            <strong>{{session('info')}}</strong>
        </div>
    @endif
    <x-adminlte-card title="Pagos" theme="success" theme-mode="outline" body-class="bg-none" header-class="bg-light"  footer-class="bg-none border-top rounded border-light"
    icon="fas fa-lg fa-plus" >
        <div class="row">
            @livewire('admin.pagos-evento') 
            
        </div>
    </x-adminlte-card>
@stop
