@extends('adminlte::page')
@section('title', 'Detalle pago')

@section('content_header')
    <h1>Detalle pago</h1>
@stop

@section('content')
<div class="card">
    <div class="card-body">
        <x-adminlte-card title="Informacion del pago" theme="lightblue" theme-mode="outline"
            icon="fas fa-solid fa-info" header-class="text-uppercase rounded-bottom border-info">
            <div class="row">
                <div class="col-md-4"><h4>Nombre: </h4></div>
                <div class="col-md-4"> {{$user->name.' '.$user->pname.' '.$user->mname}}</div>
                
                <div class="col-md-4">
                    @switch($pago->estatuspago)
                        @case(2)
                       
                        <a href="{{route('admin.pagos.aprobarpago', $pago)}}" class="btn btn-outline-success">Aceptar</a>
                        <a href="{{route('admin.pagos.declinarpago', $pago)}}" class="btn btn-outline-danger">Cancelar</a>
                            @break
                            
                        @case(3)
                        <x-adminlte-alert class="bg-teal text-uppercase" icon="fa fa-lg fa-thumbs-up" title="Terminado">
                            Pago completado
                        </x-adminlte-alert>
                            @break
                        @default
                        <x-adminlte-alert theme="warning" title="Pendiente">
                            En espera del comprobante
                        </x-adminlte-alert>
                    @endswitch
                </div>
            </div>
            <div class="row">
                <div class="col-md-4"><h4>Referencia: </h4></div>
                <div class="col-md-4">{{$pago->referencia}}</div>
            </div>
            <div class="row">
                <div class="col-md-4"><h4>Monto :</h4></div>
                <div class="col-md-4">${{$pago->monto}}.00</div>
            </div>
            <div class="row">
                <div class="col-md-4"><h4><h4>Comprobante de pago: </h4></h4></div> 
            </div>
            <figure>
                
                <div class="col-md-4"><img src="@if($pago->image){{Storage::url($pago->image->url)}}@else {{Storage::url('tickets/nomoney.jpg')}} @endif?w=400&h=225" alt=""></div>
            </figure>
           
        </x-adminlte-card>
       
    </div>
</div>   



@stop
