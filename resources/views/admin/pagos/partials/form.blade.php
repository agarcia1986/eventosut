<div class="form-group">
    {!! Form::label('referencia', 'Referencia:') !!}
    {!! Form::text('referencia',null, ['class'=> 'form-control', 'placeholder'=>'Ingrese la referencia']) !!}
</div>

<div class="form-group">
    {!! Form::label('fechavencimiento', 'Fecha de Vencimiento:') !!}
    {!! Form::text('fechavencimiento',null, ['class'=> 'form-control', 'placeholder'=>'Ingrese la fecha']) !!}
   
</div>
<div class="form-group">
    {!! Form::label('monto', 'Monto:') !!}
        <div class="input-group">
        <span class="input-group-text">$</span>
        {!! Form::text('monto',null, ['class'=> 'form-control', 'placeholder'=>'Ingrese el monto']) !!}
        <span class="input-group-text">.00</span>
    </div>
</div>
<div class="form-group">
    {!! Form::label('numtarjeta', 'No. Tarjeta:') !!}
    {!! Form::text('numtarjeta',null, ['class'=> 'form-control', 'placeholder'=>'Ingrese numero de tarjeta']) !!}
</div>
<div class="form-group">
    {!! Form::label('user_id', 'Participante :') !!}
    {!! Form::select('user_id', $usuario, null, ['class'=>'form-control']) !!}
   
</div>
<div class="form-group">
    {!! Form::label('tipo', 'Tipo:') !!}
    {!! Form::select('tipo', $tipos, null, ['class'=>'form-control']) !!}
   
</div>