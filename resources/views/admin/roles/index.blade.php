@extends('adminlte::page')
@section('title', 'Eventos UT')

@section('content_header')
    <h1>Lista de Roles</h1>
@stop

@section('content')
@if (session('info'))
    <div class="alert alert-success">
        {{session('info')}}
    </div>
@endif
<div class="card">
    <div class="card-header">
            <a class="btn btn-outline-secondary float-right" href="{{route('admin.roles.create')}}">Nuevo</a>
    </div>
    <div class="card-body">
        <table class="table table-striped">
            <thead>
                <tr>
                   <th>id</th>
                   <th>Rol</th>
                   <th colspan="2"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($roles as $rol)
                <tr>
                    <td>{{$rol->id}}</td>
                    <td>{{$rol->name}}</td>
                    <td width="10px">
                        <a class="btn btn-outline-primary btn-sm" href="{{route('admin.roles.edit', $rol)}}">Editar</a>
                    </td>
                    <td width="10px">
                        <form action="{{route('admin.roles.destroy',$rol)}}" method="POST">
                         @csrf
                         @method('delete')
                         <button type="submit" class="btn btn-outline-danger btn-sm">Eliminar</button>
                     </form>
                    </td>
                </tr>
                @endforeach

            </tbody>
        </table>

    </div>

</div>
@stop

