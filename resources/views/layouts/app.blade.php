<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name', 'Eventos UT') }}</title>
        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">
        <!-- Styles -->
        <link rel="stylesheet" href="{{ mix('css/app.css') }}">
        
        
        @livewireStyles
        <!-- Scripts -->
        <script src="{{ mix('js/app.js') }}" defer></script>
        
    </head>
    <body class="font-sans antialiased bg-utfondo">      
        <!--div class="min-h-screen bg-gray-100"-->
            @livewire('navigation')
            <br>
            
           
            <!-- Page Content -->
            <main>
                <!--Nuevo, prueba de todo en tailwindcss
                <div class="grid grid-rows-3 grid-flow-col gap-2">
                    
                Nuevo, prueba de todo en tailwindcss-->
                {{ $slot }}
                
            </main>           
        <!--/div-->
        @livewire('footer')
        @stack('modals')
        @livewireScripts
        <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.3/dist/jquery.min.js"></script>">
    </body>
</html>
