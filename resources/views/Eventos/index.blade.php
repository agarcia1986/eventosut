<x-app-layout>
  <section class="bg-gray-200 dark:text-gray-100 mt-[-30px] pt-0">
    <div class="container max-w-5xl p-6 mx-auto space-y-6 sm:space-y-12 rounded-sm">
      @foreach ($eventos as $evento)
        @if($loop ->first)  <!---Primera imagen-->
          <a rel="noopener noreferrer" href="{{route('eventos.show', $evento) }}" class="block max-w-sm group hover:no-underline focus:no-underline gap-3 mx-auto sm:max-w-full lg:grid lg:grid-cols-12 bg-utblue2">
            <img src="@if($evento->image){{Storage::url($evento->image->url)}}@else http:\\www.utcancun.edu.mx/wp-content/uploads/2016/06/Instalaciones-Deportivas-UT-Soccer-Americano.jpg @endif" 
            alt=""  width="100%" class="object-cover w-full h-64 rounded-sm sm:h-96 lg:col-span-7 dark:bg-gray-500">
            <div class="p-6 space-y-2 lg:col-span-5 rounded">
              <h3 class="text-2xl font-semibold @if(strlen($evento->nombre) >= 120) sm:text-3xl @else sm:text-4xl @endif  ">{{$evento->nombre}}</h3>
              <span class="text-xs dark:text-gray-600">{{date("Y/m/d",strtotime($evento->setevento->fechaevento))}}</span>
              <p>{{$evento->intro}}.</p>
              @if ($evento->setevento->capacidad > $evento->setevento->inscritos)
                @if ($evento->setevento->fechaevento > date("Y-m-d"))
                
                @else
                
                @endif
                <div class="flex items-center p-2 space-x-2 rounded-md text-gray-800">
                  <div class="flex  self-stretch justify-center">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" fill="currentColor" class="h-9 w-9">
                      <path d="M454.423,278.957,328,243.839v-8.185a116,116,0,1,0-104,0V312H199.582l-18.494-22.6a90.414,90.414,0,0,0-126.43-13.367,20.862,20.862,0,0,0-8.026,33.47L215.084,496H472V302.08A24.067,24.067,0,0,0,454.423,278.957ZM192,132a84,84,0,1,1,136,65.9V132a52,52,0,0,0-104,0v65.9A83.866,83.866,0,0,1,192,132ZM440,464H229.3L79.141,297.75a58.438,58.438,0,0,1,77.181,11.91l28.1,34.34H256V132a20,20,0,0,1,40,0V268.161l144,40Z"></path>
                    </svg>
                  </div>
                  <span class="mt-3">Más información</span>
                </div>
                <!--button type="button" class="inline-flex items-center px-5 py-3 rounded-lg bg-gray-800 text-gray-50">Ver mas...
                <span class="absolute top-0 right-0 px-5 py-1 text-xs tracki text-center uppercase whitespace-no-wrap origin-bottom-left transform rotate-45 -translate-y-full translate-x-1/3 bg-teal-600">New</span>
                </button-->
              @else
              <div class="flex items-center p-2 space-x-4 rounded-md bg-gray-800 text-gray-50">
                <div class="flex items-center self-stretch justify-center">
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" class="w-10 h-10">
                    <path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7 4a1 1 0 11-2 0 1 1 0 012 0zm-1-9a1 1 0 00-1 1v4a1 1 0 102 0V6a1 1 0 00-1-1z" clip-rule="evenodd"></path>
                  </svg>
                </div>
                <span>Evento lleno</span>
              </div> 
              @endif
              
            </div>
          </a>
          <div class="grid justify-center gap-6 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-3 rounded-sm">
        @else  <!---Resto de imagenes-->
          <a rel="noopener noreferrer" href="{{route('eventos.show', $evento) }}" class="max-w-sm group hover:no-underline focus:no-underline bg-utblue2 ">
          <img role="presentation" class="object-cover w-full rounded h-44 dark:bg-gray-500" src="@if($evento->image){{Storage::url($evento->image->url)}}@else http:\\www.utcancun.edu.mx/wp-content/uploads/2016/06/Instalaciones-Deportivas-UT-Soccer-Americano.jpg @endif">
          <div class="p-6 space-y-2">
            <h3 class="flex-1 py-2 text-lg leading-snug font-semibold">{{$evento->nombre}}</h3>
            <span class="text-xs dark:text-gray-600">{{date("d/m/Y",strtotime($evento->setevento->fechaevento))}}</span>
            <p>{{$evento->intro}}.</p>
            <div class="flex flex-wrap justify-between pt-5 space-x-2 text-sm">
              @if ($evento->setevento->capacidad > $evento->setevento->inscritos)
                  <!--button type="button" class="inline-flex items-center px-5 py-3 rounded-lg bg-gray-800 text-gray-50">Ver mas...</button-->              
                  
                  <div class="flex items-center p-2 space-x-2 rounded-md text-gray-800"> 
                    <div class="flex flex-wrap justify-between pt-3 space-x-2">
                      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" fill="currentColor" class="h-9 w-9">
                        <path d="M454.423,278.957,328,243.839v-8.185a116,116,0,1,0-104,0V312H199.582l-18.494-22.6a90.414,90.414,0,0,0-126.43-13.367,20.862,20.862,0,0,0-8.026,33.47L215.084,496H472V302.08A24.067,24.067,0,0,0,454.423,278.957ZM192,132a84,84,0,1,1,136,65.9V132a52,52,0,0,0-104,0v65.9A83.866,83.866,0,0,1,192,132ZM440,464H229.3L79.141,297.75a58.438,58.438,0,0,1,77.181,11.91l28.1,34.34H256V132a20,20,0,0,1,40,0V268.161l144,40Z"></path>
                      </svg>
                    </div>
                    <span class="mt-3">Más información</span>
                  </div>  
              @else 
                  <div class="flex items-center p-2 space-x-2 rounded-md text-gray-800">
                    <div class="flex items-center self-stretch justify-center">
                      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" class="w-10 h-10">
                        <path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7 4a1 1 0 11-2 0 1 1 0 012 0zm-1-9a1 1 0 00-1 1v4a1 1 0 102 0V6a1 1 0 00-1-1z" clip-rule="evenodd"></path>
                      </svg>
                    </div>
                    <span class="mt-3">Evento lleno</span>
                  </div>   
              @endif
            </div>
            </div>
          </a>
            @endif
          @endforeach
        </div>
        <!---El estilo esta en views/vendor/pagiation/tailwind-->
          <div class="flex justify-center">
          {{$eventos->links()}}  
          </div>
    </div>
  </section>       
</x-app-layout>    