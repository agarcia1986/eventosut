<x-app-layout>
	<!--HERO-->
	<section class="p-6 bg-utfondo dark:text-gray-100">
		<div class="container grid gap-6 mx-auto text-center lg:grid-cols-2 xl:grid-cols-5">
			<div class="w-full px-6 py-16 rounded-md sm:px-12 md:px-16 xl:col-span-2 bg-blanco shadow-lg">
				<h1 class="@if(strlen($evento->nombre) >= 100) sm:text-2xl @else sm:text-4xl @endif font-extrabold dark:text-utblue">{{$evento->nombre}}</h1>
				<p class="my-8 font-medium dark:text-gray-700">{{$evento->intro}}</p>
				@auth
					@can('user.eventos.index')
						@if ($evento->participantes->contains('id', auth()->user()->id))
							<a href="{{route('user.pagos.index')}}" class="w-full py-2 px-5 font-semibold rounded-lg bg-utgreen text-utblue hover:bg-utgreen-dark transition-colors">Ya estás inscrito</a>
						@else
							
							<a href="{{route('user.eventos.edit', $evento)}}" class="w-full py-2 px-5 font-semibold rounded-lg bg-utgreen text-utblue hover:bg-utgreen-dark transition-colors">Inscribirme</a>
						@endif
					@endcan
				@else
					<a href="{{route('register')}}" class="w-full py-2 font-semibold rounded-lg bg-utgreen dark:text-gray-900 hover:bg-utgreen-dark transition-colors">Quiero participar</a>
				@endauth
			</div>
			<img src="@if($evento->image){{Storage::url($evento->image->url)}}@else http://www.utcancun.edu.mx/wp-content/uploads/2016/06/Instalaciones-Deportivas-UT-Soccer-Americano.jpg @endif" alt="" class="object-cover w-full rounded-md xl:col-span-3 dark:bg-gray-500 shadow-lg">
		</div>
	</section>
	
	@if(!empty($evento->setevento->video))
		<!--section class="bg-utfondo text-gray-700">
			<div class="p-5 mx-auto sm:p-10 md:p-16 bg-gray-100 text-gray-800">
				<div class="flex flex-col max-w-3xl mx-auto overflow-hidden rounded">

					
					<img src="https://source.unsplash.com/random/480x360" alt="" class="w-full h-60 sm:h-96 bg-gray-500"-->
					<!--div class="p-6 pb-12 m-4 mx-auto -mt-16 space-y-6 lg:max-w-2xl sm:px-10 sm:mx-12 lg:rounded-md bg-gray-50">
						<div class="space-y-2">
							<a rel="noopener noreferrer" href="#" class="inline-block text-2xl font-semibold sm:text-3xl">The Best Activewear from the Nordstrom Anniversary Sale</a>
							<p class="text-xs text-gray-600">By
								<a rel="noopener noreferrer" href="#" class="text-xs hover:underline">Leroy Jenkins</a>
							</p>
						</div>
						<div class="text-gray-800">
							<p>Insert the actual text content here...</p>
						</div>
					</div>
				</div>
			</div>
		</section-->
	@endif
	<!--INFORMATION-->

	<section class="bg-utfondo text-gray-700">
		<div class="container bg-blanco rounded-md mx-auto p-4 space-y-2 text-center">
			<h2 class="text-5xl font-bold"></h2>
			<p class="pt-2 pb-8 text-xl font-medium text-center">{!!$evento->informacion!!}</p>
		</div>
	</section>
	<!--LOCALIDAD-->
	<section class="p-6 bg-utfondo dark:text-gray-900">
		<div class="container bg-blanco mx-auto grid gap-4 sm:grid-cols-2 lg:grid-cols-4">
			<div class="flex flex-col items-center p-4">
				<svg class="w-10 h-10 text-utblue2" fill="currentColor" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 395.71 395.71" xml:space="preserve">
					<g id="SVGRepo_bgCarrier" stroke-width="0"></g>
					<g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g>
					<g id="SVGRepo_iconCarrier"> 
						<g> 
							<path d="M197.849,0C122.131,0,60.531,61.609,60.531,137.329c0,72.887,124.591,243.177,129.896,250.388l4.951,6.738 c0.579,0.792,1.501,1.255,2.471,1.255c0.985,0,1.901-0.463,2.486-1.255l4.948-6.738c5.308-7.211,129.896-177.501,129.896-250.388 C335.179,61.609,273.569,0,197.849,0z M197.849,88.138c27.13,0,49.191,22.062,49.191,49.191c0,27.115-22.062,49.191-49.191,49.191 c-27.114,0-49.191-22.076-49.191-49.191C148.658,110.2,170.734,88.138,197.849,88.138z"></path> 
						</g> 
					</g>
				</svg>
				<!--svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" class="w-8 h-8 dark:text-violet-400">
					<path fill-rule="evenodd" d="M11.3 1.046A1 1 0 0112 2v5h4a1 1 0 01.82 1.573l-7 10A1 1 0 018 18v-5H4a1 1 0 01-.82-1.573l7-10a1 1 0 011.12-.38z" clip-rule="evenodd"></path>
				</svg-->
				<h3 class="my-3 text-3xl font-semibold">Localidad</h3>
				<div class="space-y-1 leading-tight">
					<p>{{$evento->setevento->localidad}}</p>
					<!--p>Tempore quasi porro</p>
					<p>Blanditiis aut mollitia ex</p-->
				</div>
			</div>
			<div class="flex flex-col items-center p-4">
				<svg fill="currentColor" width="64px" height="64px" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" class="w-10 h-10 text-utblue2">
					<g id="SVGRepo_bgCarrier" stroke-width="0"></g>
					<g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g>
					<g id="SVGRepo_iconCarrier">
						<path d="M6,22H18a3,3,0,0,0,3-3V7a2,2,0,0,0-2-2H17V3a1,1,0,0,0-2,0V5H9V3A1,1,0,0,0,7,3V5H5A2,2,0,0,0,3,7V19A3,3,0,0,0,6,22ZM5,12.5a.5.5,0,0,1,.5-.5h13a.5.5,0,0,1,.5.5V19a1,1,0,0,1-1,1H6a1,1,0,0,1-1-1Z"></path>
					</g>
				</svg>
				<!--svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" class="w-8 h-8 dark:text-violet-400">
					<path fill-rule="evenodd" d="M11.3 1.046A1 1 0 0112 2v5h4a1 1 0 01.82 1.573l-7 10A1 1 0 018 18v-5H4a1 1 0 01-.82-1.573l7-10a1 1 0 011.12-.38z" clip-rule="evenodd"></path>
				</svg-->
				<h3 class="my-3 text-3xl font-semibold">Fecha</h3>
				<div class="space-y-1 leading-tight">
					<p>{{date("d/m/Y",strtotime($evento->setevento->fechaevento))}}</p>
					<!--p>Tempore quasi porro</p>
					<p>Blanditiis aut mollitia ex</p-->
				</div>
			</div>
			<div class="flex flex-col items-center p-4">
				<svg class="w-10 h-10 text-utblue2" fill="currentColor" width="64px" height="64px" viewBox="0 0 32 32" id="icon" xmlns="http://www.w3.org/2000/svg"><g id="SVGRepo_bgCarrier" stroke-width="0"></g><g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g><g id="SVGRepo_iconCarrier"><defs><style>.cls-1{fill:none;}</style></defs><title>request-quote</title><path d="M22,22v6H6V4H16V2H6A2,2,0,0,0,4,4V28a2,2,0,0,0,2,2H22a2,2,0,0,0,2-2V22Z" transform="translate(0)"></path><path d="M29.54,5.76l-3.3-3.3a1.6,1.6,0,0,0-2.24,0l-14,14V22h5.53l14-14a1.6,1.6,0,0,0,0-2.24ZM14.7,20H12V17.3l9.44-9.45,2.71,2.71ZM25.56,9.15,22.85,6.44l2.27-2.27,2.71,2.71Z" transform="translate(0)"></path><rect id="_Transparent_Rectangle_" data-name="<Transparent Rectangle>" class="cls-1" width="32" height="32"></rect></g></svg>
				
				<!--svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" class="w-8 h-8 dark:text-violet-400">
					<path fill-rule="evenodd" d="M11.3 1.046A1 1 0 0112 2v5h4a1 1 0 01.82 1.573l-7 10A1 1 0 018 18v-5H4a1 1 0 01-.82-1.573l7-10a1 1 0 011.12-.38z" clip-rule="evenodd"></path>
				</svg-->
				<h3 class="my-3 text-3xl font-semibold">Requisitos</h3>
				<div class="space-y-1 leading-tight">
					<p>{{$evento->setevento->requisitos}}</p>
					<!--p>Tempore quasi porro</p>
					<p>Blanditiis aut mollitia ex</p-->
				</div>
			</div>
			<div class="flex flex-col items-center p-4">
				<svg class="w-10 h-10 text-utblue2" fill="currentColor" width="64px" height="64px" version="1.2" baseProfile="tiny" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 256 256" xml:space="preserve"><g id="SVGRepo_bgCarrier" stroke-width="0"></g><g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g><g id="SVGRepo_iconCarrier"> <g> <circle cx="193.3" cy="113.5" r="16.8"></circle> <circle cx="172.6" cy="155.3" r="16.8"></circle> <path d="M145.8,175.2h-18.9h-18.9c-11.5,0-18.7,9.5-18.7,21.4v29.3h12.9V200c0-1.2,1-2,2-2c1.2,0,2,0.8,2,2v25.8h41.5V200 c0-1.2,1-2,2-2c1.2,0,2,1,2,2v25.8h12.9v-29.1C164.8,184.7,157.5,175.2,145.8,175.2z"></path> <circle cx="150.5" cy="113.5" r="16.8"></circle> <path d="M199.9,155.3c0,9.3,7.5,16.8,16.8,16.8c9.3,0,16.8-7.5,16.8-16.8c0-9.3-7.5-16.8-16.8-16.8 C207.4,138.5,199.9,146,199.9,155.3z"></path> <path d="M198.2,175.2c-11.5,0-18.7,9.5-18.7,21.4v29.3h12.9V200c0-1.2,1-2,2-2c1.2,0,2,0.8,2,2v25.8h41.5V200c0-1.2,1-2,2-2 c1.2,0,2,1,2,2v25.8h12.9v-29.1c0.2-12.1-7.1-21.6-18.7-21.6h-18.9H198.2z"></path> <circle cx="40.6" cy="155.3" r="16.8"></circle> <path d="M15.6,225.8V200c0-1.2,1-2,2-2c1.2,0,2,0.8,2,2v25.8h41.5V200c0-1.2,1-2,2-2c1.2,0,2,1,2,2v25.8h12.9v-29.1 c0.2-12.1-7.1-21.6-18.7-21.6H40.4H21.5c-11.5,0-18.7,9.5-18.7,21.4v29.3H15.6z"></path> <path d="M110.1,155.3c0,9.3,7.5,16.8,16.8,16.8c9.3,0,16.8-7.5,16.8-16.8c0-9.3-7.5-16.8-16.8-16.8 C117.6,138.5,110.1,146,110.1,155.3z"></path> <circle cx="62.1" cy="113.5" r="16.8"></circle> <circle cx="107.5" cy="113.5" r="16.8"></circle> <circle cx="171.5" cy="77.6" r="16.8"></circle> <circle cx="83.1" cy="77.6" r="16.8"></circle> <circle cx="128.4" cy="77.6" r="16.8"></circle> <circle cx="84.9" cy="155.3" r="16.8"></circle> </g> </g></svg>
				
				<!--svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" class="w-8 h-8 dark:text-violet-400">
					<path fill-rule="evenodd" d="M11.3 1.046A1 1 0 0112 2v5h4a1 1 0 01.82 1.573l-7 10A1 1 0 018 18v-5H4a1 1 0 01-.82-1.573l7-10a1 1 0 011.12-.38z" clip-rule="evenodd"></path>
				</svg-->
				<h3 class="my-3 text-3xl font-semibold">Capacidad</h3>
				<div class="space-y-1 leading-tight">
					<p>{{$evento->setevento->capacidad}}</p>
					<!--p>Tempore quasi porro</p>
					<p>Blanditiis aut mollitia ex</p-->
				</div>
			</div>
		</div>
	</section>

	<!--PONENTES-->
	@if($evento->ponente->count())
	
	<section class="p-6 bg-utfondo dark:text-gray-900">
		<div class="container bg-blanco p-4 mx-auto space-y-16 sm:p-10 rounded-md ">
			<div class="space-y-4 text-center">
				<h2 class="text-5xl font-extrabold dark:text-utblue">PONENTES</h2>
				<!--p class="max-w-2xl dark:text-gray-900">At a assumenda quas cum earum ut itaque commodi saepe rem aspernatur quam natus quis nihil quod, hic explicabo doloribus magnam neque, exercitationem eius sunt!</p-->
			</div>
			<div class="flex items-center justify-center space-x-2">	
			@foreach ($evento->ponente as $pote)
		
				<div class="space-y-4 bg-gray-100 rounded-sm items-center text-center">
					<img alt="" class="object-cover h-56 mx-auto mb-4 bg-center rounded-sm dark:bg-gray-800" src="{{Storage::url($pote->image->url)}}" height="320px" width="240px">
					<div class="flex flex-col items-center">
						<h4 class="text-sm font-semibold dark:text-utblue2">{{$pote->nivestudio}} {{$pote->nombre}}</h4>
						<p class="text-sm dark:text-utblue">{{$pote->compania}}</p>
						<p class="text-sm dark:text-utblue">{{$pote->puesto}}</p>
						<!--div class="flex mt-2 space-x-2">
							<a rel="noopener noreferrer" href="#" title="Twitter" class="dark:text-gray-900">
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50" fill="currentColor" class="w-4 h-4">
									<path d="M 50.0625 10.4375 C 48.214844 11.257813 46.234375 11.808594 44.152344 12.058594 C 46.277344 10.785156 47.910156 8.769531 48.675781 6.371094 C 46.691406 7.546875 44.484375 8.402344 42.144531 8.863281 C 40.269531 6.863281 37.597656 5.617188 34.640625 5.617188 C 28.960938 5.617188 24.355469 10.21875 24.355469 15.898438 C 24.355469 16.703125 24.449219 17.488281 24.625 18.242188 C 16.078125 17.8125 8.503906 13.71875 3.429688 7.496094 C 2.542969 9.019531 2.039063 10.785156 2.039063 12.667969 C 2.039063 16.234375 3.851563 19.382813 6.613281 21.230469 C 4.925781 21.175781 3.339844 20.710938 1.953125 19.941406 C 1.953125 19.984375 1.953125 20.027344 1.953125 20.070313 C 1.953125 25.054688 5.5 29.207031 10.199219 30.15625 C 9.339844 30.390625 8.429688 30.515625 7.492188 30.515625 C 6.828125 30.515625 6.183594 30.453125 5.554688 30.328125 C 6.867188 34.410156 10.664063 37.390625 15.160156 37.472656 C 11.644531 40.230469 7.210938 41.871094 2.390625 41.871094 C 1.558594 41.871094 0.742188 41.824219 -0.0585938 41.726563 C 4.488281 44.648438 9.894531 46.347656 15.703125 46.347656 C 34.617188 46.347656 44.960938 30.679688 44.960938 17.09375 C 44.960938 16.648438 44.949219 16.199219 44.933594 15.761719 C 46.941406 14.3125 48.683594 12.5 50.0625 10.4375 Z"></path>
								</svg>
							</a>
							<a rel="noopener noreferrer" href="#" title="LinkedIn" class="dark:text-gray-900">
								<svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 32 32" class="w-4 h-4">
									<path d="M8.268 28h-5.805v-18.694h5.805zM5.362 6.756c-1.856 0-3.362-1.538-3.362-3.394s1.505-3.362 3.362-3.362 3.362 1.505 3.362 3.362c0 1.856-1.506 3.394-3.362 3.394zM29.994 28h-5.792v-9.1c0-2.169-0.044-4.95-3.018-4.95-3.018 0-3.481 2.356-3.481 4.794v9.256h-5.799v-18.694h5.567v2.55h0.081c0.775-1.469 2.668-3.019 5.492-3.019 5.875 0 6.955 3.869 6.955 8.894v10.269z"></path>
								</svg>
							</a>
							<a rel="noopener noreferrer" href="#" title="GitHub" class="dark:text-gray-900">
								<svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 32 32" class="w-4 h-4">
									<path d="M16 0.396c-8.839 0-16 7.167-16 16 0 7.073 4.584 13.068 10.937 15.183 0.803 0.151 1.093-0.344 1.093-0.772 0-0.38-0.009-1.385-0.015-2.719-4.453 0.964-5.391-2.151-5.391-2.151-0.729-1.844-1.781-2.339-1.781-2.339-1.448-0.989 0.115-0.968 0.115-0.968 1.604 0.109 2.448 1.645 2.448 1.645 1.427 2.448 3.744 1.74 4.661 1.328 0.14-1.031 0.557-1.74 1.011-2.135-3.552-0.401-7.287-1.776-7.287-7.907 0-1.751 0.62-3.177 1.645-4.297-0.177-0.401-0.719-2.031 0.141-4.235 0 0 1.339-0.427 4.4 1.641 1.281-0.355 2.641-0.532 4-0.541 1.36 0.009 2.719 0.187 4 0.541 3.043-2.068 4.381-1.641 4.381-1.641 0.859 2.204 0.317 3.833 0.161 4.235 1.015 1.12 1.635 2.547 1.635 4.297 0 6.145-3.74 7.5-7.296 7.891 0.556 0.479 1.077 1.464 1.077 2.959 0 2.14-0.020 3.864-0.020 4.385 0 0.416 0.28 0.916 1.104 0.755 6.4-2.093 10.979-8.093 10.979-15.156 0-8.833-7.161-16-16-16z"></path>
								</svg>
							</a>
						</div-->
					</div>
				</div>
			@endforeach
		</div>
		</div>
	</section>
@endif	
	
	
	<!--CONVOCATORIA-->
@if (!empty($evento->archivo))
<section class="p-6 bg-utfondo dark:text-gray-900">
<div class="p-6 py-12  dark:text-gray-900">
	<div class="container bg-utblue2 p-6 mx-auto rounded-md">
		<div class="flex flex-col lg:flex-row items-center justify-between">
			<h2 class="text-center text-5xl tracking-tighter font-bold text-blanco">Conoce todas las bases</h2>
			<div class="space-x-2 text-center py-2 lg:py-0">
				
				<span class="font-bold text-lg"></span>
			</div>
			<a href="{{Storage::url($evento->archivo->url)}}"target="_blank" ><button type="button" class="relative px-8 py-4 ml-4 overflow-hidden font-semibold rounded dark:bg-blanco dark:text-gray-900">Ver
				<span class="absolute top-0 right-0 px-5 py-1 text-xs tracking-wider text-center uppercase whitespace-no-wrap origin-bottom-left transform rotate-45 -translate-y-full translate-x-1/3 dark:bg-utblue3">PDF</span>
			</button></a>
			
			
		</div>
	</div>
</div>
</section>
@endif

	
<!--PATROCINADORES-->
@if($evento->patrocinadores->count())
<section class="py-6 bg-gray-100 text-gray-800">
	<div class="container flex flex-col items-center justify-center p-4 mx-auto sm:p-10">
		<!--p class="p-2 text-sm font-medium tracki text-center uppercase">Development team</p-->
		<h1 class="text-4xl font-bold leadi text-center dark:text-utblue sm:text-5xl">Los patrocinadores que hacen esto posible</h1>
		<div class="flex flex-row flex-wrap-reverse justify-center mt-8">
			@foreach ($evento->patrocinadores as $patrocinador)
			<div class="flex flex-col justify-center w-full px-8 mx-6 my-12 text-center rounded-md md:w-96 lg:w-80 xl:w-64 bg-utblue text-gray-100">
				<img alt="" class="self-center flex-shrink-0 w-24 h-24 -mt-12 bg-center bg-cover rounded-full bg-gray-500" src="{{Storage::url($patrocinador->image->url)}}">
				<div class="flex-1 my-4">
					<p class="text-xl font-semibold leadi">{{$patrocinador->nombre}}</p>
					<p></p><br>
				</div>
				<div class="flex items-center justify-center p-3 space-x-3 border-t-2">
				@foreach (json_decode($patrocinador->redes) as $item =>$key)                        
				@switch($item)
					@case("facebook")
					<a rel="noopener noreferrer" href="#" title="{{$item}}" class="text-gray-50 hover:text-teal-600">
						<svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 32 32" class="w-4 h-4 fill-current">
							<path d="M32 16c0-8.839-7.167-16-16-16-8.839 0-16 7.161-16 16 0 7.984 5.849 14.604 13.5 15.803v-11.177h-4.063v-4.625h4.063v-3.527c0-4.009 2.385-6.223 6.041-6.223 1.751 0 3.584 0.312 3.584 0.312v3.937h-2.021c-1.984 0-2.604 1.235-2.604 2.5v3h4.437l-0.713 4.625h-3.724v11.177c7.645-1.199 13.5-7.819 13.5-15.803z"></path>
						</svg>
					</a>
						@break
					@case("instagram")
					<a rel="noopener noreferrer" href="#" title="{{$item}}" class="text-gray-50 hover:text-teal-600">
						<svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="w-4 h-4 fill-current" viewBox="0 0 23 23" >
							<path d="M16.19 2H7.81C4.17 2 2 4.17 2 7.81V16.18C2 19.83 4.17 22 7.81 22H16.18C19.82 22 21.99 19.83 21.99 16.19V7.81C22 4.17 19.83 2 16.19 2ZM12 15.88C9.86 15.88 8.12 14.14 8.12 12C8.12 9.86 9.86 8.12 12 8.12C14.14 8.12 15.88 9.86 15.88 12C15.88 14.14 14.14 15.88 12 15.88ZM17.92 6.88C17.87 7 17.8 7.11 17.71 7.21C17.61 7.3 17.5 7.37 17.38 7.42C17.26 7.47 17.13 7.5 17 7.5C16.73 7.5 16.48 7.4 16.29 7.21C16.2 7.11 16.13 7 16.08 6.88C16.03 6.76 16 6.63 16 6.5C16 6.37 16.03 6.24 16.08 6.12C16.13 5.99 16.2 5.89 16.29 5.79C16.52 5.56 16.87 5.45 17.19 5.52C17.26 5.53 17.32 5.55 17.38 5.58C17.44 5.6 17.5 5.63 17.56 5.67C17.61 5.7 17.66 5.75 17.71 5.79C17.8 5.89 17.87 5.99 17.92 6.12C17.97 6.24 18 6.37 18 6.5C18 6.63 17.97 6.76 17.92 6.88Z" ></path> 
						</svg>
					</a>
						@break
					@case("tiktok")
					<a rel="noopener noreferrer" href="#" title="{{$item}}" class="text-gray-50 hover:text-teal-600">
						<!--svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" fill="currentColor" -->
							<svg fill="currentColor" viewBox="0 0 512 512" id="icons" class="w-4 h-4 fill-current" xmlns="http://www.w3.org/2000/svg"><g id="SVGRepo_bgCarrier" stroke-width="0"></g><g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g><g id="SVGRepo_iconCarrier"><path d="M412.19,118.66a109.27,109.27,0,0,1-9.45-5.5,132.87,132.87,0,0,1-24.27-20.62c-18.1-20.71-24.86-41.72-27.35-56.43h.1C349.14,23.9,350,16,350.13,16H267.69V334.78c0,4.28,0,8.51-.18,12.69,0,.52-.05,1-.08,1.56,0,.23,0,.47-.05.71,0,.06,0,.12,0,.18a70,70,0,0,1-35.22,55.56,68.8,68.8,0,0,1-34.11,9c-38.41,0-69.54-31.32-69.54-70s31.13-70,69.54-70a68.9,68.9,0,0,1,21.41,3.39l.1-83.94a153.14,153.14,0,0,0-118,34.52,161.79,161.79,0,0,0-35.3,43.53c-3.48,6-16.61,30.11-18.2,69.24-1,22.21,5.67,45.22,8.85,54.73v.2c2,5.6,9.75,24.71,22.38,40.82A167.53,167.53,0,0,0,115,470.66v-.2l.2.2C155.11,497.78,199.36,496,199.36,496c7.66-.31,33.32,0,62.46-13.81,32.32-15.31,50.72-38.12,50.72-38.12a158.46,158.46,0,0,0,27.64-45.93c7.46-19.61,9.95-43.13,9.95-52.53V176.49c1,.6,14.32,9.41,14.32,9.41s19.19,12.3,49.13,20.31c21.48,5.7,50.42,6.9,50.42,6.9V131.27C453.86,132.37,433.27,129.17,412.19,118.66Z"></path></g></svg>
					</a>
						@break
					@case("x")
						<a rel="noopener noreferrer" href="#" title="{{$item}}" class="text-gray-50 hover:text-teal-600">
							<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" class="w-4 h-4 fill-current">
								<path d="M23.954 4.569a10 10 0 01-2.825.775 4.958 4.958 0 002.163-2.723c-.951.555-2.005.959-3.127 1.184a4.92 4.92 0 00-8.384 4.482C7.691 8.094 4.066 6.13 1.64 3.161a4.822 4.822 0 00-.666 2.475c0 1.71.87 3.213 2.188 4.096a4.904 4.904 0 01-2.228-.616v.061a4.923 4.923 0 003.946 4.827 4.996 4.996 0 01-2.212.085 4.937 4.937 0 004.604 3.417 9.868 9.868 0 01-6.102 2.105c-.39 0-.779-.023-1.17-.067a13.995 13.995 0 007.557 2.209c9.054 0 13.999-7.496 13.999-13.986 0-.209 0-.42-.015-.63a9.936 9.936 0 002.46-2.548l-.047-.02z"></path>
							</svg>
						</a>
						@break
					@default
						
				@endswitch
				@endforeach  
				</div>
				<!--div class="flex items-center justify-center p-3 space-x-3 border-t-2">
					<a rel="noopener noreferrer" href="#" title="Email" class="text-gray-50 hover:text-teal-600">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" class="w-5 h-5">
							<path d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884z"></path>
							<path d="M18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z"></path>
						</svg>
					</a>
					<a rel="noopener noreferrer" href="#" title="Twitter" class="text-gray-50 hover:text-teal-600">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50" fill="currentColor" class="w-5 h-5">
							<path d="M 50.0625 10.4375 C 48.214844 11.257813 46.234375 11.808594 44.152344 12.058594 C 46.277344 10.785156 47.910156 8.769531 48.675781 6.371094 C 46.691406 7.546875 44.484375 8.402344 42.144531 8.863281 C 40.269531 6.863281 37.597656 5.617188 34.640625 5.617188 C 28.960938 5.617188 24.355469 10.21875 24.355469 15.898438 C 24.355469 16.703125 24.449219 17.488281 24.625 18.242188 C 16.078125 17.8125 8.503906 13.71875 3.429688 7.496094 C 2.542969 9.019531 2.039063 10.785156 2.039063 12.667969 C 2.039063 16.234375 3.851563 19.382813 6.613281 21.230469 C 4.925781 21.175781 3.339844 20.710938 1.953125 19.941406 C 1.953125 19.984375 1.953125 20.027344 1.953125 20.070313 C 1.953125 25.054688 5.5 29.207031 10.199219 30.15625 C 9.339844 30.390625 8.429688 30.515625 7.492188 30.515625 C 6.828125 30.515625 6.183594 30.453125 5.554688 30.328125 C 6.867188 34.410156 10.664063 37.390625 15.160156 37.472656 C 11.644531 40.230469 7.210938 41.871094 2.390625 41.871094 C 1.558594 41.871094 0.742188 41.824219 -0.0585938 41.726563 C 4.488281 44.648438 9.894531 46.347656 15.703125 46.347656 C 34.617188 46.347656 44.960938 30.679688 44.960938 17.09375 C 44.960938 16.648438 44.949219 16.199219 44.933594 15.761719 C 46.941406 14.3125 48.683594 12.5 50.0625 10.4375 Z"></path>
						</svg>
					</a>
					<a rel="noopener noreferrer" href="#" title="LinkedIn" class="text-gray-50 hover:text-teal-600">
						<svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 32 32" class="w-5 h-5">
							<path d="M8.268 28h-5.805v-18.694h5.805zM5.362 6.756c-1.856 0-3.362-1.538-3.362-3.394s1.505-3.362 3.362-3.362 3.362 1.505 3.362 3.362c0 1.856-1.506 3.394-3.362 3.394zM29.994 28h-5.792v-9.1c0-2.169-0.044-4.95-3.018-4.95-3.018 0-3.481 2.356-3.481 4.794v9.256h-5.799v-18.694h5.567v2.55h0.081c0.775-1.469 2.668-3.019 5.492-3.019 5.875 0 6.955 3.869 6.955 8.894v10.269z"></path>
						</svg>
					</a>
					<a rel="noopener noreferrer" href="#" title="GitHub" class="text-gray-50 hover:text-teal-600">
						<svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 32 32" class="w-5 h-5">
							<path d="M16 0.396c-8.839 0-16 7.167-16 16 0 7.073 4.584 13.068 10.937 15.183 0.803 0.151 1.093-0.344 1.093-0.772 0-0.38-0.009-1.385-0.015-2.719-4.453 0.964-5.391-2.151-5.391-2.151-0.729-1.844-1.781-2.339-1.781-2.339-1.448-0.989 0.115-0.968 0.115-0.968 1.604 0.109 2.448 1.645 2.448 1.645 1.427 2.448 3.744 1.74 4.661 1.328 0.14-1.031 0.557-1.74 1.011-2.135-3.552-0.401-7.287-1.776-7.287-7.907 0-1.751 0.62-3.177 1.645-4.297-0.177-0.401-0.719-2.031 0.141-4.235 0 0 1.339-0.427 4.4 1.641 1.281-0.355 2.641-0.532 4-0.541 1.36 0.009 2.719 0.187 4 0.541 3.043-2.068 4.381-1.641 4.381-1.641 0.859 2.204 0.317 3.833 0.161 4.235 1.015 1.12 1.635 2.547 1.635 4.297 0 6.145-3.74 7.5-7.296 7.891 0.556 0.479 1.077 1.464 1.077 2.959 0 2.14-0.020 3.864-0.020 4.385 0 0.416 0.28 0.916 1.104 0.755 6.4-2.093 10.979-8.093 10.979-15.156 0-8.833-7.161-16-16-16z"></path>
						</svg>
					</a>
				</div-->
			</div>
		@endforeach
		</div>
	</div>
</section>
@endif

	<!--OTROS EVENTOS-->
	@if($similares->count())
	<section class="bg-utfondo dark:text-gray-800">
		<div class="container bg-blanco p-6 mx-auto space-y-8 rounded-md">
			<div class="space-y-2 text-center">
				<h2 class="text-5xl font-extrabold dark:text-utblue">Eventos similares</h2>
				<p class="font-serif text-md dark:text-utblue">Algunos eventos de nuestra universidad</p>
			</div>
			<div class="grid grid-cols-1 gap-x-4 gap-y-8 md:grid-cols-2 lg:grid-cols-4">
				@foreach ($similares as $similar)
				<article class="flex flex-col max-w-xs p-6 bg-gray-100 rounded-lg shadow-md">
					<a rel="noopener noreferrer" href="{{route('eventos.show',$similar)}}" aria-label="eventos similares">
						<img alt="" class="object-cover w-full h-52 rounded-t-lg dark:bg-gray-500" src="@if($similar->image){{Storage::url($similar->image->url)}}@else http://www.utcancun.edu.mx/wp-content/uploads/2016/06/Instalaciones-Deportivas-UT-Soccer-Americano.jpg @endif">
					</a>
					<div class="flex flex-col flex-1 p-6">
						<h3 class="text-xs tracking-wider uppercase text-utblue2">{{$similar->nombre}}</h3>
						<h3 class="flex-1 py-2 text-lg font-semibold leading-snug text-utblue">{{$similar->intro}}</h3>
						<div class="flex flex-wrap justify-between pt-3 space-x-2 text-xs dark:text-gray-400">
							<span>{{date("d/m/Y",strtotime($similar->setevento->fechaevento))}}</span>
							<span>{{$similar->setevento->localidad}}</span>
						</div>
					</div>
				</article>
				@endforeach
			</div>
		</div>
	</section>
	@endif
	
</x-app-layout>