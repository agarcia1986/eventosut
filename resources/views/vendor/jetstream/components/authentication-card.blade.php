<div class="min-h-screen flex flex-col sm:justify-center items-center pt-6 sm:pt-0 bg-blanco dark:text-gray-100">
    <div>
        {{ $logo }}
    </div>

    <div class="bg-utblue2 dark:text-gray-100 shadow-md overflow-hidden sm:rounded-lg">
        {{ $slot }}
    </div>
</div>
