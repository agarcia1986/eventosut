@extends('adminlte::page')
@section('title', 'Eventos UT')

@section('content_header')
    <h1></h1>
@stop
@section('content')
<x-adminlte-card theme="lightblue" theme-mode="outline">
    <div class="card-body">
        <div class="container">
            <x-adminlte-profile-widget class="elevation-4" name="{{auth()->user()->name}} {{auth()->user()->pname}} {{auth()->user()->mname}}" desc="Participante"
            img="https://picsum.photos/id/177/100" cover="https://picsum.photos/id/541/550/200"
            header-class="text-white text-right" footer-class='bg-gradient-dark'>
            <x-adminlte-profile-row-item title="En la plataforma desde: {{date('d/m/Y',strtotime(auth()->user()->created_at))}}" class="text-center border-bottom border-secondary"/>
            <x-adminlte-profile-col-item title="{{auth()->user()->email}}" icon="fas fa-2x fa-envelope text-orange" size=4/>
            <x-adminlte-profile-col-item title="Facebook" icon="fab fa-2x fa-facebook text-orange" size=4/>
            <x-adminlte-profile-col-item title="{{$participante[0]['celular']}}" icon="fas fa-2x fa-phone text-orange" size=4/>
           
            <!--x-adminlte-profile-col-item title="CSS3" icon="fab fa-2x fa-css3 text-orange" size=3/>
            <!x-adminlte-profile-col-item title="Angular" icon="fab fa-2x fa-angular text-orange" size=4/>
            <x-adminlte-profile-col-item title="Bootstrap" icon="fab fa-2x fa-bootstrap text-orange" size=4/-->
   
              
           
            </x-adminlte-profile-widget>
        </div>
        <x-adminlte-card  theme-mode="outline" maximizable>
            {!! QrCode::size(200)->generate(auth()->user()->id) !!}
        </x-adminlte-card>
        
        
</x-adminlte-card>   


   
     
    

@stop