@extends('adminlte::page')
@section('title', 'Eventos UT')

@section('content_header')

    <h1>Inscripción</h1>
@stop

@section('content')
@if(session('info'))
    <div class="alert alert-success">
        <strong>{{session('info')}}</strong>
    </div>
   
@endif
<div class="card">
    <div class="card-body">
        <h4>Estas a punto de inscribirte al evento {{$evento}}
    </div>
</div>
@stop
