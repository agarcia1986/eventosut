@extends('adminlte::page')
@section('title', 'Eventos UT')

@section('content_header')
    <h1>Mis pagos </h1>
@stop

@section('content')
@if(session('info'))
    <div class="alert alert-success">
        <strong>{{session('info')}}</strong>
    </div>
@endif
<x-adminlte-card theme="lightblue" theme-mode="outline">
    <div class="card-body">
        <div class="row">
            @if ($pagos->count())
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Evento</th>
                        <th>referencia</th>
                        <th colspan="2">Acciones</th>
                        
                    </tr>
                </thead>
                <tbody>
                 @foreach ($pagos as $pago)
                    <tr>
                        <td>
                         @foreach($pago->eventos as $ev)
                             {{$ev->nombre}}
                         @endforeach   
                        <td>{{$pago->referencia}}</td>
                             @switch($pago->estatuspago)
                                 @case(2)
                                 <td>
                                     <a class="btn btn-outline-success btn-sm disabled" href="{{route('user.pagos.edit', $pago)}}">en revisión</a>     
                                 </td>
                                 @break  
                                 @case(3)
                                 <td>
                                     <a class="btn btn-outline-success btn-sm disabled" href="{{route('user.pagos.edit', $pago)}}">Gracias</a>     
                                 </td>
                                 @break
                                 @default
                                 <td>
                                     <a class="btn btn-outline-primary btn-sm" href="{{route('user.pagos.edit', $pago)}}">Subir comprobante</a>
                                 </td>
                             @endswitch
                         <td with="10px">
                         </td>     
                    </tr>
                @endforeach
                </tbody>
            </table>
            @else 
                <x-adminlte-callout theme="danger"  title="No tienes pagos realizados o pendientes" icon="fas fa-lg fa-face">
                </x-adminlte-callout>
            @endif          
        </div>
    </div>
</x-adminlte-card>
@stop