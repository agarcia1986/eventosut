@extends('adminlte::page')
@section('title', 'Eventos UT')

@section('content_header')

    <h1>Pago</h1>
@stop

@section('content')
@if(session('info'))
    <div class="alert alert-success">
        <strong>{{session('info')}}</strong>
    </div>
@endif
<div class="card">
    <div class="card-body"> 
                
                {!! Form::model($pago,['route' => ['user.pagos.update',$pago], 'autocomplete' => 'off', 'files' => true, 'method' => 'put']) !!}
                <div class="row">
                    <div class="form-group col-md-4">
                        {!! Form::label('referencia', 'Referencia', ) !!}
                        {!! Form::text('referencia', null, ['class' => 'form-control','readonly']) !!}
                    </div>
                    <div class="form-group col-md-4">
                        {!! Form::label('file', 'Comprobante :') !!}
                        {!! Form::file('file', ['class' => 'form-control-file', 'accept' => 'image/*']) !!}
                        @error('file')
                            <small class="text-danger">Tu comprobante es muy pesado, por favor reduce la calidad de tu imagen</small>
                        @enderror
                    </div>
                    <div class="form-group col-md-4">
                        <div class="col">
                            <div class="">
                                @isset ($evento->image)
                                    <img id="picture" src="{{Storage::url($evento->image->url)}}">
                                    @else
                                    <img id="picture" src="" width="200px" height="80%" class="img img-resposive"  hidden alt="">
                                    <div class="alert alert-success" id="aviso" role="alert">
                                        Tu comprobante debe pesar menos de 2 MB.
                                    </div>
                                @endisset
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    {!! Form::submit('Enviar', ['class' => 'btn btn-outline-primary']) !!}
                    {!! Form::close() !!}
                </div>
               
    </div>   
</div>
@stop
@section('css')
<style>
    .image-wrapper{
        position :relative;
        padding-bottom: 56.25%;
    }
    .image-wrapper img{
        position: absolute;
        object-fit: cover;
        width: 100%;
        height: 100%;
    }
</style>
@endsection
@section('js')
 <script>
    let ticketPago= $("#file");
    let imgSpace= document.getElementById("picture");
    let aviso = document.getElementById("aviso");
    ticketPago.on('change', function(cambiarImagen) {
                const [file] = this.files
                var sizeByte = this.files[0].size;
                var siezekiloByte = parseInt(sizeByte / 1024);
            if(siezekiloByte > 2048){
                ticketPago.addClass('is-invalid');
                alert('El tamaño de tu imagen supera el limite permitido');

            }else{         
                var img = new Image();
                img.src = URL.createObjectURL(file);
                imgSpace.setAttribute('src', img.src);
                aviso.setAttribute('hidden');
                imgSpace.removeAttribute('hidden');
            };
});
</script>
    
    

@endsection