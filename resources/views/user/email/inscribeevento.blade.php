<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>¡FELICIDADES TU REGISTRO FUE EXITOSO!
    </title>
</head>
<body>
    <img src="{{$message->embed('..\public\storage\default_event.jpg')}}">
    <h3>Estimado/a : {{$datos['name']}} {{$datos['pname']}} {{$datos['mname']}}: </h3>
    <h3>Gracias por registrarte en nuestra plataforma de eventos.</h3>
    <p>Los datos de tu cuenta son:</p>
    <p> <strong> Email:</strong> {{$datos['email']}} </p>
    <p><strong> Contraseña:</strong> {{$datos['password']}} </p>

    <p>Ahora estas listo para inscribirte a cualquiera de nuestros eventos próximos.</p>
    <footer>
        {{$footer}}
    </footer>
</body>
</html>