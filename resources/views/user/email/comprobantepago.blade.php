<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Comprobante recibido.
    </title>
</head>
<body>
    
    <h3>Estimado/a : {{$datos['name']}}: </h3>
    <h3>Gracias por realizar su pago, en un periodo de 48 Horas estara recibiendo una respuesta por parte de nuestro staff.</h3>
   
    <p>Gracias por participar.</p>
    <footer>
        {{$footer}}
    </footer>
</body>
</html>