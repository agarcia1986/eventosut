<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>¡FELICIDADES TU REGISTRO FUE EXITOSO!
    </title>
</head>
<body>
    <img src="{{$message->embed('..\public\storage\logos\ut.png')}}">
    <h3>Estimado/a : {{$datos['name']}} {{$datos['pname']}} {{$datos['mname']}}: </h3>
    <h3>Para concluir con el proceso de inscripción a la carrera deportiva, favor de seguir los siguientes pasos:</h3>
    <ol>
        <li>Ingresar a la url proporcionada y obtener los datos bancarios o descargar su ficha de pago.</li>
        <li>El pago lo podrá realizar en cualquier sucursal bancaria o por medio de transferencia.</li>
        <small>NOTA: EN CASO DE QUE EL PAGO SEA POR TRANSFERENCIA DEBERÁ DE PONER EN EL APARATADO DE CONCEPTO LA REFERENCIA DE LA CUENTA PROPORCIONADA, (si llega requerir factura, favor de descargar el formato archivo factura.xls del portal, y llenarlo debidamente. Posterior a ello enviarlo como adjunto al correo dpenalver@utcancun.edu.mx, solicitando la emisión de la misma. Tiene 24 hrs para realizar este trámite.</small>
        <li>El último día para realizar el pago será el 21 de septiembre del 2022.</li>
        <li>Una vez realizado el pago, deberá ingresar nuevamente a su usuario y adjuntar el comprobante de pago en el apartado correspondiente, posteriormente deberá esperar un lapso 2 días hábiles para que sea validado, por lo que una vez que sea aprobado se podrá visualizar en el estatus y se notificará al correo electrónico proporcionado (es importante verificar la bandeja de spam de su correo electrónico).</li>
        <li>Por último, una vez validado su pago podrá acudir por su kit deportivo al Departamento de Actividades Culturales y Deportivas ubicado en el edificio B de la Universidad Tecnológica de Cancún, de lunes a viernes en un horario de 8:30 a 18:00 horas.</li>
    </ol>
    
    <h4 class="text-center">10.	Atentamente:</h4>
    <p class="text-center">11.	COMITÉ ORGANIZADOR</p>

   
    <footer>
        {{$footer}}
    </footer>
</body>
</html>