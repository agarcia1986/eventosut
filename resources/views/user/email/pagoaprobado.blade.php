<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Comprobante aprobado.
    </title>
</head>
<body>
    
    <h3>Estimado/a : {{$datos['name']}}: </h3>
    <h3>Gracias por realizar su pago, su pago fue recibido y ha sido aprobado.</h3>
   
    <p>Gracias por participar.</p>
    <footer>
        {{$footer}}
    </footer>
</body>
</html>