@extends('adminlte::page')
@section('title', 'Eventos UT')
@section('content_header')
    <h1>Inscripción al evento :<h3> {{$evento->nombre}}</h3></h1>
@stop
@section('content')
<x-adminlte-card theme="lightblue" theme-mode="outline">
    <div class="card-body"> 
        @if (session('fail'))
        <x-adminlte-alert theme="danger">
                {{session('fail')}}
            </x-adminlte-alert>
        @endif
       @if (session('done'))
        <x-adminlte-alert theme="info">
                {{session('done')}}
                <p>En su correo electrónico recibira los datos para realizar su pago, cuando lo realice tiene que ir a la seccion de pagos para subir su comprobante. </p>
            </x-adminlte-alert>
        @else
            @if(session('info'))
                @if(isset($evento->archivo->url))
                <x-adminlte-alert theme="info" title="Hey: aún no terminamos" >
                    <i class="text-dark">Si tienes dudas puedes consultar la convocatoria <a href="{{Storage::url($evento->archivo->url)}}" target="_blank">aquí!!</a></i>
                </x-adminlte-alert>
                @endif
               
             
                @livewire('user.datos-pago', ['evento' => $evento])
                <br>
                </div>  
            @else
                @livewire('user.datos-participante', ['evento' => $evento])
            @endif 
        @endif
    </div> 

</x-adminlte-card>
@endsection