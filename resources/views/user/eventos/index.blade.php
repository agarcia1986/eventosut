@extends('adminlte::page')
@section('title', 'Eventos UT')

@section('content_header')

    <h1>Eventos en los que puedes participar</h1>
@stop

@section('content')
@if(session('info'))
    <div class="alert alert-success">
        <strong>{{session('info')}}</strong>
    </div>
@endif
    @livewire('user.evento-index')
@stop