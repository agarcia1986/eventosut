@extends('adminlte::page')
@section('title', 'Eventos UT-Participante')

@section('content_header')
    <h1>UT Cancún</h1>
@stop

@section('content')

<x-adminlte-card theme="success" theme-mode="outline">
    <x-adminlte-info-box title="Bienvenido" text="Si quieres conocer todos nuestros eventos, dirigete a la seccion de eventos." icon="fas fa-lg fa-arrow-left text-dark" theme="gradient-primary"/>

    <x-adminlte-info-box title="Información" text="Si ya realizaste tu inscripcion y ahora vas a subir el comprobante, dirigete directo a la seccion de pagos." icon="fas fa-lg fa-arrow-left text-dark" theme="gradient-teal"/>
</x-adminlte-card>
    
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop