<?php

use App\Http\Controllers\Admin\PonenteController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\CategoriaController;
use App\Http\Controllers\Admin\PagoController;
use App\Http\Controllers\Admin\EventoController;
use App\Http\Controllers\Admin\ReferenciaController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\SubcategoriaController;
use App\Http\Controllers\Admin\PermisoController;
use App\Http\Controllers\Admin\PatrocinadorController;

Route::get('', [HomeController::class, 'index'])->middleware('can:admin.home')->name('admin.home');
Route::resource('users', UserController::class)->only(['index','edit','update','destroy'])->names('admin.users');
Route::resource('roles', RoleController::class)->names('admin.roles');
Route::resource('permisos', PermisoController::class)->names('admin.permisos');
Route::resource('categorias',CategoriaController::class)->names('admin.categorias');
Route::resource('pagos',PagoController::class)->names('admin.pagos');
Route::get('pagos/aprobarpago/{pago}', [PagoController::class, 'aprobarpago'])->name('admin.pagos.aprobarpago');
Route::get('pagos/declinarpago/{pago}', [PagoController::class, 'declinarpago'])->name('admin.pagos.declinarpago');
Route::resource('eventos',EventoController::class)->names('admin.eventos');
Route::get('eventos/asistencia/{evento}', [EventoController::class, 'asistencia'])->name('admin.eventos.asistencia');
Route::get('eventos/lista/{evento}', [EventoController::class, 'lista'])->name('admin.eventos.lista');
Route::get('/admin/eventos/{id}/eventopago', [EventoController::class, 'eventopago'])->name('admin.eventos.eventopago');
Route::resource('participantes', 'App\Http\Controllers\Admin\ParticipanteController')->names('admin.participantes');
Route::resource('referencias',ReferenciaController::class)->names('admin.referencias');
Route::resource('subcategorias',SubcategoriaController::class)->names('admin.subcategorias');
Route::get('subcategorias/listarsubcategorias/',[SubcategoriaController::class,'listarsubcategorias'])->name('admin.subcategorias.listarsubcategorias');
Route::resource('patrocinadores',PatrocinadorController::class)->names('admin.patrocinadores');
Route::resource('ponentes',PonenteController::class)->names('admin.ponentes');
//Route::post('subcategorias/listarsubcategorias',[SubcategoriaController::class,'listarsubcategorias'])->name('admin.subcategorias.listarsubcategorias');
Route::get('users/export/{evento_id}', [UserController::class, 'export'])->name('admin.user.export');