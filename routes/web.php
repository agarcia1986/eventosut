<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EventoController;
use App\Http\Controllers\ParticipanteController;

Route::get('/', [EventoController::class,'index'])->name('eventos.index');
Route::get('Eventos/{evento}', [EventoController::class,'show'])->name('eventos.show');
Route::get('/premiacion', function () {
    return view ('premiacion');
})->name('premiacion');



Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified'
])->group(function () {
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');
});
