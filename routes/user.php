<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\User\HomeController;
use App\Http\Controllers\User\ParticipanteController;
use App\Http\Controllers\User\EventoController;
use App\Http\Controllers\User\PagoController;

Route::get('', [HomeController::class, 'index'])->name('user.home');
Route::get('', [HomeController::class, 'index'])->name('user.home');
Route::resource('participante', ParticipanteController::class)->names('user.participantes');
Route::resource('evento', EventoController::class)->names('user.eventos');
Route::resource('pago', PagoController::class)->names('user.pagos');