const defaultTheme = require('tailwindcss/defaultTheme');
const colors = require('tailwindcss/colors');

module.exports = {
    content: [
        './vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php',
        './vendor/laravel/jetstream/**/*.blade.php',
        './storage/framework/views/*.php',
        './resources/views/**/*.blade.php',
    ],

    theme: {
        extend: {
            fontFamily: {
                sans: ['Nunito', ...defaultTheme.fontFamily.sans],
            },
            colors :{
                utblue :'#024059',
                utblue2 :'#03A6A6',
                utblue3 :'#04BFBF',
                utgreen : '#03A688',
                utfondo : '#F2F2F2',
                blanco : '#FFFFFF'
            },
        },
    },

    plugins: [require('@tailwindcss/forms'), require('@tailwindcss/typography'),require('daisyui')],
    CorePlugins: {
    container: false,
    }
};
