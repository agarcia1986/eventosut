<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Evento;
use App\Models\Participante;
Use App\Models\Categoria;
use App\Models\Pago;
use App\Models\Tipoparticipante;
use App\Models\Subcategoria;

class ParticipanteController extends Controller
{
    
    public function index()
    {
        //$participante= Participante::find(auth()->user()->id);
        $participante= Participante::with('eventos:id')->where('id','=',auth()->user()->id)->get();
        //dd($eventos); 
        return view('user.participantes.index',compact('participante'));
    }
    public function show()
    {
        $eventos = Participante::find(auth()->user()->id);
        return $eventos;
       
    }

    
    public function edit($id)
    {
        //
    }

    
    public function update(Request $request, Participante $participante)
    {
        //return $request;
        if($request->categoria_evento == 5){ //evalua la categoria
            $validated = $request->validate([
                'talla' => 'required',
                'modalidad' =>'required',
                'alergias' => 'required',
                'sexo' => 'required',
                'celular' => 'required',
                'fechanac' => 'required',
                'emergencia' => 'required',
                'tiposanguineo' => 'required',
                'subcategoria'=>'required',
                'responsabilidad' => 'required'
            ]);
            //return $validated;
           
        }else{
            $validated = $request->validate([              
                'sexo' => 'required',
                'celular' => 'required',
                'fechanac' => 'required',
                'subcategoria'=>'required',
        ]);
        //return $validated;
        return back()->with('info','Listo Ahora selecciona el boton inscribirte para continuar.');               
        }
       
    }

    public function destroy($id)
    {
        //
    }
}
