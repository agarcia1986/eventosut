<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Evento;
use App\Models\Participante;
use App\Models\Referencia;
use Illuminate\Support\Facades\Auth;
use App\Models\Pago;
use Illuminate\Support\Carbon;
//use App\Mail\RegistroEventos;
//use Illuminate\Support\Facades\Mail;
use DateTime;

class EventoController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:user.eventos.index')->only('index');
        $this->middleware('can:user.eventos.edit')->only('edit','update');
       // $this->middleware('can:user.eventos.create')->only('create','store');
    }
    
    public function index()
    {
        return view('user.eventos.index');
    }
    
    public function show(Request $request, Evento $evento)
    {
        return view('user.eventos.show');
    }

    
    public function edit( Evento $evento )
    {     
        $participante= Participante::find(auth()->user()->id);
        $tipo= Referencia::find($evento);
        return view('user.eventos.edit', compact('evento','participante','tipo'));
    }

    /**request trae el participante**/
    /**$evento trae los datos del evento**/
    public function update(Request $request, Evento  $evento)
    {   
       
        //return $request;
        $participante= Participante::find($request->participante);
        $inscritos= $evento->setevento->inscritos;
        $capacidad= $evento->setevento->capacidad;
        $fechaInicio= new DateTime($evento->setevento->fechaInicio);
        $fechaFin= new DateTime($evento->setevento->fechaFin);
        $today= Carbon::now();
        //return $today;
       
            if($inscritos < $capacidad){
                $participante->update($request->all());
                $participante->subcategorias()->attach($request->subcategoria);
                $evento->setevento->increment('inscritos');
                $vencimiento = (new DateTime('+10 days'))->format('Y-m-d');
                $pago= Pago::create([
                    'referencia' =>$request->referencia,
                    'monto'=>$request->monto,
                    'fechavencimiento'=> $vencimiento,
                    'tipo' => 1,
                    'estatuspago'=> 3,
                    'participante_id' => $request->participante ]);
                $evento->pagos()->attach($pago);
            }else{
                return back()->with('fail','El evento esta lleno o no admite mas inscritos.');
            }
        return redirect()->route('user.eventos.edit',$evento)->with('done', 'Se inscribió correctamente al evento');
    }
    public function qrcode(Request $request)
    {
        //$qrParticipante= QrCode::size(300)->generate('https://techvblogs.com/blog/generate-qr-code-laravel-9');
        //return view() 
    }
}
