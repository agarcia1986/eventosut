<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Participante;
use Illuminate\Http\Request;
use App\Models\Pago;
use App\Models\Evento;
use Illuminate\Support\Facades\Storage;
use App\Mail\ComprobantePago;
use Illuminate\Support\Facades\Mail;
class PagoController extends Controller
{
    
    public function index()
    {
        
        $pagos = Pago::with('eventos:id,nombre')
        ->where('participante_id',auth()->user()->id)
        ->get();
        

        return view('user.pagos.index', compact('pagos'));
       
    }

    
    public function create(Evento $evento)
    {
        //dd($request);
        return view('user.pagos.create', compact('evento'));        
    }

    
    public function store(Request $request)
    {
        //dump($request);
    }
    
    public function show($id)
    {
        //
    }

    
    public function edit(Pago $pago)
    {
        return view('user.pagos.edit', compact('pago'));
    }

    
    public function update(Request $request, Pago $pago)
    {
         //return $request->file;
         $validated=$request->validate([
            'file' =>'image | max:2048'
         ]);
        $pago->update([
            'estatuspago'=> 2,
        ]);
        //return $request->file;
        if($request->file('file')){
            $url = Storage::put('public/tickets', $request->file('file'));
            
            if($pago->image){
            Storage::delete($pago->image->url);
            $pago->image->update([
                'url' => $url
            ]);
            }else{
                $pago->image()->create([
                'url' => $url
                ]);
            }
        }
        $data= auth()->user();
        $correo = new ComprobantePago($data);
        Mail::to($data['email'])->send($correo);
        return redirect()->route('user.pagos.index', $pago)->with('info','El comprobante se subió  con éxito');
    }

    
    public function destroy(Pago $pago)
    {
       return($pago);
       //return "delete";
        $pago->delete();
        $pago->eventos->delete();
        return $pago;
        return redirect()->route('user.pagos.index')->with('info','Pago eliminado');
    }
}
