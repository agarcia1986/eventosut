<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Evento;
use App\Models\Participante;

class EventoController extends Controller
{
    public function index(){
        $eventos = Evento::with(['image','setevento'])
        ->where('status',2)
        ->latest('id')
        ->paginate(7);
        //return $eventos;
        return view('Eventos.index', compact('eventos'));
        
    }
    public function show(Evento $evento){
        $this->authorize('published',$evento);
        $similares = Evento::with('image','setevento','archivo','participantes:id','ponente')
        ->where('categoria_id',$evento->categoria_id) //misma categoria
        ->where('status',2)//activos
        ->where('id','!=',$evento->id)
        ->latest('id')//por id
        ->take(4)//solo 3
        ->get();
        //$patrocinadores=$evento->patrocinadores();
        //$ponentes= $evento->ponente;
        //dd ($ponentes);
        return view('Eventos.show', compact('evento','similares'));
    }
}
