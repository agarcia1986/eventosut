<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Participante;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class ParticipanteController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:admin.participantes.index')->only('index');
        $this->middleware('can:admin.participantes.edit')->only('edit','update');
        $this->middleware('can:admin.participantes.create')->only('create','store');
    }

    public function index()
    {
       $participante = Participante::find(auth()->user()->id);
        return $participante;
        //return view('admin.participantes.index');
    }

    
    public function create()
    {
        return view('admin.participantes.create');
    }

    public function store(Request $request)
    {
        //
    }

   
    public function show(Participante $participante )
    {
        return view('admin.participantes.show', compact('participante'));
    }

    
    public function edit(Participante $participante )
    {
        return view('admin.participantes.edit');
    }

    
    public function update(Request $request, Participante $participante )
    {
        //
    }

    
    public function destroy(Participante $participante)
    {
        //
    }
}
