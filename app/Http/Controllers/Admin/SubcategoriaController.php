<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Subcategoria;
class SubcategoriaController extends Controller
{
    
    public function index()
    {
        //return "anda";
        return view('admin.subcategorias.index');
    }
    public function listarsubcategorias(Request $request)
    {
       
        return 'llegaste';
        //return response()->json(['success' => $request]);
        //return response()->json([data ="sdsd"]);
        //return view('admin.subcategorias.index');
    }

    
    public function create()
    {
        return view('admin.subcategorias.create');
    }

    
    public function store(Request $request)
    {
       // return $request;
       $request->validate([
        'nombre'=>'required |unique:subcategorias',
        'referencia'=>'required',
        'precio'=> 'required'
    ]);
    $subcategoria= Subcategoria::create($request->all());
    return redirect()->route('admin.subcategorias.index',$subcategoria)->with('info','Subcategoria creada con exito.');

    }

    
    public function show($id)
    {
        //
    }

   
    public function edit(Subcategoria $subcategoria)
    {
        return view('admin.subcategorias.edit',compact('subcategoria'));
    }

    
    public function update(Request $request, $id)
    {
        //
    }

    public function destroy(Subcategoria $subcategoria)
    {
        $subcategoria->delete();
        return redirect()->route('admin.subcategorias.index')->with('info','La Subcategoria se eliminó con éxito');
    }
}
