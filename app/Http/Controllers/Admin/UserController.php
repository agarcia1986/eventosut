<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Spatie\Permission\Models\Role;
use App\Exports\UsersAsistenciaviewExport;
use Maatwebsite\Excel\Facades\Excel;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:admin.users.index')->only('index');
        $this->middleware('can:admin.users.edit')->only('edit','update');
        $this->middleware('can:admin.users.create')->only('create','store');
    }
    
    
    public function index()
    {
        return view('admin.users.index');
    }


    public function edit(User $user)
    {
        $roles = Role::all();
        return view('admin.users.edit', compact('user','roles'));
    }
    public function emailExist($email)
    {
        return 'exist';
    } 

    public function update(Request $request, User $user)
    { 
        $user->roles()->sync($request->roles);
        return redirect()->route('admin.users.edit', $user)->with('info','Los roles se asignaron correctamente');
    }
    public function export($evento_id) 
    {
        //return $evento_id;
        return Excel::download(new UsersAsistenciaviewExport($evento_id), 'users.xlsx');
    }
    public function destroy( User $user){
        if($user->participante){
            $user->participante->delete();
        }
        
        $user->delete();
        return redirect()->route('admin.users.index')->with('info','El usuario se eliminó con éxito');
    }
}
