<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Referencia;

class ReferenciaController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:admin.referencias.index')->only('index');
        $this->middleware('can:admin.referencias.edit')->only('edit','update');
        $this->middleware('can:admin.referencias.create')->only('create','store');
    }
    
    public function index()
    {
        return view('admin.referencias.index');
    }

    
    public function create()
    {
       return view('admin.referencias.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'tipo'=>'required | max:20',
            'referencia'=>'required | min:3 |max:15',
            'precio'=>'required | min:1 |max:4'
        ]);
        Referencia::create($request->all());
        return redirect()->route('admin.referencias.index')->with('info','La referencia se creó con éxito');
    }

    
    public function show($id)
    {
        //
    }

    
    public function edit(Referencia $referencia)
    {
        //return $referencia;
        return view('admin.referencias.edit', compact('referencia'));
    }

  
    public function update(Request $request, Referencia $referencia)
    {
        $request->validate([
            'tipo'=>'required | max:20',
            'referencia'=>'required | min:3 |max:15',
            'precio'=>'required | min:1 |max:4',      
        ]);
        $referencia->update($request->all());
        return redirect()->route('admin.referencias.index', $referencia)->with('info','La referencia se actualizó con éxito');
    }

    
    public function destroy(Referencia $referencia)
    {
        $referencia->delete();
        return redirect()->route('admin.referencias.index')->with('info','La referencia se eliminó con éxito');
    }
}
