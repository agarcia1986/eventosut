<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Tipoparticipante;
use Illuminate\Auth\Events\Validated;
use Illuminate\Http\Request;

class TipoparticipanteController extends Controller
{
    
    public function index()
    {
        return view('admin.tipoparticipante.index');
    }

   
    public function create()
    {
        return view('admin.tipoparticipante.create');
    }

   
    public function store(Request $request)
    {
        $request->validate([
            'tipo'=>'required | max:20',
            'referencia'=>'required | min:3 |max:8',
            'precio'=>'required',
            
        ]);

        Tipoparticipante::create($request->all());
     
     
        return view('admin.tipoparticipante.index')->with('info','Referencia creada con exito.');
    }

    
    public function show($id)
    {
        //
    }

    
    public function edit(Tipoparticipante $tipoparticipante)
    {
        return view('admin.tipoparticipante.edit', compact('tipoparticipante'));
    }

   
    public function update(Request $request, Tipoparticipante $tipoparticipante)
    {
        $request->validate([
            'tipo'=>'required | max:20',
            'referencia'=>'required | min:3 |max:8',
            'precio'=>'required | min:1 |max:4',
            
        ]);
       
        $tipoparticipante->update($request->all());
        return redirect()->route('admin.tipoparticipante.edit', $tipoparticipante)->with('info','Referencia Creada creada');
    }

   
    public function destroy(Tipoparticipante $tipoparticipante)
    {
        //return $tipoparticipante;
        $tipoparticipante->delete();
        return redirect()->route('admin.tipoparticipante.index')->with('info','La referencia se eliminó con éxito');
    }
}
