<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Pago;
use App\Models\Tipoparticipante;
use App\Models\User;
use App\Models\Evento;

class PagoController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:admin.pagos.index')->only('index');
        $this->middleware('can:admin.pagos.edit')->only('edit','update');
        $this->middleware('can:admin.pagos.create')->only('create','store');
    }

    public function index()
    {
        $pagos = Pago::with('eventos:id,nombre')->get();
        $eventos = Evento::where('status',2)->pluck('nombre','id');
        return view('admin.pagos.index', compact('pagos','eventos'));
    }

    

    public function create()
    {
       return view('admin.pagos.create');
    }

    

    public function store(Request $request)
    {
        $request->validate([
            'referencia'=>'required',
            'monto'=>'required',
            'tipo'=>'required'
        ]);
        $pago= Pago::create($request->all());
        return redirect()->route('admin.pagos.edit', compact('pago'));
    }

    

    public function show(Pago $pago)
    {
        //return $pago;
        $user = User::find($pago->participante_id);
       $pago = Pago::with('image')->find($pago->id);
       //return $pago;
        return view('admin.pagos.show', compact('pago','user'));
    }

    

    public function edit(Pago $pago, Evento $evento)
    {
        return $pago;
        //$tipos = Tipoparticipante::pluck('tipoparticipante', 'id');
        $usuario = User::pluck('name', 'id');
        return view('admin.pagos.edit', compact('pago','usuario'));
    }
   

    public function update(Request $request, Pago $pago)
    {
        /*$request->validate([
            'referencia'=>'required',
            'monto'=>'required',
            'tipo'=>'required'
        ]);*/
    }
    public function aprobarpago(Pago $pago)
    {
        $pago->update([
            'estatuspago' => 3
        ]);
        return redirect()->route('admin.pagos.show',$pago)->with('info','Pago aceptado');
    }
    public function declinarpago(Pago $pago)
    {
        $pago->update([
            'estatuspago' => 1
        ]);
        return redirect()->route('admin.pagos.show',$pago)->with('info','Pago declinado');
    }

    public function destroy(Pago $pago)
    {
        $pago->delete();
        return redirect()->route('admin.pagos.index')->with('info','Pago eliminado');
    }
}
