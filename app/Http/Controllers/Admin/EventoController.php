<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Patrocinador;
use Illuminate\Http\Request;
use App\Models\Evento;
use App\Models\Ponente;
use App\Models\Categoria;
use App\Http\Requests\EventoRequest;
use App\Models\Participante;
use App\Models\Setevento;
use App\Models\Referencia;
use App\Models\User;
use App\Models\Pago;
use App\Models\Subcategoria;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;


class EventoController extends Controller
{

    public function __construct()
    {
        
        $this->middleware('can:admin.eventos.index')->only('index');
        $this->middleware('can:admin.eventos.edit')->only('edit','update');
        $this->middleware('can:admin.eventos.create')->only('create','store');
    }
    public function index()
    {
        return view('admin.eventos.index');
    }
   
    public function create()
    {
        $patrocinadores= Patrocinador::with('image')->get();
        $ponentes = Ponente::with('image')->get();
            return view('admin.eventos.create',compact('patrocinadores','ponentes',));
    }
    
    public function store(EventoRequest $request)
    {
        //dd($request);
        $evento = new Evento();   
        DB::beginTransaction();      
            $evento = Evento::create($request->all());
            //return $evento;
            Setevento::create([
            'detalle'=>$request->detalle,
            'requisitos'=>$request->requisitos,
            'video'=>$request->video,   
            'localidad'=>$request->localidad,
            'inscritos' => 0,
            'capacidad' => $request->capacidad,       
            'fechaevento'=>$request->fechaevento,
            'horaInicio'=>$request->horaInicio,
            'iniciainscripcion'=>$request->iniciainscripcion,
            'terminainscripcion'=>$request->terminainscripcion,
            'evento_id'=>$evento->id
            ]);
                    
        if($request->file('image')){
          $url = Storage::put('public/eventos', $request->file('image'));
          $task = $evento->image()->create([
            'url' => $url
            ]);
        }
        if($request->file('archivo')){
            $url = Storage::put('public/eventos', $request->file('archivo'));
            $task = $evento->archivo()->create([
              'url' => $url
              ]);
          }
        $evento->subcategorias()->attach($request->subcategoria);
        $evento->patrocinadores()->attach($request->patrocinador);
        $evento->ponente()->attach($request->ponente);
        DB::commit();
        return redirect()->route('admin.eventos.index',$evento)->with('info','evento creado con éxito');
    }

    //**Esta accion nos mostrara los detalles del evento de la tabla seteventos**//
    public function show(Evento $evento)
    {    
        $similares = Evento::with('image','setevento','archivo')
        ->where('categoria_id',$evento->categoria_id) //misma categoria
        ->where('status',2)//activos
        ->where('id','!=',$evento->id)
        ->latest('id')//por id
        ->take(3)//solo 3
        ->get();
        return view('Eventos.show', compact('evento','similares'));
    }
    
    public function edit(Evento $evento)
    {
        //return $evento;
        $this->authorize('author', $evento);
        $categorias = $evento->categoria->pluck('nombre', 'id');// este nos dara id y nombre para pasarlo al select
        $subcategorias = Subcategoria::all();
        $patrocinadores= Patrocinador::with('image')->get();
        $ponentesall = Ponente::with('image')->get();
        $ponentes= $evento->ponente()->get(['ponente_id']);
        $setevento = $evento->setevento;
        //return $setevento;
        return view('admin.eventos.edit', compact('evento','setevento','categorias','subcategorias','patrocinadores','ponentes','ponentesall'));
    }  
    public function update(EventoRequest $request, Evento $evento)
    {
        //return $request->ponente;
        DB::beginTransaction();   
        $this->authorize('author', $evento);
        $evento->update($request->all());
        $evento->setevento->update([
            'detalle'=>$request->detalle,
            'requisitos'=>$request->requisitos,
            'video'=>$request->video,   
            'localidad'=>$request->localidad,
            //'inscritos' => 0,
            'capacidad' => $request->capacidad,      
            'fechaevento'=>$request->fechaevento,
            'horaInicio'=>$request->horaInicio,
            'iniciainscripcion'=>$request->iniciainscripcion,
            'terminainscripcion'=>$request->terminainscripcion,
            'evento_id'=>$evento->id
        ]);     
        
        if($request->file('image')){
            $url = Storage::put('public/eventos', $request->file('image'));

            if($evento->image){
            Storage::delete($evento->image->url);
            $evento->image->update([
                'url' => $url
            ]);
            }else{
                $evento->image()->create([
                'url' => $url
                ]);
            }
        }
        if($request->file('archivo')){
            $url = Storage::put('public/eventos', $request->file('archivo'));

            if($evento->archivo){
            Storage::delete($evento->archivo->url);
            $evento->archivo->update([
                'url' => $url
            ]);
            }else{
                $evento->archivo()->create([
                'url' => $url
                ]);
            }
        }
        $evento->subcategorias()->sync($request->subcategoria);
        //$evento->referencias()->sync($request->referencias);
        $evento->patrocinadores()->sync($request->patrocinador);
        $evento->ponente()->sync($request->ponente);
        DB::commit();
        return redirect()->route('admin.eventos.index', $evento)->with('info','El evento se actualizó con éxito');
    }

    public function lista(Evento $evento){
       //return "prueba";
        $pagos = $evento->pagos()->get();
        $deta = Evento::with('participantes','pagos','image')->find($evento->id);
       //return $deta;
        return view('admin.eventos.lista', compact('evento','pagos','deta'));
    }
    public function eventopago(Evento $evento)
    {
        return $evento->id;
    }
    public function asistencia(Evento $evento){
        $participantes = Evento::with('participantes')->get();
    
        //dd( $participantes);
        return view('admin.eventos.asistencia', compact('participantes','evento'));
    }
    public function destroy(Evento $evento)
    {
        $this->authorize('author', $evento);
        $evento->Setevento->delete();
        if($evento->image){
            Storage::delete($evento->image->url);   
        };
        if($evento->archivo){
        Storage::delete($evento->archivo->url);
        }
        $evento->delete();
        return redirect()->route('admin.eventos.index')->with('info','El evento se eliminó con éxito');
    }
}
