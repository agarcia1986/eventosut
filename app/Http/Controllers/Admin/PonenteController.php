<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Gradoacademico;
use App\Models\Ponente;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PonenteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.ponentes.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $grados=Gradoacademico::all();
        return view('admin.ponentes.create', compact('grados'));
    }

    public function store(Request $request)
    {
       
       //return $request;
       $request->validate([
            'nombre'=>'required|max:100|min:10',
            'nivelestudio'=> 'required',
            'compania'=> 'required|max:100',
            //'ponencia'=>'required|min:5|max:300',
            'puesto'=>'required'
        ]);
        $ponente= Ponente::create([
            'nombre'=>$request->nombre,
            'nivestudio'=> $request->nivelestudio,
            'compania'=> $request->compania,
            'ponencia'=> $request->ponencia,
            'puesto'=> $request->puesto,
            
        ]);
        if($request->file('profile_photo')){

            $url = Storage::put('public/ponentes', $request->file('profile_photo'));
            //return $url;
            $task = $ponente->image()->create([
              'url' => $url
            ]);
        };
        return redirect()->route('admin.ponentes.index', $ponente)->with('info','El ponente se ha creado con éxito.');
    }

    public function show($id)
    {
        //
    }

    public function edit(Ponente $ponente)
    {
        //return $ponente;
        $grados=Gradoacademico::all();
        return view('admin.ponentes.edit',compact('ponente', 'grados'));
    }

    
    public function update(Ponente $ponente, Request $request)
    {
        $request->validate([
            'nombre'=>'required|max:100|min:10',
            'nivelestudio'=> 'required',
            'compania'=> 'required|max:100',
            'ponencia'=>'required|min:5|max:300',
            'puesto'=>'required'
        ]);
        $ponente->update([
            'nombre'=>$request->nombre,
            'nivestudio'=> $request->nivelestudio,
            'compania'=> $request->compania,
            'ponencia'=> $request->ponencia,
            'puesto'=> $request->puesto,     
        ]);
        if($request->file('profile_photo')){

            $url = Storage::put('public/ponentes', $request->file('profile_photo'));
            //return $url;
            if($ponente->image){
                Storage::delete($ponente->image->url);
                $ponente->image->update([
                    'url' => $url
                ]);
                }else{
                    $ponente->image()->create([
                    'url' => $url
                    ]);
                }
        };
        return redirect()->route('admin.ponentes.index', $ponente)->with('info','El ponente se ha actualizado con éxito.');

    }

    public function destroy(Ponente $ponente)
    {
        if($ponente->image){
            //return($patrocinadore->image->url);
            Storage::delete($ponente->image->url);
            $ponente->image->delete();
        }
        $ponente->delete();
        return redirect()->route('admin.ponentes.index', $ponente)->with('info','El ponente se eliminó con éxito');
    }
}
