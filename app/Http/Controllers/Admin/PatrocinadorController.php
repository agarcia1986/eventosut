<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Patrocinador;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;


class PatrocinadorController extends Controller
{
    
    public function index()
    {
        return view('admin.patrocinadores.index');
    }

   
    public function create()
    {
        return view('admin.patrocinadores.create');
    }

    
    public function store(Request $request)
    {
         //return $request;
        $request->validate([
            'nombre'=>'required|max:100|min:5',
            'descripcion'=> 'required|max:200',
            'pagina_web'=> 'required|max:100',
        ]);
        $redes= ['facebook'=>$request->facebook,'instagram'=>$request->instagram,'tiktok'=>$request->tiktok,'x'=>$request->x];
        //$redes= array('Facebook'=>['Facebook',$request->fb],'Instagram'=>['Instagram',$request->insta],'tiktok'=>['Tik Tok',$request->tiktok],'twitter'=>['Twitter',$request->twitter]);
        //DB::beginTransaction();
        
        $patrocinador= Patrocinador::create([
            'nombre'=>$request->nombre,
            'descripcion'=> $request->descripcion,
            'pagina_web'=> $request->pagina_web,
            'redes'=> json_encode($redes)
        ]);
        if($request->file('logo')){

            $url = Storage::put('public/patrocinadores', $request->file('logo'));
            //return $url;
            $task = $patrocinador->image()->create([
              'url' => $url
            ]);
        };
        
        //return $patrocinador;
        //DB::commit();
        return redirect()->route('admin.patrocinadores.index', $patrocinador)->with('info','El patrocinador se ha creado con éxito.');
    }

  
    public function show(Patrocinador $patrocinador)
    {
        //
    }

   
    public function edit(Patrocinador $patrocinadore)
    {
        //return $patrocinadore;
       // $patrocinador= Patrocinador::with('image')->where('id', $id)->get();
        //return $patrocinador;
        return view('admin.patrocinadores.edit',compact('patrocinadore'));
    }

    
    public function update(Patrocinador $patrocinadore,Request $request)
    {
        $request->validate([
            'nombre'=>'required|max:100|min:5',
            'descripcion'=> 'required|max:200',
            'pagina_web'=> 'required|max:100',
        ]);
        $redes= ['facebook'=>$request->facebook,'instagram'=>$request->instagram,'tiktok'=>$request->tiktok,'twitter'=>$request->twitter];
        
        $patrocinadore->update([
            'nombre'=>$request->nombre,
            'descripcion'=> $request->descripcion,
            'pagina_web'=> $request->pagina_web,
            'redes'=> json_encode($redes)
        ]);
        if($request->file('logo')){
            $url = Storage::put('public/patrocinadores', $request->file('logo'));
            if($patrocinadore->image){
            Storage::delete($patrocinadore->image->url);
            $patrocinadore->image->update([
                'url' => $url
            ]);
            }else{
                $patrocinadore->image()->create([
                'url' => $url
                ]);
            }
        }
        return redirect()->route('admin.patrocinadores.index', $patrocinadore)->with('info','El patrocinador se actualizó con éxito');
    }

    
    public function destroy(Patrocinador $patrocinadore)
    {
        if($patrocinadore->image){
            //return($patrocinadore->image->url);
            Storage::delete($patrocinadore->image->url);
            $patrocinadore->image->delete();
        }
        $patrocinadore->delete();
        return redirect()->route('admin.patrocinadores.index', $patrocinadore)->with('info','El patrocinador se eliminó con éxito');
    }
}
