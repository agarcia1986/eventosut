<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;

class PermisoController extends Controller
{
    
    public function index()
    {
        
        return view('admin.permisos.index');
    }

    
    public function create()
    {
        return view('admin.permisos.create');
    }

   
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'description'=>'required'
        ]);
        $permiso = Permission::create([
            'name'=>$request->name,
            'description' => $request->description,
            'guard_name' => 'web'
        ]);
        return redirect()->route('admin.permisos.index', $permiso)->with('info','El permiso se ha creado con éxito.');
    }

   
    public function show($id)
    {
        //
    }

   
    public function edit($id)
    {
        //
    }

    
    public function update(Request $request, $id)
    {
        //
    }

    
    public function destroy(Permission $permiso)
    {
        $permiso->delete();
        return redirect()->route('admin.permisos.index')->with('info','El permiso se eliminó con éxito');
    }
}
