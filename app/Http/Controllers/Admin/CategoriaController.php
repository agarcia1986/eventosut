<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Categoria;
use App\Models\Subcategoria;

class CategoriaController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:admin.categorias.index')->only('index');
        $this->middleware('can:admin.categorias.edit')->only('edit','update');
        $this->middleware('can:admin.categorias.create')->only('create','store');
    }
   
    public function index()
    {
        $categorias = Categoria::all();
        return view('admin.categorias.index', compact('categorias')); 
    }

    
    public function create()
    {
        $subcategorias = Subcategoria::all();
        return view('admin.categorias.create', compact('subcategorias'));
    }

    
    public function store(Request $request)
    { 
        $request->validate([
            'nombre'=>'required',
            'slug'=>'required| unique:categorias'
        ]);
        //return $request->all();
        $categoria = Categoria::create($request->all());
        $categoria->subcategorias()->attach($request->subcategorias);
        return redirect()->route('admin.categorias.index', $categoria)->with('info','Categoría creada');
    }

    
    public function show(Categoria $categoria)
    {
        return view('admin.categorias.show',compact('categoria'));
    }

    
    public function edit(Categoria $categoria)
    {
        $subcategorias = Subcategoria::all();
        return view('admin.categorias.edit',compact('categoria','subcategorias'));
    }

   
    public function update(Request $request, Categoria $categoria)
    {
        //return $request;
        $request->validate([
            'nombre'=>'required',
            'slug'=>"required| unique:categorias,slug,$categoria->id"
        ]);
        $categoria->update($request->all());
        $categoria->subcategorias()->sync($request->subcategorias);
        return redirect()->route('admin.categorias.index', $categoria)->with('info','Categoría actualizada');

    }

    
    public function destroy(Categoria $categoria)
    {
        $categoria->delete();
        return redirect()->route('admin.categorias.index')->with('info','Categoría eliminada');
    }
}
