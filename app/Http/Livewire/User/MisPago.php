<?php

namespace App\Http\Livewire\User;

use Livewire\Component;

class MisPago extends Component
{
    public function render()
    {
        return view('livewire.user.mis-pago');
    }
}
