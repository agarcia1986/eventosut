<?php

namespace App\Http\Livewire\User;

use App\Models\Participante;
use App\Models\Referencia;
use Livewire\Component;
use App\Models\Evento;
use \App\Models\User;
use \App\Models\Subcategoria;
class DatosParticipante extends Component
{
    public $evento;
    public $club;
    public $existe;
    public $test= false;
    public function render()
    {
        $participante = Participante::find(auth()->user()->id);
        $even =$this->evento;
        $existe = $even->participantes()->find($participante->id);
        //$evento = $even->referencias()->get();
        $subcategorias= $even->subcategorias()->get();
        //dd($subcategorias);
        return view('livewire.user.datos-participante', compact('participante','subcategorias','existe'));

    }
    public function updatedClub()
    {
      return $this->club;
    }
}
