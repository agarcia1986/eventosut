<?php

namespace App\Http\Livewire\User;

use App\Models\Pago;
use App\Models\Evento;
use App\Models\Participante;
use App\Models\Tipoparticipante;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class DatosPago extends Component
{
    public $evento;
    public function render()
    {
       
        $participante = Participante::find(Auth()->user()->id);       
        $evento = $this->evento;
        if(!$referencia= $participante->subcategorias()->get()){
           $referencia="free"; 
        }
        //dump($referencia);
        return view('livewire.user.datos-pago',compact('referencia','evento'));
    }
}
