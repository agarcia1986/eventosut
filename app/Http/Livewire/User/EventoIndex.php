<?php

namespace App\Http\Livewire\User;

use Livewire\Component;
use App\Models\Evento;
use App\Models\Participante;
use Illuminate\Support\Facades\Auth;
use Livewire\WithPagination;
use App\Models\Pago;
use Carbon\Carbon;

class EventoIndex extends Component
{
    use WithPagination;
    protected $paginationTheme = "bootstrap";
    //public $hoy = Carbon::now()->format(Y-m-d);
    public function render()
    {
        $hoy = Carbon::now()->format('Y-m-d');
        
        // Número de eventos por página
        $porPagina = 9; // Ajusta este número según tus necesidades
        
        /*$eventos = Evento::select('eventos.*') // Selecciona todas las columnas de la tabla eventos
            ->join('Seteventos', 'eventos.id', '=', 'Seteventos.evento_id') // Asegúrate de que la clave foránea es correcta
            ->where('eventos.status', 2)
            ->where(function ($query) use ($hoy) {
                $query->whereBetween($hoy, ['Seteventos.iniciainscripcion', 'Seteventos.terminainscripcion'])
                      ->orWhere('Seteventos.fechaevento', '>=',$hoy);
            })
            ->orderBy('Seteventos.fechaevento', 'asc') // Ordenar por fecha del evento en orden ascendente
            ->paginate($porPagina)
            ->withQueryString(); 
        */
        $eventos = Evento::with(['Setevento','participantes:id','pagos:id,estatuspago,participante_id','image','categoria'])
        ->where('status',2)
        ->latest('id')
        ->paginate(9);
        return view('livewire.user.evento-index', compact('eventos'));
        
    }
}
