<?php

namespace App\Http\Livewire\Admin;

use App\Models\Subcategoria;
use Livewire\Component;
use Livewire\WithPagination;
class Subcategorias extends Component
{
    use WithPagination;
    protected $paginationTheme = "bootstrap";
    public $search;  //vincula la barra de busqueda en la vista
    public function updatingSearch(){ //sirve para resetear la paginacion de la tabla
        $this->resetPage();

    }
    public function render()
    {
        $subcategorias = Subcategoria::where('nombre','LIKE' ,'%'.$this->search.'%')
        ->latest('id')
        ->paginate(10);
        return view('livewire.admin.subcategorias', compact('subcategorias'));
        
    }
}
