<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\Models\Categoria;
use App\Models\Subcategoria;

class CategoriaSubcategoria extends Component
{
    public $categorias;
    public $categoriaId;
    public $wsubcategorias;
    public $wsubcategoriasId;
    public function mount(){
        $this->categorias = Categoria::orderby('nombre')->get();
    }
    public function updatedCategoriaId(){
        
        //$this->wsubcategorias="uno";
        if($this->categoriaId !=''){
            $this->wsubcategorias = Categoria::find($this->categoriaId)->subcategorias;
        }else{
            $data= array("id" => "No","nombre"=> "hay datos");
            $this->wsubcategorias =json_encode($data);
        }
    }
    public function render()
    {
        return view('livewire.admin.categoria-subcategoria');
    }
    public function mostrarSubcategorias(){
       $wiresubcategorias="hola";
        //$subcategorias= Categoria::find($categoria)->subcategoria->get();
    }
}

