<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\Models\Referencia;

class ReferenciaIndex extends Component
{
    public $search;
    public function render()
    {
        $referencias = Referencia::where('tipo', 'like','%'.$this->search .'%')
        ->orWhere('tipo', 'like','%'.$this->search .'%')
        ->latest('id')
        ->paginate(10);
        return view('livewire.admin.referencia-index',compact('referencias'));
    }
}
