<?php

namespace App\Http\Livewire\Admin;

use App\Models\Ponente;
use Livewire\Component;
use Livewire\WithPagination;

class PonentesIndex extends Component
{
    use WithPagination;
    protected $paginationTheme = "bootstrap";
    public $search;  //vincula la barra de busqueda en la vista
    protected $listeners=['render'=>'render'];
    public function updatingSearch(){ //sirve para resetear la paginacion de la tabla
        $this->resetPage();

    }
    public function render()
    {
        
        $ponentes = Ponente::with('image')
        ->where('nombre','LIKE' ,'%'.$this->search.'%')
        ->latest('id')
        ->paginate(10);
        //dd($ponentes);
        return view('livewire.admin.ponentes-index', compact('ponentes'));
        //return view('livewire.admin.patrocinadores-index');
       
    }
}
