<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\Models\User;
use App\Models\Evento;

class Asistencia extends Component
{
    protected $listeners = ['asistenciaP'];
    public $nombreP;
    //public $decodedText;
    public function render()
    {
       //$idParticipante = $this->decodedText;
        return view('livewire.admin.asistencia');
    }
    public function asistenciaP($eventoinf)
    {
       $evento = Evento::with('pagos')->find($eventoinf[1])->all();
       //dd($evento); 
       $existe= array_search($eventoinf[0],$evento);
       dd($existe);
       $this->nombreP= $eventoinf;
        
    }
}