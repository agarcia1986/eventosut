<?php

namespace App\Http\Livewire\Admin;
use Spatie\Permission\Models\Permission;
use Livewire\WithPagination;

use Livewire\Component;

class PermisosIndex extends Component
{
    use WithPagination;
    protected $paginationTheme = "bootstrap";
    public $search;  //vincula la barra de busqueda en la vista
    public function updatingSearch(){ //sirve para resetear la paginacion de la tabla
        $this->resetPage();

    }
    public function render()
    {
    $permisos = Permission::where('name','LIKE' ,'%'.$this->search.'%')
    ->orWhere('description', 'like','%'.$this->search .'%')
    ->latest('id')
    ->paginate(10);
        return view('livewire.admin.permisos-index',compact('permisos'));
    }
}
