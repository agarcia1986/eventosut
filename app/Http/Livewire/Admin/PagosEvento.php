<?php

namespace App\Http\Livewire\Admin;

use App\Exports\UsersPagosExport;
use App\Exports\UsersAsistenciaExport;
use Livewire\Component;
use App\Models\Evento;
use App\Models\Pago;
use Illuminate\Database\Eloquent\Collection;
use Livewire\WithPagination;

class PagosEvento extends Component
{
    use WithPagination;
    public Collection $pagosexport;
    public $selectEvento= null;
    public $pagos = null;
    public $evento = null;
   // public $pagose;
    
    //ESTO DA LOS VALORES PARA EL SELECT
    public function updatedselectEvento($evento_id)
    {
       if($evento_id != "vacio"){
        $this->pagos = Pago::select('pagos.id','users.name','users.pname','users.mname','users.email','participantes.celular','participantes.numero','pagos.referencia','pagos.monto','participantes.talla','participantes.modalidad','participantes.tiposanguineo','participantes.emergencia','participantes.responsabilidad','pagos.estatuspago')
        ->join ('participantes', 'participantes.id', '=','pagos.participante_id')
        ->join ('users','users.id', '=','participantes.id')
        ->join('evento_pago','evento_pago.pago_id','=','pagos.id')
        ->where('evento_pago.evento_id','=',$evento_id)
        ->get();
        
       }else{
        $this->reset('evento','pagos');
       }
    }
    
    public function render()
    {
        $eventos= Evento::with('participantes:id,talla')->where('status',2)
        ->orderBy('id', 'DESC')
        ->get(['id','nombre']);
        return view('livewire.admin.pagos-evento',compact('eventos'));
    }
    public function exportPagos(){
        return (new UsersPagosExport($this->selectEvento))->download('Pagos.xlsx');
    }
    public function exportAsistencia(){
        return (new UsersAsistenciaExport($this->selectEvento))->download('ListaAsistencia.xlsx');
    }
}
