<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\Models\Patrocinador;
use Livewire\WithPagination;

class PatrocinadoresIndex extends Component
{
    use WithPagination;
    protected $paginationTheme = "bootstrap";
    public $search;  //vincula la barra de busqueda en la vista
    protected $listeners=['render'=>'render'];
    public function updatingSearch(){ //sirve para resetear la paginacion de la tabla
        $this->resetPage();

    }
    public function render()
    {
        $patrocinadores = Patrocinador::with('image')
        ->where('nombre','LIKE' ,'%'.$this->search.'%')
        ->latest('id')
        ->paginate(10);
        return view('livewire.admin.patrocinadores-index', compact('patrocinadores'));
        //return view('livewire.admin.patrocinadores-index');
    }
}
