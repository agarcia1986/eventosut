<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;

class CreateEvento extends Component
{
    public function render()
    {
        return view('livewire.admin.create-evento');
    }
}
