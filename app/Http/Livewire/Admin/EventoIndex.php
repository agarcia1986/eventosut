<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\Models\Evento;
use Livewire\WithPagination;

class EventoIndex extends Component
{
    use WithPagination;
    protected $paginationTheme = "bootstrap";
    public $search;  //vincula la barra de busqueda en la vista
    public function updatingSearch(){ //sirve para resetear la paginacion de la tabla
        $this->resetPage();

    }
    public function render()
    {
        $eventos = Evento::with('pagos','setevento')->where('nombre','LIKE' ,'%'.$this->search.'%')
        ->latest('id')
        ->paginate(10);
        return view('livewire.admin.evento-index', compact('eventos'));
    }
}
