<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EventoRequest extends FormRequest
{
   
    public function authorize()
    {
        
       return true;
    }

   
    public function rules()
    {
    
        $evento = $this->route()->parameter('evento');
        
        $rules = [
            'nombre' => 'required',
            'slug' => 'required|unique:eventos',
            'status' => 'required|in:1,2',
            'image' => 'image',
            'categoria_id' => 'required'
        ];
        if($evento){
            $rules['slug'] = 'required| unique:eventos,slug,'.$evento->id;
        }
        if($this->status == 2){
                $rules = array_merge($rules, [
                'intro' => 'required | min:20 | max:360',
                'informacion' => 'required',
                'localidad' => 'required',
                'requisitos' => 'required',
                'capacidad' => 'required',
                'fechaevento'=>'required',
                'horaInicio' => 'required',
                'iniciainscripcion'=>'required',
                'terminainscripcion'=>'required'
            ]);
        }
        return $rules;
    }
}