<?php

namespace App\Observers;

use App\Models\Evento;
use Illuminate\Support\Facades\Storage;

class EventoObserver
{
    
    public function creating(Evento $evento)
    {
        if(! \App::runningInConsole()){
            $evento->user_id = auth()->user()->id;
        }
    }

    
    public function deleting(Evento $evento)
    {
        if($evento->image){
            Storage::delete($evento->image->url);
        }
    }

}
