<?php

namespace App\Actions\Fortify;

use App\Models\Participante;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Laravel\Fortify\Contracts\CreatesNewUsers;
use Laravel\Jetstream\Jetstream;
use Laravel\Jetstream\Rules\Role;
use App\Mail\RegistroEventos;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;

class CreateNewUser implements CreatesNewUsers
{
    use PasswordValidationRules;

    
    public function create(array $input)
    {
        //dd ($input);
        Validator::make($input, [
            'name' => ['required', 'string', 'max:50'],
            'pname' => ['required', 'string', 'max:50'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => $this->passwordRules(),
            'celular' => ['required', 'string', 'min:8', 'max:13'],
            'sexo' =>['required'],
            'modalidad' => ['required'],
            'terms' => Jetstream::hasTermsAndPrivacyPolicyFeature() ? ['accepted', 'required'] : ''
        ])->validate();


        DB::beginTransaction(); 
         $userdata = User::create([
            'name' => strtoupper($input['name']),
            'pname' => strtoupper ($input['pname']),
            'mname' => strtoupper($input['mname']),
            'email' => $input['email'],
            'password' => Hash::make($input['password'])
        ])->assignRole('Participante');
         Participante::create([
            'talla' => '',
            'alergias' => '',
            'condicion' => '',
            'sexo' => $input['sexo'],
            'club' => '',
            'celular' => $input['celular'], 
            'modalidad' => $input['modalidad'],       
            'user_id' => $userdata->id
        ]);
        DB::commit();
        return $userdata;
    }
}
