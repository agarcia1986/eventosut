<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ComprobantePago extends Mailable
{
    use Queueable, SerializesModels;

    public $subject = 'Comprobante recibido';
    public $datos;
    public $footer= "Cualquier informacion con respecto a su pago,favor de llamar al 8811900 ext.1092";

    public function __construct($input)
    {
        $this->datos=$input;
    }

    
    public function build()
    {
        return $this->view('user.email.comprobantepago');
    }
}
