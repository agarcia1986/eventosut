<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;


class RegistroEventos extends Mailable
{
    use Queueable, SerializesModels;

    public $subject = 'Registro exitoso en plataforma de Eventos UT';
    public $datos;
    public $footer= "Cualquier informacion favor de llamar al 8811900 ext.1092";

    public function __construct($input)
    {
        $this->datos=$input;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('user.email.registroeventos');
    }
}
