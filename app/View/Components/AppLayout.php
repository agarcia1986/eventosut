<?php

namespace App\View\Components;

use Illuminate\View\Component;

class AppLayout extends Component
{
    /**Desde aqui inicia la plantilla **/
    public function render()
    {
        return view('layouts.app');
    }
}
