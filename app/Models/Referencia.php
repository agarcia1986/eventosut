<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Referencia extends Model
{
    use HasFactory;
    protected $fillable = [
        'tipo',
        'referencia',
        'precio'
    ];
    public function eventos(){
        return $this->morphedByMany(Evento::class,'referenciable');
    }
    public function participantes (){
        return $this->morphedByMany(Participante::class,'referenciable');
    }
}
