<?php

namespace App\Models;

use Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Evento extends Model
{
    use HasFactory;
    protected $guarded =['id', 'created_at', 'updated_at'];
    public function user(){
        return $this->belongsTo('App\Models\User');
    }
    public function categoria(){
        return $this->belongsTo(Categoria::class);
    }
    public function image(){
        return $this->morphOne(Image::class, 'imageable');
    }
    public function archivo(){
        return $this->morphOne(Archivo::class, 'archivable');
    }
    public function pagos(){
        return $this->belongsToMany(Pago::class)->withTimestamps();
    }
    public function participantes(){
        return $this->belongsToMany(Participante::class)->withTimestamps();
    }
   public function setevento(){
        return $this->hasOne(Setevento::class);
    }
    public function correo(){
        return $this->hasOne(Correo::class);
    }
    public function referencias(){
        return $this->morphToMany(Referencia::class,'referenciable');
    }
    public function patrocinadores(){
        return $this->belongsToMany(Patrocinador::class)->withTimestamps();
    }
    public function ponente(){
        return $this->belongsToMany(Ponente::class)->withTimestamps();
    }
    public function subcategorias(){
        return $this->belongsToMany(Subcategoria::class)->withTimestamps();
    }
    protected function informacion(): Attribute
    {
        return Attribute::make(
            set: fn(string $value) => $this->makeInformacionContent($value),
        );  
    }
    public function makeInformacionContent($content)
    {
        $dom = new \DomDocument();
        $dom->loadHtml($content, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $imageFile = $dom->getElementsByTagName('img');  
            foreach($imageFile as $item => $image){
                $data = $image->getAttribute('src');
                list($type, $data) = explode(';', $data);
                list(, $data)      = explode(',', $data);
                $imgeData = base64_decode($data);
                $image_name= "/uploads/" . time().$item.'.png';
                $path = public_path() . $image_name;
                file_put_contents($path, $imgeData);
                     
                $image->removeAttribute('src');
                $image->setAttribute('src', $image_name);
            }
            return $dom->saveHTML();
    }
}
