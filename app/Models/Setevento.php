<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Evento;
use Illuminate\Support\Carbon;

class Setevento extends Model
{
    use HasFactory;
    protected $fillable = [
        'localidad',
        'detalle',
        'requisitos',
        'video',
        'fechaevento',
        'horaInicio',
        'inscritos',
        'capacidad', 
        'iniciainscripcion',
        'terminainscripcion',
        'evento_id'
    ];
  
    public function getfechaInicioAttribute($value)
    {
        return Carbon::parse($value);
    }
    public function getfechaFinAttribute($value)
    {
        return Carbon::parse($value);
    }
    public function getfechaEventoAttribute($value)
    {
        return Carbon::parse($value);
    }
    public function evento(){
        return $this->belongsTo(Evento::class);
    }
}

