<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Participante extends Model
{
    use HasFactory;
    protected $fillable = [
        'talla',
        'alergias',
        'condicion',
        'sexo',
        'numero',
        'club',
        'celular',
        'fechanac',
        'modalidad',
        'enfermedad',
        'tiposanguineo',
        'emergencia',
        'responsabilidad',
        'user_id'
    ];

    public function eventos(){
        return $this->belongsToMany(Evento::class);
    }
    public function pagoEvento(){
        return $this->hasOneThrough(Pago::class, Evento::class);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function referencias(){
        return $this->morphToMany(Referencia::class,'referenciable');
    }
    public function subcategorias(){
        return $this->belongsToMany(Subcategoria::class);
    }
}
