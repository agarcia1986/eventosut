<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subcategoria extends Model
{
    public $timestamps = false;
    use HasFactory;
    protected $fillable = [
        'nombre',
        'referencia',
        'precio'
    ];

    public function categorias(){
        return $this->belongsToMany('App\Models\Categoria');
    }
    public function eventos(){
        return $this->belongsToMany('App\Models\Evento');
    }
    public function participantes(){
        return $this->belongsToMany('App\Models\Participante');
    }
}
