<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pago extends Model
{
    use HasFactory;

    protected $guarded =['id', 'created_at', 'updated_at'];
    
    public function eventos(){
        return $this->belongsToMany(Evento::class);
    }
    public function image(){
        return $this->morphOne(Image::class, 'imageable');
    }
}
