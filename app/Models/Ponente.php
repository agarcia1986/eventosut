<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ponente extends Model
{
    use HasFactory;

    protected $fillable = [
        'nombre',
        'nivestudio',
        'puesto',
        'compania',
        'ponencia'
    ];
    public function image(){
        return $this->morphOne(Image::class, 'imageable');
    }
}
