<?php

namespace App\Exports;

use App\Models\User;
use App\Models\Pago;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Support\Facades\DB;
class UsersAsistenciaviewExport implements FromView
{
    public function __construct($evento_id)
    {
        $this->evento_id=$evento_id;
        return $this;
    }

    public function view():View
    {
        return view('asistencias', [
            'usuarios' => Pago::select(DB::raw("CONCAT(users.pname,' ',users.mname,' ',users.name )  AS nombre"))
            ->join ('participantes', 'participantes.id', '=','pagos.participante_id')
            ->join ('users','users.id', '=','participantes.id')
            ->join('evento_pago','evento_pago.pago_id','=','pagos.id')
            ->where('evento_pago.evento_id','=',$this->evento_id)
            ->orderby('users.pname','ASC')
        ]);
    }
}
