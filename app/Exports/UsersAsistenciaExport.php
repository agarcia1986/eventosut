<?php

namespace App\Exports;

use App\Models\User;
use App\Models\Pago;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Support\Facades\DB;

class UsersAsistenciaExport implements FromQuery, WithHeadings, ShouldAutoSize
{
    use Exportable;
    public function __construct($evento_id)
    {
        $this->evento_id=$evento_id;
        return $this;
    }
    public function headings():array
    {
        return [
            'Nombre Completo',
            'Grupo',
            'Asistencia'
        ];
    }
    public function query()
    {
        //return Pago::all();

        //**NO INCLUIR PAGO.ID PORQUE ES IRRELEVANTE EN EL EXCEL **/
        return Pago::select(DB::raw("CONCAT(users.pname,' ',users.mname,' ',users.name )  AS Nombre"))
        ->join ('participantes', 'participantes.id', '=','pagos.participante_id')
        ->join ('users','users.id', '=','participantes.id')
        ->join('evento_pago','evento_pago.pago_id','=','pagos.id')
        ->where('evento_pago.evento_id','=',$this->evento_id)
        ->orderby('users.pname','ASC');
    }
}
