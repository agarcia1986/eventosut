<?php

namespace App\Exports;

use App\Models\Pago;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
   

class UsersPagosExport implements FromQuery, WithHeadings
{
    use Exportable;

    public function __construct($evento_id)
    {
        $this->evento_id=$evento_id;
        return $this;
    }
    public function headings():array
    {
        return [
            'Nombre',
            'Paterno',
            'Materno',
            'Email',
            'Celular',
            'Numero',
            'Referencia',
            'Monto',
            'Talla',
            'modalidad',
            'tipo sanguineo',
            'Tel. emergencia',
            'Acuerdo de responsabilidad',
            'Estatus(1=Espera,2=Revision,3=Pago aprobado)',
        ];
    }
    public function query()
    {
        //return Pago::all();

        //**NO INCLUIR PAGO.ID PORQUE ES IRRELEVANTE EN EL EXCEL **/
        return Pago::select('users.name','users.pname','users.mname','users.email','participantes.celular','participantes.numero','pagos.referencia','pagos.monto','participantes.talla','participantes.modalidad','participantes.tiposanguineo','participantes.emergencia','participantes.responsabilidad','pagos.estatuspago')
        ->join ('participantes', 'participantes.id', '=','pagos.participante_id')
        ->join ('users','users.id', '=','participantes.id')
        ->join('evento_pago','evento_pago.pago_id','=','pagos.id')
        ->where('evento_pago.evento_id','=',$this->evento_id);
    }
}
