<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Evento;
use Illuminate\Auth\Access\HandlesAuthorization;

class EventoPolicy
{
    use HandlesAuthorization;
    /**POLITICA PARA QUE NO PUEDAS EDITAR UN EVENTO QUE NO SEA TUYO **/
    public function author(User $user, Evento $evento)
    {
        /*return $user;
        if ($user->id == $evento->user_id || $user->roles()->name == 'Admin' ) {
            return true;
        }else{
            return false;
        }*/
        return true;
    }
    /**POLITICA PARA QUE NO PUEDAS VER UN EVENTO QUE NO ESTE PUBLICADO AUN **/
    public function published(?User $user, Evento $evento){ //el signo de interrogacion, permite que ese parametro sea opcional.
        if ($evento->status == 2) {
            return true;
        }else{
            return false;
        }
    }
}
