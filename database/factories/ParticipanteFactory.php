<?php

namespace Database\Factories;

use App\Models\Tipoparticipante;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\User;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Participante>
 */
class ParticipanteFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'talla'=>$this->faker->randomElement(['extra chica','chica','mediana','grande','extragrande']),
            'alergias'=>$this->faker->word(20),
            'condicion'=>$this->faker->word(20),
            'sexo'=>$this->faker->randomElement([1,2]),
            'club'=>$this->faker->word(20),
            'celular'=>$this->faker->tollFreePhoneNumber(),
            'user_id'=>User::all()->random()->id
        ];
    }
}
