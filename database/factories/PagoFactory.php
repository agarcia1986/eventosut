<?php

namespace Database\Factories;

use App\Models\Evento;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Pago>
 */
class PagoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        
        return [
            'referencia'=> $this->faker->unique()->word(10),
            'fechavencimiento'=>$this->faker->dateTime('2022-05-30 00:00.00'),
            'monto'=>$this->faker->numberBetween($min = 100, $max = 2000) ,
            'numtarjeta'=>$this->faker->creditCardNumber(),       
            'mesexpira'=>$this->faker->creditCardExpirationDateString(),
            'cve'=>$this->faker->numberBetween($min= 1, $max=999),
            'tipo'=>$this->faker->numberBetween($min = 1, $max = 3),
            'estatuspago'=>$this->faker->numberBetween($min = 1, $max = 3),
        ];
    }
}
