<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Categoria>
 */
class CategoriaFactory extends Factory
{
    
    public function definition()
    {
        $name= $this->faker->unique()->word(20);
        return [
            'nombre'=> $name,
            'slug' =>Str::slug($name)
        ];
    }
}
