<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;


class ImageFactory extends Factory
{
    
    public function definition()
    {
        return [
            'url'=> 'eventos/' .$this->faker->image('public/storage/eventos', 1024, 480,null,false),
        ];
    }
}
