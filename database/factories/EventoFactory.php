<?php

namespace Database\Factories;

use App\Models\Categoria;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class EventoFactory extends Factory
{
    
    public function definition()
    {
        $name=$this->faker->unique()->sentence();
        return [
            'nombre' => $name,
            'slug'  => Str::slug($name),
            'intro' => $this->faker->text(100),
            'informacion' =>$this->faker->text(2000),
            'status'=>$this->faker->randomElement([1,2]),
            'categoria_id' => Categoria::all()->random()->id,
            'user_id' =>User::all()->random()->id
        ];
    }
}
