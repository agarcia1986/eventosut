<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Evento;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Setevento>
 */
class SeteventoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'detalle'=>$this->faker->word(100),
            'requisitos'=>$this->faker->shuffle('ser mayor de edad','ser alumno','definir su categoria','','','','','','',''),
            'video'=>$this->faker->word(50),
            'localidad'=>$this->faker->address(),
            'iniciainscripcion'=>$this->faker->date(),
            'terminainscripcion'=>$this->faker->date(),
            'fechaevento'=>$this->faker->date(),
            'capacidad'=> $this->faker->randomElement([20,300]),
            'inscritos'=>$this->faker-> randomElement([10,20]),
            //'evento_id'=>  Evento::all()->random()->id,   
        ];
    }
}
