<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Evento;
use App\Models\Image;
use App\Models\Setevento;


class EventoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $eventos= Evento::factory(6)->create();
        foreach($eventos as $evento){
          Image::factory(1)->create([
              'imageable_id' => $evento->id,
              'imageable_type'=>Evento::class
          ]);
          $evento->pagos()->attach([
              rand(1,3),
              rand(4,6)
          ]);
          $evento->participantes()->attach([
            rand(1,3),
            rand(4,6)
          ]);
          Setevento::factory(1)->create([
            'evento_id' =>$evento->id,
          ]);
          $evento->referencias()->attach([
            rand(1,2),
            rand(3,5)
          ]);
          $evento->pagos()->attach([
            rand(1,3),
            rand(4,6)
          ]);
        }
    }
}
