<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Categoria;

class CategoriaSeeder extends Seeder
{
    
    public function run()
    {
        Categoria::create([
            'nombre' =>'Deportivo',
            'slug' =>'deportivo'
        ]);
        Categoria::create([
            'nombre' =>'Educativo',
            'slug' =>'educativo'
        ]);
        Categoria::create([
            'nombre' =>'Ceremonia',
            'slug' =>'ceremonia'
        ]);
        Categoria::create([
            'nombre' =>'Foro',
            'slug' =>'foro'
        ]);
    }
}
