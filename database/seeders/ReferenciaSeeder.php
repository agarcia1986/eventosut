<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Referencia;

class ReferenciaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Referencia::create([
            'tipo'=> 'Estudiante',
            'referencia' => 'JDBE33',
            'precio'=> '200',
         
        ]);
        Referencia::create([
            'tipo'=> 'Administrativo',
            'referencia' => 'JDBE22',
            'precio'=> '200',
        ]);
        Referencia::create([
            'tipo'=> 'Egresado',
            'referencia' => 'JDBE11',
            'precio'=> '200',
          
        ]);
        Referencia::create([
            'tipo'=> 'Público en general',
            'referencia' => 'JDBE00',
            'precio'=> '250',
        ]);
        Referencia::create([
            'tipo'=> 'Cortesia',
            'referencia' => 'JDBE100',
            'precio'=> '0',
           
        ]);
    }
}
