<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role1= Role::create(['name' => 'Admin']);
        $role2= Role::create(['name' => 'Responsable']);
        $role3= Role::create(['name' => 'Participante']);
        /** Permisos administrador y encargado**/
       
        Permission::create(['name' => 'admin.home','description' => 'Dashboard'])->syncRoles([$role1, $role2]); 
         

        Permission::create(['name' => 'admin.users.index','description' => 'Lista de usuarios'])->syncRoles([$role1]);
        Permission::create(['name' => 'admin.users.edit','description' => 'Editar usuario'])->syncRoles([$role1]);
        Permission::create(['name' => 'admin.users.update','description' => 'Actualizar usuario'])->syncRoles([$role1]);
        
        Permission::create(['name' => 'admin.roles.index','description' => 'Lista de roles'])->syncRoles([$role1]);
        Permission::create(['name' => 'admin.roles.edit','description' => 'Editar roles'])->syncRoles([$role1]);
        Permission::create(['name' => 'admin.roles.update','description' => 'Actualizar roles'])->syncRoles([$role1]);
        
        Permission::create(['name' => 'admin.permisos.index','description' => 'Lista de permiso'])->syncRoles([$role1]);
        Permission::create(['name' => 'admin.permisos.edit','description' => 'Editar permiso'])->syncRoles([$role1]);
        Permission::create(['name' => 'admin.permisos.update','description' => 'Actualizar permiso'])->syncRoles([$role1]);
        Permission::create(['name' => 'admin.permisos.destroy','description' => 'Eliminar permiso'])->syncRoles([$role1, $role2]);

        Permission::create(['name' => 'admin.categorias.index','description' => 'Lista de categorias'])->syncRoles([$role1, $role2]);
        Permission::create(['name' => 'admin.categorias.create','description' => 'Crear categorias'])->syncRoles([$role1, $role2]);
        Permission::create(['name' => 'admin.categorias.edit','description' => 'Editar categorias'])->syncRoles([$role1, $role2]);
        Permission::create(['name' => 'admin.categorias.destroy','description' => 'Eliminar categorias'])->syncRoles([$role1, $role2]);

        Permission::create(['name' => 'admin.subcategorias.index','description' => 'Lista de subcategorias'])->syncRoles([$role1, $role2]);
        Permission::create(['name' => 'admin.subcategorias.create','description' => 'Crear subcategorias'])->syncRoles([$role1, $role2]);
        Permission::create(['name' => 'admin.subcategorias.edit','description' => 'Editar subcategorias'])->syncRoles([$role1, $role2]);
        Permission::create(['name' => 'admin.subcategorias.destroy','description' => 'Eliminar subcategorias'])->syncRoles([$role1, $role2]);
        
        Permission::create(['name' => 'admin.pagos.index','description' => 'Ver pagos'])->syncRoles([$role1, $role2]);
        Permission::create(['name' => 'admin.pagos.create','description' => 'Crear pagos'])->syncRoles([$role1, $role2]);
        Permission::create(['name' => 'admin.pagos.edit','description' => 'Editar pagos'])->syncRoles([$role1, $role2]);
        Permission::create(['name' => 'admin.pagos.destroy','description' => 'Eliminar pagos'])->syncRoles([$role1, $role2]);
      
        Permission::create(['name' => 'admin.eventos.index','description' => 'Listar eventos'])->syncRoles([$role1, $role2]);
        Permission::create(['name' => 'admin.eventos.create','description' => 'Crear eventos'])->syncRoles([$role1, $role2]);
        Permission::create(['name' => 'admin.eventos.edit','description' => 'Editar eventos'])->syncRoles([$role1, $role2]);
        Permission::create(['name' => 'admin.eventos.destroy','description' => 'Eliminar eventos'])->syncRoles([$role1, $role2]);

        Permission::create(['name' => 'admin.participantes.index','description' => 'Listar participante'])->syncRoles([$role1, $role2]);
        Permission::create(['name' => 'admin.participantes.create','description' => 'Crear participante'])->syncRoles([$role1, $role2]);
        Permission::create(['name' => 'admin.participantes.edit','description' => 'Editar participante'])->syncRoles([$role1, $role2]);
        Permission::create(['name' => 'admin.participantes.destroy','description' => 'Eliminar participante'])->syncRoles([$role1, $role2]);

        Permission::create(['name' => 'admin.referencias.index','description' => 'Listar referencias'])->syncRoles([$role1, $role2]);
        Permission::create(['name' => 'admin.referencias.create','description' => 'Crear referencias'])->syncRoles([$role1, $role2]);
        Permission::create(['name' => 'admin.referencias.edit','description' => 'Editar referencias'])->syncRoles([$role1, $role2]);
        Permission::create(['name' => 'admin.referencias.destroy','description' => 'Eliminar referencias'])->syncRoles([$role1, $role2]);
       
        /**Permisos participantes **/
        Permission::create(['name' => 'user.home','description' => 'Dashboard-Usuario'])->assignRole($role3);


        Permission::create(['name' => 'user.pagos.index','description' => 'Ver pagos Participante'])->assignRole($role3);
        Permission::create(['name' => 'user.pagos.create','description' => 'Crear pagos Participante'])->assignRole($role3);
        Permission::create(['name' => 'user.pagos.edit','description' => 'Editar pagos Participante'])->assignRole($role3);

        Permission::create(['name' => 'user.eventos.index','description' => 'Listar eventos Participante'])->assignRole($role3);
        Permission::create(['name' => 'user.eventos.create','description' => 'Crear eventos Participante'])->assignRole($role3);
        Permission::create(['name' => 'user.eventos.edit','description' => 'Editar eventos Participante'])->assignRole($role3);
        //Permission::create(['name' => 'user.eventos.destroy','description' => 'Eliminar eventos Participante'])->syncRoles([$role1, $role2]);

        Permission::create(['name' => 'user.participantes.index','description' => 'Listar participante Participante'])->assignRole($role3);
        Permission::create(['name' => 'user.participantes.create','description' => 'Crear participante Participante'])->assignRole($role2);
        Permission::create(['name' => 'user.participantes.edit','description' => 'Editar participante Participante'])->assignRole($role3);
        //Permission::create(['name' => 'user.participantes.destroy','description' => 'Eliminar participante Participante'])->syncRoles([$role1, $role2]);
        
    }
}
