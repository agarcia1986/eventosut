<?php

namespace Database\Seeders;

use App\Models\Participante;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    public function run()
    {
        $user1=User::create([
            'name'=> 'Aurelio',
            'pname'=>'Garcia',
            'mname'=>'Mendoza',
            'email'=> 'agarcia@utcancun.edu.mx',
            'password'=> bcrypt('Garcia982')          
        ])->assignRole('Admin');
        Participante::create([
            'talla' => 'md',
            'alergias' => 'Ninguna',
            'condicion' => 'Ninguna',
            'sexo' => '1',
            'club' => 'Ninguno',
            'celular' => '8 81 19 00',        
            'user_id' => $user1->id
        ]);
        $user2=User::create([
            'name'=> 'Justino',
            'pname'=>'Sanchez',
            'mname'=>'',
            'email'=> 'jusanchez@utcancun.edu.mx',
            'password'=> bcrypt('Just1n0$!')   
        ])->assignRole('Responsable');
        Participante::create([
            'talla' => 'md',
            'alergias' => 'Ninguna',
            'condicion' => 'Ninguna',
            'sexo' => '1',
            'club' => 'Ninguno',
            'celular' => '8 81 19 00',        
            'user_id' => $user2->id
        ]);
        $user3 =User::create([
            'name'=> 'Arlette Cristelly',
            'pname'=>'Cime',
            'mname'=>'Albornoz',
            'email'=> '19362071@utcancun.edu.mx',
            'password'=> bcrypt('password')   
        ])->assignRole('Participante');
        Participante::create([
            'talla' => 'md',
            'alergias' => 'Ninguna',
            'condicion' => 'Ninguna',
            'sexo' => '2',
            'club' => 'Ninguno',
            'celular' => '8 81 19 00',        
            'user_id' => $user3->id
        ]);
    }
}