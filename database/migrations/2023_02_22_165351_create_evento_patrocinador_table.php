<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evento_patrocinador', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('patrocinador_id');
            $table->unsignedBigInteger('evento_id');
            $table->foreign('patrocinador_id')->references('id')->on('patrocinadors')->onDelete('cascade');
            $table->foreign('evento_id')->references('id')->on('eventos')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evento_patrocinador');
    }
};
