<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{

    public function up()
    {
        Schema::create('pagos', function (Blueprint $table) {
            $table->id();
            $table->string('referencia');
            $table->date('fechavencimiento');
            $table->integer('monto');
            $table->string('numtarjeta')->nullable();
            $table->string('mesexpira')->nullable();
            $table->string('cve')->nullable();
            $table->enum('tipo',[1,2,3])->default(1);
            $table->enum('estatuspago',[1,2,3])->default(1);
            $table->unsignedBigInteger('participante_id')->nullable();
            $table->foreign('participante_id')->references('id')->on('participantes')->onDelete('set null');
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('pagos');
    }
};
