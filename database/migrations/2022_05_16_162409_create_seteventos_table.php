<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    
    public function up()
    {
        Schema::create('seteventos', function (Blueprint $table) {
            $table->id();
            $table->string('detalle')->nullable();
            $table->string('requisitos');
            $table->string('video')->nullable();
            $table->string('localidad');//programa
            $table->date('iniciainscripcion')->nullable();
            $table->date('terminainscripcion')->nullable();
            $table->string('horaInicio');
            $table->date('fechaevento');
            $table->integer('capacidad')->nullable();
            $table->integer('inscritos')->nullable();
            $table->unsignedBigInteger('evento_id')->unique();
            $table->timestamps();
            $table->foreign('evento_id')->references('id')->on('eventos')->onDelete('cascade');

        });
    }

    
    public function down()
    {
        Schema::dropIfExists('seteventos');
    }
};
