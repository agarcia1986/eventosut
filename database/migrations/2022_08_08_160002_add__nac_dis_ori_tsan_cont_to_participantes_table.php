<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('participantes', function (Blueprint $table) {
            $table->date('fechanac')->nullable()->after('celular');
            $table->string('modalidad')->nullable()->after('celular');
            $table->string('enfermedad')->nullable()->after('celular');
            $table->string('tiposanguineo')->nullable()->after('celular');
            $table->string('emergencia')->nullable()->after('celular');
            $table->string('responsabilidad')->nullable()->after('celular');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('participantes', function (Blueprint $table) {
            $table->dropColumn('fechanac','modalidad','enfermedad','tiposanguineo','emergencia','responsabilidad');
        });
    }
};
